// THIS IS A GENERATED FILE. DO NOT EDIT. SOURCE metrics.py AND metrics_table.py
registerCounter64Stat("questions", questionsOID);
registerCounter64Stat("ipv6-questions", ipv6QuestionsOID);
registerCounter64Stat("tcp-questions", tcpQuestionsOID);
registerCounter64Stat("cache-hits", cacheHitsOID);
registerCounter64Stat("cache-misses", cacheMissesOID);
registerCounter64Stat("cache-entries", cacheEntriesOID);
registerCounter64Stat("cache-bytes", cacheBytesOID);
registerCounter64Stat("packetcache-hits", packetcacheHitsOID);
registerCounter64Stat("packetcache-misses", packetcacheMissesOID);
registerCounter64Stat("packetcache-entries", packetcacheEntriesOID);
registerCounter64Stat("packetcache-bytes", packetcacheBytesOID);
registerCounter64Stat("malloc-bytes", mallocBytesOID);
registerCounter64Stat("servfail-answers", servfailAnswersOID);
registerCounter64Stat("nxdomain-answers", nxdomainAnswersOID);
registerCounter64Stat("noerror-answers", noerrorAnswersOID);
registerCounter64Stat("unauthorized-udp", unauthorizedUdpOID);
registerCounter64Stat("unauthorized-tcp", unauthorizedTcpOID);
registerCounter64Stat("tcp-client-overflow", tcpClientOverflowOID);
registerCounter64Stat("client-parse-errors", clientParseErrorsOID);
registerCounter64Stat("server-parse-errors", serverParseErrorsOID);
registerCounter64Stat("too-old-drops", tooOldDropsOID);
registerCounter64Stat("answers0-1", answers01OID);
registerCounter64Stat("answers1-10", answers110OID);
registerCounter64Stat("answers10-100", answers10100OID);
registerCounter64Stat("answers100-1000", answers1001000OID);
registerCounter64Stat("answers-slow", answersSlowOID);
registerCounter64Stat("auth4-answers0-1", auth4Answers01OID);
registerCounter64Stat("auth4-answers1-10", auth4Answers110OID);
registerCounter64Stat("auth4-answers10-100", auth4Answers10100OID);
registerCounter64Stat("auth4-answers100-1000", auth4Answers1001000OID);
registerCounter64Stat("auth4-answers-slow", auth4AnswersSlowOID);
registerCounter64Stat("auth6-answers0-1", auth6Answers01OID);
registerCounter64Stat("auth6-answers1-10", auth6Answers110OID);
registerCounter64Stat("auth6-answers10-100", auth6Answers10100OID);
registerCounter64Stat("auth6-answers100-1000", auth6Answers1001000OID);
registerCounter64Stat("auth6-answers-slow", auth6AnswersSlowOID);
registerCounter64Stat("qa-latency", qaLatencyOID);
registerCounter64Stat("unexpected-packets", unexpectedPacketsOID);
registerCounter64Stat("case-mismatches", caseMismatchesOID);
registerCounter64Stat("spoof-prevents", spoofPreventsOID);
registerCounter64Stat("nsset-invalidations", nssetInvalidationsOID);
registerCounter64Stat("resource-limits", resourceLimitsOID);
registerCounter64Stat("over-capacity-drops", overCapacityDropsOID);
registerCounter64Stat("policy-drops", policyDropsOID);
registerCounter64Stat("no-packet-error", noPacketErrorOID);
registerCounter64Stat("dlg-only-drops", dlgOnlyDropsOID);
registerCounter64Stat("ignored-packets", ignoredPacketsOID);
registerCounter64Stat("max-mthread-stack", maxMthreadStackOID);
registerCounter64Stat("negcache-entries", negcacheEntriesOID);
registerCounter64Stat("throttle-entries", throttleEntriesOID);
registerCounter64Stat("nsspeeds-entries", nsspeedsEntriesOID);
registerCounter64Stat("failed-host-entries", failedHostEntriesOID);
registerCounter64Stat("concurrent-queries", concurrentQueriesOID);
registerCounter64Stat("security-status", securityStatusOID);
registerCounter64Stat("outgoing-timeouts", outgoingTimeoutsOID);
registerCounter64Stat("outgoing4-timeouts", outgoing4TimeoutsOID);
registerCounter64Stat("outgoing6-timeouts", outgoing6TimeoutsOID);
registerCounter64Stat("tcp-outqueries", tcpOutqueriesOID);
registerCounter64Stat("all-outqueries", allOutqueriesOID);
registerCounter64Stat("ipv6-outqueries", ipv6OutqueriesOID);
registerCounter64Stat("throttled-outqueries", throttledOutqueriesOID);
registerCounter64Stat("dont-outqueries", dontOutqueriesOID);
registerCounter64Stat("unreachables", unreachablesOID);
registerCounter64Stat("chain-resends", chainResendsOID);
registerCounter64Stat("tcp-clients", tcpClientsOID);
#ifdef __linux
registerCounter64Stat("udp-recvbuf-errors", udpRecvbufErrorsOID);
#endif
#ifdef __linux
registerCounter64Stat("udp-sndbuf-errors", udpSndbufErrorsOID);
#endif
#ifdef __linux
registerCounter64Stat("udp-noport-errors", udpNoportErrorsOID);
#endif
#ifdef __linux
registerCounter64Stat("udp-in-errors", udpInErrorsOID);
#endif
registerCounter64Stat("edns-ping-matches", ednsPingMatchesOID);
registerCounter64Stat("edns-ping-mismatches", ednsPingMismatchesOID);
registerCounter64Stat("dnssec-queries", dnssecQueriesOID);
registerCounter64Stat("noping-outqueries", nopingOutqueriesOID);
registerCounter64Stat("noedns-outqueries", noednsOutqueriesOID);
registerCounter64Stat("uptime", uptimeOID);
registerCounter64Stat("real-memory-usage", realMemoryUsageOID);
registerCounter64Stat("fd-usage", fdUsageOID);
registerCounter64Stat("user-msec", userMsecOID);
registerCounter64Stat("sys-msec", sysMsecOID);
registerCounter64Stat("dnssec-validations", dnssecValidationsOID);
registerCounter64Stat("dnssec-result-insecure", dnssecResultInsecureOID);
registerCounter64Stat("dnssec-result-secure", dnssecResultSecureOID);
registerCounter64Stat("dnssec-result-bogus", dnssecResultBogusOID);
registerCounter64Stat("dnssec-result-indeterminate", dnssecResultIndeterminateOID);
registerCounter64Stat("dnssec-result-nta", dnssecResultNtaOID);
registerCounter64Stat("policy-result-noaction", policyResultNoactionOID);
registerCounter64Stat("policy-result-drop", policyResultDropOID);
registerCounter64Stat("policy-result-nxdomain", policyResultNxdomainOID);
registerCounter64Stat("policy-result-nodata", policyResultNodataOID);
registerCounter64Stat("policy-result-truncate", policyResultTruncateOID);
registerCounter64Stat("policy-result-custom", policyResultCustomOID);
registerCounter64Stat("query-pipe-full-drops", queryPipeFullDropsOID);
registerCounter64Stat("truncated-drops", truncatedDropsOID);
registerCounter64Stat("empty-queries", emptyQueriesOID);
registerCounter64Stat("dnssec-authentic-data-queries", dnssecAuthenticDataQueriesOID);
registerCounter64Stat("dnssec-check-disabled-queries", dnssecCheckDisabledQueriesOID);
registerCounter64Stat("variable-responses", variableResponsesOID);
registerCounter64Stat("special-memory-usage", specialMemoryUsageOID);
registerCounter64Stat("rebalanced-queries", rebalancedQueriesOID);
registerCounter64Stat("qname-min-fallback-success", qnameMinFallbackSuccessOID);
registerCounter64Stat("proxy-protocol-invalid", proxyProtocolInvalidOID);
registerCounter64Stat("record-cache-contended", recordCacheContendedOID);
registerCounter64Stat("record-cache-acquired", recordCacheAcquiredOID);
registerCounter64Stat("nod-lookups-dropped-oversize", nodLookupsDroppedOversizeOID);
registerCounter64Stat("taskqueue-pushed", taskqueuePushedOID);
registerCounter64Stat("taskqueue-expired", taskqueueExpiredOID);
registerCounter64Stat("taskqueue-size", taskqueueSizeOID);
registerCounter64Stat("aggressive-nsec-cache-entries", aggressiveNSECCacheEntriesOID);
registerCounter64Stat("aggressive-nsec-cache-nsec-hits", aggressiveNSECCacheNSECHitsOID);
registerCounter64Stat("aggressive-nsec-cache-nsec3-hits", aggressiveNSECCacheNSEC3HitsOID);
registerCounter64Stat("aggressive-nsec-cache-nsec-wc-hits", aggressiveNSECCacheNSECWcHitsOID);
registerCounter64Stat("aggressive-nsec-cache-nsec3-wc-hits", aggressiveNSECCacheNSEC3WcHitsOID);
registerCounter64Stat("dot-outqueries", dotOutqueriesOID);
registerCounter64Stat("dns64-prefix-answers", dns64PrefixAnswersOID);
registerCounter64Stat("almost-expired-pushed", almostExpiredPushedOID);
registerCounter64Stat("almost-expired-run", almostExpiredRunOID);
registerCounter64Stat("almost-expired-exceptions", almostExpiredExceptionsOID);
#ifdef __linux
registerCounter64Stat("udp-in-csum-errors", udpInCsumErrorsOID);
#endif
#ifdef __linux
registerCounter64Stat("udp6-recvbuf-errors", udp6RecvbufErrorsOID);
#endif
#ifdef __linux
registerCounter64Stat("udp6-sndbuf-errors", udp6SndbufErrorsOID);
#endif
#ifdef __linux
registerCounter64Stat("udp6-noport-errors", udp6NoportErrorsOID);
#endif
#ifdef __linux
registerCounter64Stat("udp6-in-errors", udp6InErrorsOID);
#endif
#ifdef __linux
registerCounter64Stat("udp6-in-csum-errors", udp6InCsumErrorsOID);
#endif
registerCounter64Stat("source-disallowed-notify", sourceDisallowedNotifyOID);
registerCounter64Stat("zone-disallowed-notify", zoneDisallowedNotifyOID);
registerCounter64Stat("non-resolving-nameserver-entries", nonResolvingNameserverEntriesOID);
registerCounter64Stat("maintenance-usec", maintenanceUsecOID);
registerCounter64Stat("maintenance-calls", maintenanceCallsOID);
registerCounter64Stat("auth-noerror-answers", authNoerrorAnswersOID);
registerCounter64Stat("auth-formerr-answers", authFormerrAnswersOID);
registerCounter64Stat("auth-servfail-answers", authServfailAnswersOID);
registerCounter64Stat("auth-nxdomain-answers", authNxdomainAnswersOID);
registerCounter64Stat("auth-notimp-answers", authNotimpAnswersOID);
registerCounter64Stat("auth-refused-answers", authRefusedAnswersOID);
registerCounter64Stat("auth-yxdomain-answers", authYxdomainAnswersOID);
registerCounter64Stat("auth-yxrrset-answers", authYxrrsetAnswersOID);
registerCounter64Stat("auth-nxrrset-answers", authNxrrsetAnswersOID);
registerCounter64Stat("auth-notauth-answers", authNotauthAnswersOID);
registerCounter64Stat("auth-rcode10-answers", authRcode10AnswersOID);
registerCounter64Stat("auth-rcode11-answers", authRcode11AnswersOID);
registerCounter64Stat("auth-rcode12-answers", authRcode12AnswersOID);
registerCounter64Stat("auth-rcode13-answers", authRcode13AnswersOID);
registerCounter64Stat("auth-rcode14-answers", authRcode14AnswersOID);
registerCounter64Stat("auth-rcode15-answers", authRcode15AnswersOID);
registerCounter64Stat("packetcache-contended", packetcacheContendedOID);
registerCounter64Stat("packetcache-acquired", packetcacheAcquiredOID);
registerCounter64Stat("nod-events", nodEventsOID);
registerCounter64Stat("udr-events", udrEventsOID);
registerCounter64Stat("max-chain-length", maxChainLengthOID);
registerCounter64Stat("max-chain-weight", maxChainWeightOID);
registerCounter64Stat("chain-limits", chainLimitsOID);
registerCounter64Stat("tcp-overflow", tcpOverflowOID);
