// THIS IS A GENERATED FILE. DO NOT EDIT. SOURCE: see settings dir

#include "arguments.hh"
#include "cxxsettings.hh"
#include "cxxsettings-private.hh"

void pdns::settings::rec::defineOldStyleSettings()
{
  ::arg().set("aggressive-nsec-cache-size", "The number of records to cache in the aggressive cache. If set to a value greater than 0, and DNSSEC processing or validation is enabled, the recursor will cache NSEC and NSEC3 records to generate negative answers, as defined in rfc8198") = "100000";
  ::arg().set("aggressive-cache-min-nsec3-hit-ratio", "The minimum expected hit ratio to store NSEC3 records into the aggressive cache") = "2000";
  ::arg().set("allow-from", "If set, only allow these comma separated netmasks to recurse") = "127.0.0.0/8, 10.0.0.0/8, 100.64.0.0/10, 169.254.0.0/16, 192.168.0.0/16, 172.16.0.0/12, ::1/128, fc00::/7, fe80::/10";
  ::arg().set("allow-from-file", "If set, load allowed netmasks from this file") = "";
  ::arg().set("allow-notify-for", "If set, NOTIFY requests for these zones will be allowed") = "";
  ::arg().set("allow-notify-for-file", "If set, load NOTIFY-allowed zones from this file") = "";
  ::arg().set("allow-notify-from", "If set, NOTIFY requests from these comma separated netmasks will be allowed") = "";
  ::arg().set("allow-notify-from-file", "If set, load NOTIFY-allowed netmasks from this file") = "";
  ::arg().setSwitch("allow-no-rd", "Allow 'no recursion desired (RD=0)' queries.") = "no";
  ::arg().setSwitch("any-to-tcp", "Answer ANY queries with tc=1, shunting to TCP") = "no";
  ::arg().setSwitch("allow-trust-anchor-query", "Allow queries for trustanchor.server CH TXT and negativetrustanchor.server CH TXT") = "no";
  ::arg().set("api-config-dir", "Directory where REST API stores config and zones") = "";
  ::arg().set("api-key", "Static pre-shared authentication key for access to the REST API") = "";
  ::arg().set("auth-zones", "Zones for which we have authoritative data, comma separated domain=file pairs") = "";
  ::arg().set("carbon-interval", "Number of seconds between carbon (graphite) updates") = "30";
  ::arg().set("carbon-namespace", "If set overwrites the first part of the carbon string") = "pdns";
  ::arg().set("carbon-ourname", "If set, overrides our reported hostname for carbon stats") = "";
  ::arg().set("carbon-instance", "If set overwrites the instance name default") = "recursor";
  ::arg().set("carbon-server", "If set, send metrics in carbon (graphite) format to this server IP address") = "";
  ::arg().set("chroot", "switch to chroot jail") = "";
  ::arg().set("client-tcp-timeout", "Timeout in seconds when talking to TCP clients") = "2";
  ::arg().setCmd("config", "Output blank configuration. You can use --config=check to test the config file and command line arguments.");
  ::arg().set("config-dir", "Location of configuration directory (recursor.conf or recursor.yml)") = SYSCONFDIR;
  ::arg().set("config-name", "Name of this virtual configuration - will rename the binary image") = "";
  ::arg().set("cpu-map", "Thread to CPU mapping, space separated thread-id=cpu1,cpu2..cpuN pairs") = "";
  ::arg().setSwitch("daemon", "Operate as a daemon") = "no";
  ::arg().set("dont-throttle-names", "Do not throttle nameservers with this name or suffix") = "";
  ::arg().set("dont-throttle-netmasks", "Do not throttle nameservers with this IP netmask") = "";
  ::arg().setSwitch("devonly-regression-test-mode", "internal use only") = "no";
  ::arg().setSwitch("disable-packetcache", "Disable packetcache") = "no";
  ::arg().setSwitch("disable-syslog", "Disable logging to syslog, useful when running inside a supervisor that logs stderr") = "no";
  ::arg().set("distribution-load-factor", "The load factor used when PowerDNS is distributing queries to worker threads") = "0.0";
  ::arg().set("distribution-pipe-buffer-size", "Size in bytes of the internal buffer of the pipe used by the distributor to pass incoming queries to a worker thread") = "0";
  ::arg().set("distributor-threads", "Launch this number of distributor threads, distributing queries to other threads") = "0";
  ::arg().set("dot-to-auth-names", "Use DoT to authoritative servers with these names or suffixes") = "";
  ::arg().setSwitch("dot-to-port-853", "Force DoT connection to target port 853 if DoT compiled in") = "yes";
  ::arg().set("dns64-prefix", "DNS64 prefix") = "";
  ::arg().set("dnssec", "DNSSEC mode: off/process-no-validate/process (default)/log-fail/validate") = "process";
  ::arg().set("dnssec-disabled-algorithms", "List of DNSSEC algorithm numbers that are considered unsupported") = "";
  ::arg().setSwitch("dnssec-log-bogus", "Log DNSSEC bogus validations") = "no";
  ::arg().set("dont-query", "If set, do not query these netmasks for DNS data") = "127.0.0.0/8, 10.0.0.0/8, 100.64.0.0/10, 169.254.0.0/16, 192.168.0.0/16, 172.16.0.0/12, ::1/128, fc00::/7, fe80::/10, 0.0.0.0/8, 192.0.0.0/24, 192.0.2.0/24, 198.51.100.0/24, 203.0.113.0/24, 240.0.0.0/4, ::/96, ::ffff:0:0/96, 100::/64, 2001:db8::/32";
  ::arg().set("ecs-add-for", "List of client netmasks for which EDNS Client Subnet will be added") = "0.0.0.0/0, ::/0, !127.0.0.0/8, !10.0.0.0/8, !100.64.0.0/10, !169.254.0.0/16, !192.168.0.0/16, !172.16.0.0/12, !::1/128, !fc00::/7, !fe80::/10";
  ::arg().set("ecs-ipv4-bits", "Number of bits of IPv4 address to pass for EDNS Client Subnet") = "24";
  ::arg().set("ecs-ipv4-cache-bits", "Maximum number of bits of IPv4 mask to cache ECS response") = "24";
  ::arg().set("ecs-ipv6-bits", "Number of bits of IPv6 address to pass for EDNS Client Subnet") = "56";
  ::arg().set("ecs-ipv6-cache-bits", "Maximum number of bits of IPv6 mask to cache ECS response") = "56";
  ::arg().setSwitch("ecs-ipv4-never-cache", "If we should never cache IPv4 ECS responses") = "no";
  ::arg().setSwitch("ecs-ipv6-never-cache", "If we should never cache IPv6 ECS responses") = "no";
  ::arg().set("ecs-minimum-ttl-override", "The minimum TTL for records in ECS-specific answers") = "1";
  ::arg().set("ecs-cache-limit-ttl", "Minimum TTL to cache ECS response") = "0";
  ::arg().set("ecs-scope-zero-address", "Address to send to allow-listed authoritative servers for incoming queries with ECS prefix-length source of 0") = "";
  ::arg().set("edns-outgoing-bufsize", "Outgoing EDNS buffer size") = "1232";
  ::arg().set("edns-padding-from", "List of netmasks (proxy IP in case of proxy-protocol presence, client IP otherwise) for which EDNS padding will be enabled in responses, provided that 'edns-padding-mode' applies") = "";
  ::arg().set("edns-padding-mode", "Whether to add EDNS padding to all responses ('always') or only to responses for queries containing the EDNS padding option ('padded-queries-only', the default). In both modes, padding will only be added to responses for queries coming from 'setting-edns-padding-from' sources") = "padded-queries-only";
  ::arg().setSwitch("edns-padding-out", "Whether to add EDNS padding to outgoing DoT messages") = "yes";
  ::arg().set("edns-padding-tag", "Packetcache tag associated to responses sent with EDNS padding, to prevent sending these to clients for which padding is not enabled.") = "7830";
  ::arg().set("edns-subnet-whitelist", "List of netmasks and domains that we should enable EDNS subnet for (deprecated)") = "";
  ::arg().set("edns-subnet-allow-list", "List of netmasks and domains that we should enable EDNS subnet for") = "";
  ::arg().setSwitch("enable-old-settings", "Enable (deprecated) parsing of old-style settings") = "no";
  ::arg().set("entropy-source", "If set, read entropy from this file") = "/dev/urandom";
  ::arg().set("etc-hosts-file", "Path to 'hosts' file") = "/etc/hosts";
  ::arg().set("event-trace-enabled", "If set, event traces are collected and send out via protobuf logging (1), logfile (2) or both(3)") = "0";
  ::arg().setSwitch("export-etc-hosts", "If we should serve up contents from /etc/hosts") = "no";
  ::arg().set("export-etc-hosts-search-suffix", "Also serve up the contents of /etc/hosts with this suffix") = "";
  ::arg().setSwitch("extended-resolution-errors", "If set, send an EDNS Extended Error extension on resolution failures, like DNSSEC validation errors") = "yes";
  ::arg().set("forward-zones", "Zones for which we forward queries, comma separated domain=ip pairs") = "";
  ::arg().set("forward-zones-file", "File with (+)domain=ip pairs for forwarding") = "";
  ::arg().set("forward-zones-recurse", "Zones for which we forward queries with recursion bit, comma separated domain=ip pairs") = "";
  ::arg().setSwitch("gettag-needs-edns-options", "If EDNS Options should be extracted before calling the gettag() hook") = "no";
  ::arg().setCmd("help", "Provide a helpful message");
  ::arg().set("hint-file", "If set, load root hints from this file") = "";
  ::arg().set("ignore-unknown-settings", "Configuration settings to ignore if they are unknown") = "";
  ::arg().set("include-dir", "Include settings files from this directory.") = "";
  ::arg().set("latency-statistic-size", "Number of latency values to calculate the qa-latency average") = "10000";
  ::arg().set("local-address", "IP addresses to listen on, separated by spaces or commas. Also accepts ports.") = "127.0.0.1";
  ::arg().set("local-port", "port to listen on") = "53";
  ::arg().setSwitch("log-timestamp", "Print timestamps in log lines, useful to disable when running with a tool that timestamps stderr already") = "yes";
  ::arg().setSwitch("non-local-bind", "Enable binding to non-local addresses by using FREEBIND / BINDANY socket options") = "no";
  ::arg().set("loglevel", "Amount of logging. Higher is more. Do not set below 3") = "6";
  ::arg().setSwitch("log-common-errors", "If we should log rather common errors") = "no";
  ::arg().setSwitch("log-rpz-changes", "Log additions and removals to RPZ zones at Info level") = "no";
  ::arg().set("logging-facility", "Facility to log messages as. 0 corresponds to local0") = "";
  ::arg().setSwitch("lowercase-outgoing", "Force outgoing questions to lowercase") = "no";
  ::arg().set("lua-config-file", "More powerful configuration options") = "";
  ::arg().set("lua-global-include-dir", "More powerful configuration options") = "";
  ::arg().set("lua-dns-script", "Filename containing an optional Lua script that will be used to modify dns answers") = "";
  ::arg().set("lua-maintenance-interval", "Number of seconds between calls to the lua user defined maintenance() function") = "1";
  ::arg().set("max-busy-dot-probes", "Maximum number of concurrent DoT probes") = "0";
  ::arg().set("max-cache-bogus-ttl", "maximum number of seconds to keep a Bogus (positive or negative) cached entry in memory") = "3600";
  ::arg().set("max-cache-entries", "If set, maximum number of entries in the main cache") = "1000000";
  ::arg().set("max-cache-ttl", "maximum number of seconds to keep a cached entry in memory") = "86400";
  ::arg().set("max-concurrent-requests-per-tcp-connection", "Maximum number of requests handled concurrently per TCP connection") = "10";
  ::arg().set("max-chain-length", "maximum number of queries that can be chained to an outgoing request, 0 is no limit") = "0";
  ::arg().set("max-include-depth", "Maximum nested $INCLUDE depth when loading a zone from a file") = "20";
  ::arg().set("max-generate-steps", "Maximum number of $GENERATE steps when loading a zone from a file") = "0";
  ::arg().set("max-mthreads", "Maximum number of simultaneous Mtasker threads") = "2048";
  ::arg().set("max-packetcache-entries", "maximum number of entries to keep in the packetcache") = "500000";
  ::arg().set("max-qperq", "Maximum outgoing queries per query") = "50";
  ::arg().set("max-cnames-followed", "Maximum number CNAME records followed") = "10";
  ::arg().setSwitch("limit-qtype-any", "Limit answers to ANY queries in size") = "yes";
  ::arg().set("max-rrset-size", "Maximum size of RRSet in cache") = "256";
  ::arg().set("max-ns-address-qperq", "Maximum outgoing NS address queries per query") = "10";
  ::arg().set("max-ns-per-resolve", "Maximum number of NS records to consider to resolve a name, 0 is no limit") = "13";
  ::arg().set("max-negative-ttl", "maximum number of seconds to keep a negative cached entry in memory") = "3600";
  ::arg().set("max-recursion-depth", "Maximum number of internal recursion calls per query, 0 for unlimited") = "16";
  ::arg().set("max-tcp-clients", "Maximum number of simultaneous TCP clients") = "1024";
  ::arg().set("max-tcp-per-client", "If set, maximum number of TCP sessions per client (IP address)") = "0";
  ::arg().set("max-tcp-queries-per-connection", "If set, maximum number of TCP queries in a TCP connection") = "0";
  ::arg().set("max-total-msec", "Maximum total wall-clock time per query in milliseconds, 0 for unlimited") = "7000";
  ::arg().set("max-udp-queries-per-round", "Maximum number of UDP queries processed per recvmsg() round, before returning back to normal processing") = "10000";
  ::arg().set("minimum-ttl-override", "The minimum TTL") = "1";
  ::arg().setSwitch("new-domain-tracking", "Track newly observed domains (i.e. never seen before).") = "no";
  ::arg().setSwitch("new-domain-log", "Log newly observed domains.") = "yes";
  ::arg().set("new-domain-lookup", "Perform a DNS lookup newly observed domains as a subdomain of the configured domain") = "";
  ::arg().set("new-domain-db-size", "Size of the DB used to track new domains in terms of number of cells. Defaults to 67108864") = "67108864";
  ::arg().set("new-domain-history-dir", "Persist new domain tracking data here to persist between restarts") = NODCACHEDIRNOD;
  ::arg().set("new-domain-db-snapshot-interval", "Interval (in seconds) to write the NOD and UDR DB snapshots") = "600";
  ::arg().set("new-domain-whitelist", "List of domains (and implicitly all subdomains) which will never be considered a new domain (deprecated)") = "";
  ::arg().set("new-domain-ignore-list", "List of domains (and implicitly all subdomains) which will never be considered a new domain") = "";
  ::arg().set("new-domain-ignore-list-file", "File with a list of domains (and implicitly all subdomains) which will never be considered a new domain") = "";
  ::arg().set("new-domain-pb-tag", "If protobuf is configured, the tag to use for messages containing newly observed domains. Defaults to 'pdns-nod'") = "pdns-nod";
  ::arg().set("network-timeout", "Wait this number of milliseconds for network i/o") = "1500";
  ::arg().setSwitch("no-shuffle", "Don't change") = "no";
  ::arg().set("non-resolving-ns-max-fails", "Number of failed address resolves of a nameserver to start throttling it, 0 is disabled") = "5";
  ::arg().set("non-resolving-ns-throttle-time", "Number of seconds to throttle a nameserver with a name failing to resolve") = "60";
  ::arg().set("nothing-below-nxdomain", "When an NXDOMAIN exists in cache for a name with fewer labels than the qname, send NXDOMAIN without doing a lookup (see RFC 8020)") = "dnssec";
  ::arg().set("nsec3-max-iterations", "Maximum number of iterations allowed for an NSEC3 record") = "50";
  ::arg().set("max-rrsigs-per-record", "Maximum number of RRSIGs to consider when validating a given record") = "2";
  ::arg().set("max-nsec3s-per-record", "Maximum number of NSEC3s to consider when validating a given denial of existence") = "10";
  ::arg().set("max-signature-validations-per-query", "Maximum number of RRSIG signatures we are willing to validate per incoming query") = "30";
  ::arg().set("max-nsec3-hash-computations-per-query", "Maximum number of NSEC3 hashes that we are willing to compute during DNSSEC validation, per incoming query") = "600";
  ::arg().set("aggressive-cache-max-nsec3-hash-cost", "Maximum estimated NSEC3 cost for a given query to consider aggressive use of the NSEC3 cache") = "150";
  ::arg().set("max-ds-per-zone", "Maximum number of DS records to consider per zone") = "8";
  ::arg().set("max-dnskeys", "Maximum number of DNSKEYs with the same algorithm and tag to consider when validating a given record") = "2";
  ::arg().set("packetcache-ttl", "maximum number of seconds to keep a cached entry in packetcache") = "86400";
  ::arg().set("packetcache-negative-ttl", "maximum number of seconds to keep a cached NxDomain or NoData entry in packetcache") = "60";
  ::arg().set("packetcache-servfail-ttl", "maximum number of seconds to keep a cached servfail entry in packetcache") = "60";
  ::arg().set("packetcache-shards", "Number of shards in the packet cache") = "1024";
  ::arg().setSwitch("pdns-distributes-queries", "If PowerDNS itself should distribute queries over threads") = "no";
  ::arg().set("processes", "Launch this number of processes (EXPERIMENTAL, DO NOT CHANGE)") = "1";
  ::arg().setSwitch("protobuf-use-kernel-timestamp", "Compute the latency of queries in protobuf messages by using the timestamp set by the kernel when the query was received (when available)") = "no";
  ::arg().set("proxy-protocol-from", "A Proxy Protocol header is required from these subnets") = "";
  ::arg().set("proxy-protocol-exceptions", "A Proxy Protocol header should not be used for these listen addresses.") = "";
  ::arg().set("proxy-protocol-maximum-size", "The maximum size of a proxy protocol payload, including the TLV values") = "512";
  ::arg().set("public-suffix-list-file", "Path to the Public Suffix List file, if any") = "";
  ::arg().setSwitch("qname-minimization", "Use Query Name Minimization") = "yes";
  ::arg().set("qname-max-minimize-count", "RFC9156 max minimize count") = "10";
  ::arg().set("qname-minimize-one-label", "RFC9156 minimize one label parameter") = "4";
  ::arg().set("query-local-address", "Source IP address for sending queries") = "0.0.0.0";
  ::arg().setSwitch("quiet", "Suppress logging of questions and answers") = "yes";
  ::arg().set("record-cache-locked-ttl-perc", "Replace records in record cache only after this % of original TTL has passed") = "0";
  ::arg().set("record-cache-shards", "Number of shards in the record cache") = "1024";
  ::arg().set("refresh-on-ttl-perc", "If a record is requested from the cache and only this % of original TTL remains, refetch") = "0";
  ::arg().setSwitch("reuseport", "Enable SO_REUSEPORT allowing multiple recursors processes to listen to 1 address") = "yes";
  ::arg().set("rng", "Specify random number generator to use. Valid values are auto,sodium,openssl,getrandom,arc4random,urandom.") = "auto";
  ::arg().setSwitch("root-nx-trust", "If set, believe that an NXDOMAIN from the root means the TLD does not exist") = "yes";
  ::arg().setSwitch("save-parent-ns-set", "Save parent NS set to be used if child NS set fails") = "yes";
  ::arg().set("security-poll-suffix", "Domain name from which to query security update notifications") = "secpoll.powerdns.com.";
  ::arg().setSwitch("serve-rfc1918", "If we should be authoritative for RFC 1918 private IP space") = "yes";
  ::arg().setSwitch("serve-rfc6303", "If we should be authoritative for RFC 6303 private IP space") = "yes";
  ::arg().set("serve-stale-extensions", "Number of times a record's ttl is extended by 30s to be served stale") = "0";
  ::arg().set("server-down-max-fails", "Maximum number of consecutive timeouts (and unreachables) to mark a server as down ( 0 => disabled )") = "64";
  ::arg().set("server-down-throttle-time", "Number of seconds to throttle all queries to a server after being marked as down") = "60";
  ::arg().set("bypass-server-throttling-probability", "Determines the probability of a server marked down to be used anyway") = "25";
  ::arg().set("server-id", "Returned when queried for 'id.server' TXT or NSID, defaults to hostname, set custom or 'disabled'") = "*runtime determined*";
  ::arg().set("setgid", "If set, change group id to this gid for more security") = "";
  ::arg().set("setuid", "If set, change user id to this uid for more security") = "";
  ::arg().set("signature-inception-skew", "Allow the signature inception to be off by this number of seconds") = "60";
  ::arg().setSwitch("single-socket", "If set, only use a single socket for outgoing queries") = "no";
  ::arg().setSwitch("snmp-agent", "If set, register as an SNMP agent") = "no";
  ::arg().set("snmp-master-socket", "If set and snmp-agent is set, the socket to use to register to the SNMP daemon (deprecated)") = "";
  ::arg().set("snmp-daemon-socket", "If set and snmp-agent is set, the socket to use to register to the SNMP daemon") = "";
  ::arg().set("soa-minimum-ttl", "Don't change") = "0";
  ::arg().set("socket-dir", "Where the controlsocket will live, /var/run/pdns-recursor when unset and not chrooted") = "";
  ::arg().set("socket-group", "Group of socket") = "";
  ::arg().set("socket-mode", "Permissions for socket") = "";
  ::arg().set("socket-owner", "Owner of socket") = "";
  ::arg().set("spoof-nearmiss-max", "If non-zero, assume spoofing after this many near misses") = "1";
  ::arg().set("stack-cache-size", "Size of the stack cache, per mthread") = "100";
  ::arg().set("stack-size", "stack size per mthread") = "200000";
  ::arg().set("statistics-interval", "Number of seconds between printing of recursor statistics, 0 to disable") = "1800";
  ::arg().set("stats-api-blacklist", "List of statistics that are disabled when retrieving the complete list of statistics via the API (deprecated)") = "cache-bytes, packetcache-bytes, special-memory-usage, ecs-v4-response-bits-1, ecs-v4-response-bits-2, ecs-v4-response-bits-3, ecs-v4-response-bits-4, ecs-v4-response-bits-5, ecs-v4-response-bits-6, ecs-v4-response-bits-7, ecs-v4-response-bits-8, ecs-v4-response-bits-9, ecs-v4-response-bits-10, ecs-v4-response-bits-11, ecs-v4-response-bits-12, ecs-v4-response-bits-13, ecs-v4-response-bits-14, ecs-v4-response-bits-15, ecs-v4-response-bits-16, ecs-v4-response-bits-17, ecs-v4-response-bits-18, ecs-v4-response-bits-19, ecs-v4-response-bits-20, ecs-v4-response-bits-21, ecs-v4-response-bits-22, ecs-v4-response-bits-23, ecs-v4-response-bits-24, ecs-v4-response-bits-25, ecs-v4-response-bits-26, ecs-v4-response-bits-27, ecs-v4-response-bits-28, ecs-v4-response-bits-29, ecs-v4-response-bits-30, ecs-v4-response-bits-31, ecs-v4-response-bits-32, ecs-v6-response-bits-1, ecs-v6-response-bits-2, ecs-v6-response-bits-3, ecs-v6-response-bits-4, ecs-v6-response-bits-5, ecs-v6-response-bits-6, ecs-v6-response-bits-7, ecs-v6-response-bits-8, ecs-v6-response-bits-9, ecs-v6-response-bits-10, ecs-v6-response-bits-11, ecs-v6-response-bits-12, ecs-v6-response-bits-13, ecs-v6-response-bits-14, ecs-v6-response-bits-15, ecs-v6-response-bits-16, ecs-v6-response-bits-17, ecs-v6-response-bits-18, ecs-v6-response-bits-19, ecs-v6-response-bits-20, ecs-v6-response-bits-21, ecs-v6-response-bits-22, ecs-v6-response-bits-23, ecs-v6-response-bits-24, ecs-v6-response-bits-25, ecs-v6-response-bits-26, ecs-v6-response-bits-27, ecs-v6-response-bits-28, ecs-v6-response-bits-29, ecs-v6-response-bits-30, ecs-v6-response-bits-31, ecs-v6-response-bits-32, ecs-v6-response-bits-33, ecs-v6-response-bits-34, ecs-v6-response-bits-35, ecs-v6-response-bits-36, ecs-v6-response-bits-37, ecs-v6-response-bits-38, ecs-v6-response-bits-39, ecs-v6-response-bits-40, ecs-v6-response-bits-41, ecs-v6-response-bits-42, ecs-v6-response-bits-43, ecs-v6-response-bits-44, ecs-v6-response-bits-45, ecs-v6-response-bits-46, ecs-v6-response-bits-47, ecs-v6-response-bits-48, ecs-v6-response-bits-49, ecs-v6-response-bits-50, ecs-v6-response-bits-51, ecs-v6-response-bits-52, ecs-v6-response-bits-53, ecs-v6-response-bits-54, ecs-v6-response-bits-55, ecs-v6-response-bits-56, ecs-v6-response-bits-57, ecs-v6-response-bits-58, ecs-v6-response-bits-59, ecs-v6-response-bits-60, ecs-v6-response-bits-61, ecs-v6-response-bits-62, ecs-v6-response-bits-63, ecs-v6-response-bits-64, ecs-v6-response-bits-65, ecs-v6-response-bits-66, ecs-v6-response-bits-67, ecs-v6-response-bits-68, ecs-v6-response-bits-69, ecs-v6-response-bits-70, ecs-v6-response-bits-71, ecs-v6-response-bits-72, ecs-v6-response-bits-73, ecs-v6-response-bits-74, ecs-v6-response-bits-75, ecs-v6-response-bits-76, ecs-v6-response-bits-77, ecs-v6-response-bits-78, ecs-v6-response-bits-79, ecs-v6-response-bits-80, ecs-v6-response-bits-81, ecs-v6-response-bits-82, ecs-v6-response-bits-83, ecs-v6-response-bits-84, ecs-v6-response-bits-85, ecs-v6-response-bits-86, ecs-v6-response-bits-87, ecs-v6-response-bits-88, ecs-v6-response-bits-89, ecs-v6-response-bits-90, ecs-v6-response-bits-91, ecs-v6-response-bits-92, ecs-v6-response-bits-93, ecs-v6-response-bits-94, ecs-v6-response-bits-95, ecs-v6-response-bits-96, ecs-v6-response-bits-97, ecs-v6-response-bits-98, ecs-v6-response-bits-99, ecs-v6-response-bits-100, ecs-v6-response-bits-101, ecs-v6-response-bits-102, ecs-v6-response-bits-103, ecs-v6-response-bits-104, ecs-v6-response-bits-105, ecs-v6-response-bits-106, ecs-v6-response-bits-107, ecs-v6-response-bits-108, ecs-v6-response-bits-109, ecs-v6-response-bits-110, ecs-v6-response-bits-111, ecs-v6-response-bits-112, ecs-v6-response-bits-113, ecs-v6-response-bits-114, ecs-v6-response-bits-115, ecs-v6-response-bits-116, ecs-v6-response-bits-117, ecs-v6-response-bits-118, ecs-v6-response-bits-119, ecs-v6-response-bits-120, ecs-v6-response-bits-121, ecs-v6-response-bits-122, ecs-v6-response-bits-123, ecs-v6-response-bits-124, ecs-v6-response-bits-125, ecs-v6-response-bits-126, ecs-v6-response-bits-127, ecs-v6-response-bits-128";
  ::arg().set("stats-api-disabled-list", "List of statistics that are disabled when retrieving the complete list of statistics via the API") = "cache-bytes, packetcache-bytes, special-memory-usage, ecs-v4-response-bits-1, ecs-v4-response-bits-2, ecs-v4-response-bits-3, ecs-v4-response-bits-4, ecs-v4-response-bits-5, ecs-v4-response-bits-6, ecs-v4-response-bits-7, ecs-v4-response-bits-8, ecs-v4-response-bits-9, ecs-v4-response-bits-10, ecs-v4-response-bits-11, ecs-v4-response-bits-12, ecs-v4-response-bits-13, ecs-v4-response-bits-14, ecs-v4-response-bits-15, ecs-v4-response-bits-16, ecs-v4-response-bits-17, ecs-v4-response-bits-18, ecs-v4-response-bits-19, ecs-v4-response-bits-20, ecs-v4-response-bits-21, ecs-v4-response-bits-22, ecs-v4-response-bits-23, ecs-v4-response-bits-24, ecs-v4-response-bits-25, ecs-v4-response-bits-26, ecs-v4-response-bits-27, ecs-v4-response-bits-28, ecs-v4-response-bits-29, ecs-v4-response-bits-30, ecs-v4-response-bits-31, ecs-v4-response-bits-32, ecs-v6-response-bits-1, ecs-v6-response-bits-2, ecs-v6-response-bits-3, ecs-v6-response-bits-4, ecs-v6-response-bits-5, ecs-v6-response-bits-6, ecs-v6-response-bits-7, ecs-v6-response-bits-8, ecs-v6-response-bits-9, ecs-v6-response-bits-10, ecs-v6-response-bits-11, ecs-v6-response-bits-12, ecs-v6-response-bits-13, ecs-v6-response-bits-14, ecs-v6-response-bits-15, ecs-v6-response-bits-16, ecs-v6-response-bits-17, ecs-v6-response-bits-18, ecs-v6-response-bits-19, ecs-v6-response-bits-20, ecs-v6-response-bits-21, ecs-v6-response-bits-22, ecs-v6-response-bits-23, ecs-v6-response-bits-24, ecs-v6-response-bits-25, ecs-v6-response-bits-26, ecs-v6-response-bits-27, ecs-v6-response-bits-28, ecs-v6-response-bits-29, ecs-v6-response-bits-30, ecs-v6-response-bits-31, ecs-v6-response-bits-32, ecs-v6-response-bits-33, ecs-v6-response-bits-34, ecs-v6-response-bits-35, ecs-v6-response-bits-36, ecs-v6-response-bits-37, ecs-v6-response-bits-38, ecs-v6-response-bits-39, ecs-v6-response-bits-40, ecs-v6-response-bits-41, ecs-v6-response-bits-42, ecs-v6-response-bits-43, ecs-v6-response-bits-44, ecs-v6-response-bits-45, ecs-v6-response-bits-46, ecs-v6-response-bits-47, ecs-v6-response-bits-48, ecs-v6-response-bits-49, ecs-v6-response-bits-50, ecs-v6-response-bits-51, ecs-v6-response-bits-52, ecs-v6-response-bits-53, ecs-v6-response-bits-54, ecs-v6-response-bits-55, ecs-v6-response-bits-56, ecs-v6-response-bits-57, ecs-v6-response-bits-58, ecs-v6-response-bits-59, ecs-v6-response-bits-60, ecs-v6-response-bits-61, ecs-v6-response-bits-62, ecs-v6-response-bits-63, ecs-v6-response-bits-64, ecs-v6-response-bits-65, ecs-v6-response-bits-66, ecs-v6-response-bits-67, ecs-v6-response-bits-68, ecs-v6-response-bits-69, ecs-v6-response-bits-70, ecs-v6-response-bits-71, ecs-v6-response-bits-72, ecs-v6-response-bits-73, ecs-v6-response-bits-74, ecs-v6-response-bits-75, ecs-v6-response-bits-76, ecs-v6-response-bits-77, ecs-v6-response-bits-78, ecs-v6-response-bits-79, ecs-v6-response-bits-80, ecs-v6-response-bits-81, ecs-v6-response-bits-82, ecs-v6-response-bits-83, ecs-v6-response-bits-84, ecs-v6-response-bits-85, ecs-v6-response-bits-86, ecs-v6-response-bits-87, ecs-v6-response-bits-88, ecs-v6-response-bits-89, ecs-v6-response-bits-90, ecs-v6-response-bits-91, ecs-v6-response-bits-92, ecs-v6-response-bits-93, ecs-v6-response-bits-94, ecs-v6-response-bits-95, ecs-v6-response-bits-96, ecs-v6-response-bits-97, ecs-v6-response-bits-98, ecs-v6-response-bits-99, ecs-v6-response-bits-100, ecs-v6-response-bits-101, ecs-v6-response-bits-102, ecs-v6-response-bits-103, ecs-v6-response-bits-104, ecs-v6-response-bits-105, ecs-v6-response-bits-106, ecs-v6-response-bits-107, ecs-v6-response-bits-108, ecs-v6-response-bits-109, ecs-v6-response-bits-110, ecs-v6-response-bits-111, ecs-v6-response-bits-112, ecs-v6-response-bits-113, ecs-v6-response-bits-114, ecs-v6-response-bits-115, ecs-v6-response-bits-116, ecs-v6-response-bits-117, ecs-v6-response-bits-118, ecs-v6-response-bits-119, ecs-v6-response-bits-120, ecs-v6-response-bits-121, ecs-v6-response-bits-122, ecs-v6-response-bits-123, ecs-v6-response-bits-124, ecs-v6-response-bits-125, ecs-v6-response-bits-126, ecs-v6-response-bits-127, ecs-v6-response-bits-128";
  ::arg().set("stats-carbon-blacklist", "List of statistics that are prevented from being exported via Carbon (deprecated)") = "cache-bytes, packetcache-bytes, special-memory-usage, ecs-v4-response-bits-1, ecs-v4-response-bits-2, ecs-v4-response-bits-3, ecs-v4-response-bits-4, ecs-v4-response-bits-5, ecs-v4-response-bits-6, ecs-v4-response-bits-7, ecs-v4-response-bits-8, ecs-v4-response-bits-9, ecs-v4-response-bits-10, ecs-v4-response-bits-11, ecs-v4-response-bits-12, ecs-v4-response-bits-13, ecs-v4-response-bits-14, ecs-v4-response-bits-15, ecs-v4-response-bits-16, ecs-v4-response-bits-17, ecs-v4-response-bits-18, ecs-v4-response-bits-19, ecs-v4-response-bits-20, ecs-v4-response-bits-21, ecs-v4-response-bits-22, ecs-v4-response-bits-23, ecs-v4-response-bits-24, ecs-v4-response-bits-25, ecs-v4-response-bits-26, ecs-v4-response-bits-27, ecs-v4-response-bits-28, ecs-v4-response-bits-29, ecs-v4-response-bits-30, ecs-v4-response-bits-31, ecs-v4-response-bits-32, ecs-v6-response-bits-1, ecs-v6-response-bits-2, ecs-v6-response-bits-3, ecs-v6-response-bits-4, ecs-v6-response-bits-5, ecs-v6-response-bits-6, ecs-v6-response-bits-7, ecs-v6-response-bits-8, ecs-v6-response-bits-9, ecs-v6-response-bits-10, ecs-v6-response-bits-11, ecs-v6-response-bits-12, ecs-v6-response-bits-13, ecs-v6-response-bits-14, ecs-v6-response-bits-15, ecs-v6-response-bits-16, ecs-v6-response-bits-17, ecs-v6-response-bits-18, ecs-v6-response-bits-19, ecs-v6-response-bits-20, ecs-v6-response-bits-21, ecs-v6-response-bits-22, ecs-v6-response-bits-23, ecs-v6-response-bits-24, ecs-v6-response-bits-25, ecs-v6-response-bits-26, ecs-v6-response-bits-27, ecs-v6-response-bits-28, ecs-v6-response-bits-29, ecs-v6-response-bits-30, ecs-v6-response-bits-31, ecs-v6-response-bits-32, ecs-v6-response-bits-33, ecs-v6-response-bits-34, ecs-v6-response-bits-35, ecs-v6-response-bits-36, ecs-v6-response-bits-37, ecs-v6-response-bits-38, ecs-v6-response-bits-39, ecs-v6-response-bits-40, ecs-v6-response-bits-41, ecs-v6-response-bits-42, ecs-v6-response-bits-43, ecs-v6-response-bits-44, ecs-v6-response-bits-45, ecs-v6-response-bits-46, ecs-v6-response-bits-47, ecs-v6-response-bits-48, ecs-v6-response-bits-49, ecs-v6-response-bits-50, ecs-v6-response-bits-51, ecs-v6-response-bits-52, ecs-v6-response-bits-53, ecs-v6-response-bits-54, ecs-v6-response-bits-55, ecs-v6-response-bits-56, ecs-v6-response-bits-57, ecs-v6-response-bits-58, ecs-v6-response-bits-59, ecs-v6-response-bits-60, ecs-v6-response-bits-61, ecs-v6-response-bits-62, ecs-v6-response-bits-63, ecs-v6-response-bits-64, ecs-v6-response-bits-65, ecs-v6-response-bits-66, ecs-v6-response-bits-67, ecs-v6-response-bits-68, ecs-v6-response-bits-69, ecs-v6-response-bits-70, ecs-v6-response-bits-71, ecs-v6-response-bits-72, ecs-v6-response-bits-73, ecs-v6-response-bits-74, ecs-v6-response-bits-75, ecs-v6-response-bits-76, ecs-v6-response-bits-77, ecs-v6-response-bits-78, ecs-v6-response-bits-79, ecs-v6-response-bits-80, ecs-v6-response-bits-81, ecs-v6-response-bits-82, ecs-v6-response-bits-83, ecs-v6-response-bits-84, ecs-v6-response-bits-85, ecs-v6-response-bits-86, ecs-v6-response-bits-87, ecs-v6-response-bits-88, ecs-v6-response-bits-89, ecs-v6-response-bits-90, ecs-v6-response-bits-91, ecs-v6-response-bits-92, ecs-v6-response-bits-93, ecs-v6-response-bits-94, ecs-v6-response-bits-95, ecs-v6-response-bits-96, ecs-v6-response-bits-97, ecs-v6-response-bits-98, ecs-v6-response-bits-99, ecs-v6-response-bits-100, ecs-v6-response-bits-101, ecs-v6-response-bits-102, ecs-v6-response-bits-103, ecs-v6-response-bits-104, ecs-v6-response-bits-105, ecs-v6-response-bits-106, ecs-v6-response-bits-107, ecs-v6-response-bits-108, ecs-v6-response-bits-109, ecs-v6-response-bits-110, ecs-v6-response-bits-111, ecs-v6-response-bits-112, ecs-v6-response-bits-113, ecs-v6-response-bits-114, ecs-v6-response-bits-115, ecs-v6-response-bits-116, ecs-v6-response-bits-117, ecs-v6-response-bits-118, ecs-v6-response-bits-119, ecs-v6-response-bits-120, ecs-v6-response-bits-121, ecs-v6-response-bits-122, ecs-v6-response-bits-123, ecs-v6-response-bits-124, ecs-v6-response-bits-125, ecs-v6-response-bits-126, ecs-v6-response-bits-127, ecs-v6-response-bits-128, cumul-clientanswers, cumul-authanswers, policy-hits, proxy-mapping-total, remote-logger-count";
  ::arg().set("stats-carbon-disabled-list", "List of statistics that are prevented from being exported via Carbon") = "cache-bytes, packetcache-bytes, special-memory-usage, ecs-v4-response-bits-1, ecs-v4-response-bits-2, ecs-v4-response-bits-3, ecs-v4-response-bits-4, ecs-v4-response-bits-5, ecs-v4-response-bits-6, ecs-v4-response-bits-7, ecs-v4-response-bits-8, ecs-v4-response-bits-9, ecs-v4-response-bits-10, ecs-v4-response-bits-11, ecs-v4-response-bits-12, ecs-v4-response-bits-13, ecs-v4-response-bits-14, ecs-v4-response-bits-15, ecs-v4-response-bits-16, ecs-v4-response-bits-17, ecs-v4-response-bits-18, ecs-v4-response-bits-19, ecs-v4-response-bits-20, ecs-v4-response-bits-21, ecs-v4-response-bits-22, ecs-v4-response-bits-23, ecs-v4-response-bits-24, ecs-v4-response-bits-25, ecs-v4-response-bits-26, ecs-v4-response-bits-27, ecs-v4-response-bits-28, ecs-v4-response-bits-29, ecs-v4-response-bits-30, ecs-v4-response-bits-31, ecs-v4-response-bits-32, ecs-v6-response-bits-1, ecs-v6-response-bits-2, ecs-v6-response-bits-3, ecs-v6-response-bits-4, ecs-v6-response-bits-5, ecs-v6-response-bits-6, ecs-v6-response-bits-7, ecs-v6-response-bits-8, ecs-v6-response-bits-9, ecs-v6-response-bits-10, ecs-v6-response-bits-11, ecs-v6-response-bits-12, ecs-v6-response-bits-13, ecs-v6-response-bits-14, ecs-v6-response-bits-15, ecs-v6-response-bits-16, ecs-v6-response-bits-17, ecs-v6-response-bits-18, ecs-v6-response-bits-19, ecs-v6-response-bits-20, ecs-v6-response-bits-21, ecs-v6-response-bits-22, ecs-v6-response-bits-23, ecs-v6-response-bits-24, ecs-v6-response-bits-25, ecs-v6-response-bits-26, ecs-v6-response-bits-27, ecs-v6-response-bits-28, ecs-v6-response-bits-29, ecs-v6-response-bits-30, ecs-v6-response-bits-31, ecs-v6-response-bits-32, ecs-v6-response-bits-33, ecs-v6-response-bits-34, ecs-v6-response-bits-35, ecs-v6-response-bits-36, ecs-v6-response-bits-37, ecs-v6-response-bits-38, ecs-v6-response-bits-39, ecs-v6-response-bits-40, ecs-v6-response-bits-41, ecs-v6-response-bits-42, ecs-v6-response-bits-43, ecs-v6-response-bits-44, ecs-v6-response-bits-45, ecs-v6-response-bits-46, ecs-v6-response-bits-47, ecs-v6-response-bits-48, ecs-v6-response-bits-49, ecs-v6-response-bits-50, ecs-v6-response-bits-51, ecs-v6-response-bits-52, ecs-v6-response-bits-53, ecs-v6-response-bits-54, ecs-v6-response-bits-55, ecs-v6-response-bits-56, ecs-v6-response-bits-57, ecs-v6-response-bits-58, ecs-v6-response-bits-59, ecs-v6-response-bits-60, ecs-v6-response-bits-61, ecs-v6-response-bits-62, ecs-v6-response-bits-63, ecs-v6-response-bits-64, ecs-v6-response-bits-65, ecs-v6-response-bits-66, ecs-v6-response-bits-67, ecs-v6-response-bits-68, ecs-v6-response-bits-69, ecs-v6-response-bits-70, ecs-v6-response-bits-71, ecs-v6-response-bits-72, ecs-v6-response-bits-73, ecs-v6-response-bits-74, ecs-v6-response-bits-75, ecs-v6-response-bits-76, ecs-v6-response-bits-77, ecs-v6-response-bits-78, ecs-v6-response-bits-79, ecs-v6-response-bits-80, ecs-v6-response-bits-81, ecs-v6-response-bits-82, ecs-v6-response-bits-83, ecs-v6-response-bits-84, ecs-v6-response-bits-85, ecs-v6-response-bits-86, ecs-v6-response-bits-87, ecs-v6-response-bits-88, ecs-v6-response-bits-89, ecs-v6-response-bits-90, ecs-v6-response-bits-91, ecs-v6-response-bits-92, ecs-v6-response-bits-93, ecs-v6-response-bits-94, ecs-v6-response-bits-95, ecs-v6-response-bits-96, ecs-v6-response-bits-97, ecs-v6-response-bits-98, ecs-v6-response-bits-99, ecs-v6-response-bits-100, ecs-v6-response-bits-101, ecs-v6-response-bits-102, ecs-v6-response-bits-103, ecs-v6-response-bits-104, ecs-v6-response-bits-105, ecs-v6-response-bits-106, ecs-v6-response-bits-107, ecs-v6-response-bits-108, ecs-v6-response-bits-109, ecs-v6-response-bits-110, ecs-v6-response-bits-111, ecs-v6-response-bits-112, ecs-v6-response-bits-113, ecs-v6-response-bits-114, ecs-v6-response-bits-115, ecs-v6-response-bits-116, ecs-v6-response-bits-117, ecs-v6-response-bits-118, ecs-v6-response-bits-119, ecs-v6-response-bits-120, ecs-v6-response-bits-121, ecs-v6-response-bits-122, ecs-v6-response-bits-123, ecs-v6-response-bits-124, ecs-v6-response-bits-125, ecs-v6-response-bits-126, ecs-v6-response-bits-127, ecs-v6-response-bits-128, cumul-clientanswers, cumul-authanswers, policy-hits, proxy-mapping-total, remote-logger-count";
  ::arg().set("stats-rec-control-blacklist", "List of statistics that are prevented from being exported via rec_control get-all (deprecated)") = "cache-bytes, packetcache-bytes, special-memory-usage, ecs-v4-response-bits-1, ecs-v4-response-bits-2, ecs-v4-response-bits-3, ecs-v4-response-bits-4, ecs-v4-response-bits-5, ecs-v4-response-bits-6, ecs-v4-response-bits-7, ecs-v4-response-bits-8, ecs-v4-response-bits-9, ecs-v4-response-bits-10, ecs-v4-response-bits-11, ecs-v4-response-bits-12, ecs-v4-response-bits-13, ecs-v4-response-bits-14, ecs-v4-response-bits-15, ecs-v4-response-bits-16, ecs-v4-response-bits-17, ecs-v4-response-bits-18, ecs-v4-response-bits-19, ecs-v4-response-bits-20, ecs-v4-response-bits-21, ecs-v4-response-bits-22, ecs-v4-response-bits-23, ecs-v4-response-bits-24, ecs-v4-response-bits-25, ecs-v4-response-bits-26, ecs-v4-response-bits-27, ecs-v4-response-bits-28, ecs-v4-response-bits-29, ecs-v4-response-bits-30, ecs-v4-response-bits-31, ecs-v4-response-bits-32, ecs-v6-response-bits-1, ecs-v6-response-bits-2, ecs-v6-response-bits-3, ecs-v6-response-bits-4, ecs-v6-response-bits-5, ecs-v6-response-bits-6, ecs-v6-response-bits-7, ecs-v6-response-bits-8, ecs-v6-response-bits-9, ecs-v6-response-bits-10, ecs-v6-response-bits-11, ecs-v6-response-bits-12, ecs-v6-response-bits-13, ecs-v6-response-bits-14, ecs-v6-response-bits-15, ecs-v6-response-bits-16, ecs-v6-response-bits-17, ecs-v6-response-bits-18, ecs-v6-response-bits-19, ecs-v6-response-bits-20, ecs-v6-response-bits-21, ecs-v6-response-bits-22, ecs-v6-response-bits-23, ecs-v6-response-bits-24, ecs-v6-response-bits-25, ecs-v6-response-bits-26, ecs-v6-response-bits-27, ecs-v6-response-bits-28, ecs-v6-response-bits-29, ecs-v6-response-bits-30, ecs-v6-response-bits-31, ecs-v6-response-bits-32, ecs-v6-response-bits-33, ecs-v6-response-bits-34, ecs-v6-response-bits-35, ecs-v6-response-bits-36, ecs-v6-response-bits-37, ecs-v6-response-bits-38, ecs-v6-response-bits-39, ecs-v6-response-bits-40, ecs-v6-response-bits-41, ecs-v6-response-bits-42, ecs-v6-response-bits-43, ecs-v6-response-bits-44, ecs-v6-response-bits-45, ecs-v6-response-bits-46, ecs-v6-response-bits-47, ecs-v6-response-bits-48, ecs-v6-response-bits-49, ecs-v6-response-bits-50, ecs-v6-response-bits-51, ecs-v6-response-bits-52, ecs-v6-response-bits-53, ecs-v6-response-bits-54, ecs-v6-response-bits-55, ecs-v6-response-bits-56, ecs-v6-response-bits-57, ecs-v6-response-bits-58, ecs-v6-response-bits-59, ecs-v6-response-bits-60, ecs-v6-response-bits-61, ecs-v6-response-bits-62, ecs-v6-response-bits-63, ecs-v6-response-bits-64, ecs-v6-response-bits-65, ecs-v6-response-bits-66, ecs-v6-response-bits-67, ecs-v6-response-bits-68, ecs-v6-response-bits-69, ecs-v6-response-bits-70, ecs-v6-response-bits-71, ecs-v6-response-bits-72, ecs-v6-response-bits-73, ecs-v6-response-bits-74, ecs-v6-response-bits-75, ecs-v6-response-bits-76, ecs-v6-response-bits-77, ecs-v6-response-bits-78, ecs-v6-response-bits-79, ecs-v6-response-bits-80, ecs-v6-response-bits-81, ecs-v6-response-bits-82, ecs-v6-response-bits-83, ecs-v6-response-bits-84, ecs-v6-response-bits-85, ecs-v6-response-bits-86, ecs-v6-response-bits-87, ecs-v6-response-bits-88, ecs-v6-response-bits-89, ecs-v6-response-bits-90, ecs-v6-response-bits-91, ecs-v6-response-bits-92, ecs-v6-response-bits-93, ecs-v6-response-bits-94, ecs-v6-response-bits-95, ecs-v6-response-bits-96, ecs-v6-response-bits-97, ecs-v6-response-bits-98, ecs-v6-response-bits-99, ecs-v6-response-bits-100, ecs-v6-response-bits-101, ecs-v6-response-bits-102, ecs-v6-response-bits-103, ecs-v6-response-bits-104, ecs-v6-response-bits-105, ecs-v6-response-bits-106, ecs-v6-response-bits-107, ecs-v6-response-bits-108, ecs-v6-response-bits-109, ecs-v6-response-bits-110, ecs-v6-response-bits-111, ecs-v6-response-bits-112, ecs-v6-response-bits-113, ecs-v6-response-bits-114, ecs-v6-response-bits-115, ecs-v6-response-bits-116, ecs-v6-response-bits-117, ecs-v6-response-bits-118, ecs-v6-response-bits-119, ecs-v6-response-bits-120, ecs-v6-response-bits-121, ecs-v6-response-bits-122, ecs-v6-response-bits-123, ecs-v6-response-bits-124, ecs-v6-response-bits-125, ecs-v6-response-bits-126, ecs-v6-response-bits-127, ecs-v6-response-bits-128, cumul-clientanswers, cumul-authanswers, policy-hits, proxy-mapping-total, remote-logger-count";
  ::arg().set("stats-rec-control-disabled-list", "List of statistics that are prevented from being exported via rec_control get-all") = "cache-bytes, packetcache-bytes, special-memory-usage, ecs-v4-response-bits-1, ecs-v4-response-bits-2, ecs-v4-response-bits-3, ecs-v4-response-bits-4, ecs-v4-response-bits-5, ecs-v4-response-bits-6, ecs-v4-response-bits-7, ecs-v4-response-bits-8, ecs-v4-response-bits-9, ecs-v4-response-bits-10, ecs-v4-response-bits-11, ecs-v4-response-bits-12, ecs-v4-response-bits-13, ecs-v4-response-bits-14, ecs-v4-response-bits-15, ecs-v4-response-bits-16, ecs-v4-response-bits-17, ecs-v4-response-bits-18, ecs-v4-response-bits-19, ecs-v4-response-bits-20, ecs-v4-response-bits-21, ecs-v4-response-bits-22, ecs-v4-response-bits-23, ecs-v4-response-bits-24, ecs-v4-response-bits-25, ecs-v4-response-bits-26, ecs-v4-response-bits-27, ecs-v4-response-bits-28, ecs-v4-response-bits-29, ecs-v4-response-bits-30, ecs-v4-response-bits-31, ecs-v4-response-bits-32, ecs-v6-response-bits-1, ecs-v6-response-bits-2, ecs-v6-response-bits-3, ecs-v6-response-bits-4, ecs-v6-response-bits-5, ecs-v6-response-bits-6, ecs-v6-response-bits-7, ecs-v6-response-bits-8, ecs-v6-response-bits-9, ecs-v6-response-bits-10, ecs-v6-response-bits-11, ecs-v6-response-bits-12, ecs-v6-response-bits-13, ecs-v6-response-bits-14, ecs-v6-response-bits-15, ecs-v6-response-bits-16, ecs-v6-response-bits-17, ecs-v6-response-bits-18, ecs-v6-response-bits-19, ecs-v6-response-bits-20, ecs-v6-response-bits-21, ecs-v6-response-bits-22, ecs-v6-response-bits-23, ecs-v6-response-bits-24, ecs-v6-response-bits-25, ecs-v6-response-bits-26, ecs-v6-response-bits-27, ecs-v6-response-bits-28, ecs-v6-response-bits-29, ecs-v6-response-bits-30, ecs-v6-response-bits-31, ecs-v6-response-bits-32, ecs-v6-response-bits-33, ecs-v6-response-bits-34, ecs-v6-response-bits-35, ecs-v6-response-bits-36, ecs-v6-response-bits-37, ecs-v6-response-bits-38, ecs-v6-response-bits-39, ecs-v6-response-bits-40, ecs-v6-response-bits-41, ecs-v6-response-bits-42, ecs-v6-response-bits-43, ecs-v6-response-bits-44, ecs-v6-response-bits-45, ecs-v6-response-bits-46, ecs-v6-response-bits-47, ecs-v6-response-bits-48, ecs-v6-response-bits-49, ecs-v6-response-bits-50, ecs-v6-response-bits-51, ecs-v6-response-bits-52, ecs-v6-response-bits-53, ecs-v6-response-bits-54, ecs-v6-response-bits-55, ecs-v6-response-bits-56, ecs-v6-response-bits-57, ecs-v6-response-bits-58, ecs-v6-response-bits-59, ecs-v6-response-bits-60, ecs-v6-response-bits-61, ecs-v6-response-bits-62, ecs-v6-response-bits-63, ecs-v6-response-bits-64, ecs-v6-response-bits-65, ecs-v6-response-bits-66, ecs-v6-response-bits-67, ecs-v6-response-bits-68, ecs-v6-response-bits-69, ecs-v6-response-bits-70, ecs-v6-response-bits-71, ecs-v6-response-bits-72, ecs-v6-response-bits-73, ecs-v6-response-bits-74, ecs-v6-response-bits-75, ecs-v6-response-bits-76, ecs-v6-response-bits-77, ecs-v6-response-bits-78, ecs-v6-response-bits-79, ecs-v6-response-bits-80, ecs-v6-response-bits-81, ecs-v6-response-bits-82, ecs-v6-response-bits-83, ecs-v6-response-bits-84, ecs-v6-response-bits-85, ecs-v6-response-bits-86, ecs-v6-response-bits-87, ecs-v6-response-bits-88, ecs-v6-response-bits-89, ecs-v6-response-bits-90, ecs-v6-response-bits-91, ecs-v6-response-bits-92, ecs-v6-response-bits-93, ecs-v6-response-bits-94, ecs-v6-response-bits-95, ecs-v6-response-bits-96, ecs-v6-response-bits-97, ecs-v6-response-bits-98, ecs-v6-response-bits-99, ecs-v6-response-bits-100, ecs-v6-response-bits-101, ecs-v6-response-bits-102, ecs-v6-response-bits-103, ecs-v6-response-bits-104, ecs-v6-response-bits-105, ecs-v6-response-bits-106, ecs-v6-response-bits-107, ecs-v6-response-bits-108, ecs-v6-response-bits-109, ecs-v6-response-bits-110, ecs-v6-response-bits-111, ecs-v6-response-bits-112, ecs-v6-response-bits-113, ecs-v6-response-bits-114, ecs-v6-response-bits-115, ecs-v6-response-bits-116, ecs-v6-response-bits-117, ecs-v6-response-bits-118, ecs-v6-response-bits-119, ecs-v6-response-bits-120, ecs-v6-response-bits-121, ecs-v6-response-bits-122, ecs-v6-response-bits-123, ecs-v6-response-bits-124, ecs-v6-response-bits-125, ecs-v6-response-bits-126, ecs-v6-response-bits-127, ecs-v6-response-bits-128, cumul-clientanswers, cumul-authanswers, policy-hits, proxy-mapping-total, remote-logger-count";
  ::arg().set("stats-ringbuffer-entries", "maximum number of packets to store statistics for") = "10000";
  ::arg().set("stats-snmp-blacklist", "List of statistics that are prevented from being exported via SNMP (deprecated)") = "cache-bytes, packetcache-bytes, special-memory-usage, ecs-v4-response-bits-1, ecs-v4-response-bits-2, ecs-v4-response-bits-3, ecs-v4-response-bits-4, ecs-v4-response-bits-5, ecs-v4-response-bits-6, ecs-v4-response-bits-7, ecs-v4-response-bits-8, ecs-v4-response-bits-9, ecs-v4-response-bits-10, ecs-v4-response-bits-11, ecs-v4-response-bits-12, ecs-v4-response-bits-13, ecs-v4-response-bits-14, ecs-v4-response-bits-15, ecs-v4-response-bits-16, ecs-v4-response-bits-17, ecs-v4-response-bits-18, ecs-v4-response-bits-19, ecs-v4-response-bits-20, ecs-v4-response-bits-21, ecs-v4-response-bits-22, ecs-v4-response-bits-23, ecs-v4-response-bits-24, ecs-v4-response-bits-25, ecs-v4-response-bits-26, ecs-v4-response-bits-27, ecs-v4-response-bits-28, ecs-v4-response-bits-29, ecs-v4-response-bits-30, ecs-v4-response-bits-31, ecs-v4-response-bits-32, ecs-v6-response-bits-1, ecs-v6-response-bits-2, ecs-v6-response-bits-3, ecs-v6-response-bits-4, ecs-v6-response-bits-5, ecs-v6-response-bits-6, ecs-v6-response-bits-7, ecs-v6-response-bits-8, ecs-v6-response-bits-9, ecs-v6-response-bits-10, ecs-v6-response-bits-11, ecs-v6-response-bits-12, ecs-v6-response-bits-13, ecs-v6-response-bits-14, ecs-v6-response-bits-15, ecs-v6-response-bits-16, ecs-v6-response-bits-17, ecs-v6-response-bits-18, ecs-v6-response-bits-19, ecs-v6-response-bits-20, ecs-v6-response-bits-21, ecs-v6-response-bits-22, ecs-v6-response-bits-23, ecs-v6-response-bits-24, ecs-v6-response-bits-25, ecs-v6-response-bits-26, ecs-v6-response-bits-27, ecs-v6-response-bits-28, ecs-v6-response-bits-29, ecs-v6-response-bits-30, ecs-v6-response-bits-31, ecs-v6-response-bits-32, ecs-v6-response-bits-33, ecs-v6-response-bits-34, ecs-v6-response-bits-35, ecs-v6-response-bits-36, ecs-v6-response-bits-37, ecs-v6-response-bits-38, ecs-v6-response-bits-39, ecs-v6-response-bits-40, ecs-v6-response-bits-41, ecs-v6-response-bits-42, ecs-v6-response-bits-43, ecs-v6-response-bits-44, ecs-v6-response-bits-45, ecs-v6-response-bits-46, ecs-v6-response-bits-47, ecs-v6-response-bits-48, ecs-v6-response-bits-49, ecs-v6-response-bits-50, ecs-v6-response-bits-51, ecs-v6-response-bits-52, ecs-v6-response-bits-53, ecs-v6-response-bits-54, ecs-v6-response-bits-55, ecs-v6-response-bits-56, ecs-v6-response-bits-57, ecs-v6-response-bits-58, ecs-v6-response-bits-59, ecs-v6-response-bits-60, ecs-v6-response-bits-61, ecs-v6-response-bits-62, ecs-v6-response-bits-63, ecs-v6-response-bits-64, ecs-v6-response-bits-65, ecs-v6-response-bits-66, ecs-v6-response-bits-67, ecs-v6-response-bits-68, ecs-v6-response-bits-69, ecs-v6-response-bits-70, ecs-v6-response-bits-71, ecs-v6-response-bits-72, ecs-v6-response-bits-73, ecs-v6-response-bits-74, ecs-v6-response-bits-75, ecs-v6-response-bits-76, ecs-v6-response-bits-77, ecs-v6-response-bits-78, ecs-v6-response-bits-79, ecs-v6-response-bits-80, ecs-v6-response-bits-81, ecs-v6-response-bits-82, ecs-v6-response-bits-83, ecs-v6-response-bits-84, ecs-v6-response-bits-85, ecs-v6-response-bits-86, ecs-v6-response-bits-87, ecs-v6-response-bits-88, ecs-v6-response-bits-89, ecs-v6-response-bits-90, ecs-v6-response-bits-91, ecs-v6-response-bits-92, ecs-v6-response-bits-93, ecs-v6-response-bits-94, ecs-v6-response-bits-95, ecs-v6-response-bits-96, ecs-v6-response-bits-97, ecs-v6-response-bits-98, ecs-v6-response-bits-99, ecs-v6-response-bits-100, ecs-v6-response-bits-101, ecs-v6-response-bits-102, ecs-v6-response-bits-103, ecs-v6-response-bits-104, ecs-v6-response-bits-105, ecs-v6-response-bits-106, ecs-v6-response-bits-107, ecs-v6-response-bits-108, ecs-v6-response-bits-109, ecs-v6-response-bits-110, ecs-v6-response-bits-111, ecs-v6-response-bits-112, ecs-v6-response-bits-113, ecs-v6-response-bits-114, ecs-v6-response-bits-115, ecs-v6-response-bits-116, ecs-v6-response-bits-117, ecs-v6-response-bits-118, ecs-v6-response-bits-119, ecs-v6-response-bits-120, ecs-v6-response-bits-121, ecs-v6-response-bits-122, ecs-v6-response-bits-123, ecs-v6-response-bits-124, ecs-v6-response-bits-125, ecs-v6-response-bits-126, ecs-v6-response-bits-127, ecs-v6-response-bits-128, cumul-clientanswers, cumul-authanswers, policy-hits, proxy-mapping-total, remote-logger-count";
  ::arg().set("stats-snmp-disabled-list", "List of statistics that are prevented from being exported via SNMP") = "cache-bytes, packetcache-bytes, special-memory-usage, ecs-v4-response-bits-1, ecs-v4-response-bits-2, ecs-v4-response-bits-3, ecs-v4-response-bits-4, ecs-v4-response-bits-5, ecs-v4-response-bits-6, ecs-v4-response-bits-7, ecs-v4-response-bits-8, ecs-v4-response-bits-9, ecs-v4-response-bits-10, ecs-v4-response-bits-11, ecs-v4-response-bits-12, ecs-v4-response-bits-13, ecs-v4-response-bits-14, ecs-v4-response-bits-15, ecs-v4-response-bits-16, ecs-v4-response-bits-17, ecs-v4-response-bits-18, ecs-v4-response-bits-19, ecs-v4-response-bits-20, ecs-v4-response-bits-21, ecs-v4-response-bits-22, ecs-v4-response-bits-23, ecs-v4-response-bits-24, ecs-v4-response-bits-25, ecs-v4-response-bits-26, ecs-v4-response-bits-27, ecs-v4-response-bits-28, ecs-v4-response-bits-29, ecs-v4-response-bits-30, ecs-v4-response-bits-31, ecs-v4-response-bits-32, ecs-v6-response-bits-1, ecs-v6-response-bits-2, ecs-v6-response-bits-3, ecs-v6-response-bits-4, ecs-v6-response-bits-5, ecs-v6-response-bits-6, ecs-v6-response-bits-7, ecs-v6-response-bits-8, ecs-v6-response-bits-9, ecs-v6-response-bits-10, ecs-v6-response-bits-11, ecs-v6-response-bits-12, ecs-v6-response-bits-13, ecs-v6-response-bits-14, ecs-v6-response-bits-15, ecs-v6-response-bits-16, ecs-v6-response-bits-17, ecs-v6-response-bits-18, ecs-v6-response-bits-19, ecs-v6-response-bits-20, ecs-v6-response-bits-21, ecs-v6-response-bits-22, ecs-v6-response-bits-23, ecs-v6-response-bits-24, ecs-v6-response-bits-25, ecs-v6-response-bits-26, ecs-v6-response-bits-27, ecs-v6-response-bits-28, ecs-v6-response-bits-29, ecs-v6-response-bits-30, ecs-v6-response-bits-31, ecs-v6-response-bits-32, ecs-v6-response-bits-33, ecs-v6-response-bits-34, ecs-v6-response-bits-35, ecs-v6-response-bits-36, ecs-v6-response-bits-37, ecs-v6-response-bits-38, ecs-v6-response-bits-39, ecs-v6-response-bits-40, ecs-v6-response-bits-41, ecs-v6-response-bits-42, ecs-v6-response-bits-43, ecs-v6-response-bits-44, ecs-v6-response-bits-45, ecs-v6-response-bits-46, ecs-v6-response-bits-47, ecs-v6-response-bits-48, ecs-v6-response-bits-49, ecs-v6-response-bits-50, ecs-v6-response-bits-51, ecs-v6-response-bits-52, ecs-v6-response-bits-53, ecs-v6-response-bits-54, ecs-v6-response-bits-55, ecs-v6-response-bits-56, ecs-v6-response-bits-57, ecs-v6-response-bits-58, ecs-v6-response-bits-59, ecs-v6-response-bits-60, ecs-v6-response-bits-61, ecs-v6-response-bits-62, ecs-v6-response-bits-63, ecs-v6-response-bits-64, ecs-v6-response-bits-65, ecs-v6-response-bits-66, ecs-v6-response-bits-67, ecs-v6-response-bits-68, ecs-v6-response-bits-69, ecs-v6-response-bits-70, ecs-v6-response-bits-71, ecs-v6-response-bits-72, ecs-v6-response-bits-73, ecs-v6-response-bits-74, ecs-v6-response-bits-75, ecs-v6-response-bits-76, ecs-v6-response-bits-77, ecs-v6-response-bits-78, ecs-v6-response-bits-79, ecs-v6-response-bits-80, ecs-v6-response-bits-81, ecs-v6-response-bits-82, ecs-v6-response-bits-83, ecs-v6-response-bits-84, ecs-v6-response-bits-85, ecs-v6-response-bits-86, ecs-v6-response-bits-87, ecs-v6-response-bits-88, ecs-v6-response-bits-89, ecs-v6-response-bits-90, ecs-v6-response-bits-91, ecs-v6-response-bits-92, ecs-v6-response-bits-93, ecs-v6-response-bits-94, ecs-v6-response-bits-95, ecs-v6-response-bits-96, ecs-v6-response-bits-97, ecs-v6-response-bits-98, ecs-v6-response-bits-99, ecs-v6-response-bits-100, ecs-v6-response-bits-101, ecs-v6-response-bits-102, ecs-v6-response-bits-103, ecs-v6-response-bits-104, ecs-v6-response-bits-105, ecs-v6-response-bits-106, ecs-v6-response-bits-107, ecs-v6-response-bits-108, ecs-v6-response-bits-109, ecs-v6-response-bits-110, ecs-v6-response-bits-111, ecs-v6-response-bits-112, ecs-v6-response-bits-113, ecs-v6-response-bits-114, ecs-v6-response-bits-115, ecs-v6-response-bits-116, ecs-v6-response-bits-117, ecs-v6-response-bits-118, ecs-v6-response-bits-119, ecs-v6-response-bits-120, ecs-v6-response-bits-121, ecs-v6-response-bits-122, ecs-v6-response-bits-123, ecs-v6-response-bits-124, ecs-v6-response-bits-125, ecs-v6-response-bits-126, ecs-v6-response-bits-127, ecs-v6-response-bits-128, cumul-clientanswers, cumul-authanswers, policy-hits, proxy-mapping-total, remote-logger-count";
  ::arg().setSwitch("structured-logging", "Prefer structured logging") = "yes";
  ::arg().set("structured-logging-backend", "Structured logging backend") = "default";
  ::arg().set("tcp-fast-open", "Enable TCP Fast Open support on the listening sockets, using the supplied numerical value as the queue size") = "0";
  ::arg().setSwitch("tcp-fast-open-connect", "Enable TCP Fast Open support on outgoing sockets") = "no";
  ::arg().set("tcp-out-max-idle-ms", "Time TCP/DoT connections are left idle in milliseconds or 0 if no limit") = "10000";
  ::arg().set("tcp-out-max-idle-per-auth", "Maximum number of idle TCP/DoT connections to a specific IP per thread, 0 means do not keep idle connections open") = "10";
  ::arg().set("tcp-out-max-queries", "Maximum total number of queries per TCP/DoT connection, 0 means no limit") = "0";
  ::arg().set("tcp-out-max-idle-per-thread", "Maximum number of idle TCP/DoT connections per thread") = "100";
  ::arg().set("threads", "Launch this number of threads") = "2";
  ::arg().set("tcp-threads", "Launch this number of threads listening for and processing TCP queries") = "1";
  ::arg().set("trace", "if we should output heaps of logging. set to 'fail' to only log failing domains") = "no";
  ::arg().set("udp-source-port-min", "Minimum UDP port to bind on") = "1024";
  ::arg().set("udp-source-port-max", "Maximum UDP port to bind on") = "65535";
  ::arg().set("udp-source-port-avoid", "List of comma separated UDP port numbers to avoid") = "4791,11211";
  ::arg().set("udp-truncation-threshold", "Maximum UDP response size before we truncate") = "1232";
  ::arg().setSwitch("unique-response-tracking", "Track unique responses (tuple of query name, type and RR).") = "no";
  ::arg().setSwitch("unique-response-log", "Log unique responses") = "yes";
  ::arg().set("unique-response-db-size", "Size of the DB used to track unique responses in terms of number of cells. Defaults to 67108864") = "67108864";
  ::arg().set("unique-response-history-dir", "Persist unique response tracking data here to persist between restarts") = NODCACHEDIRUDR;
  ::arg().set("unique-response-pb-tag", "If protobuf is configured, the tag to use for messages containing unique DNS responses. Defaults to 'pdns-udr'") = "pdns-udr";
  ::arg().set("unique-response-ignore-list", "List of domains (and implicitly all subdomains) which will never be considered for UDR") = "";
  ::arg().set("unique-response-ignore-list-file", "File with list of domains (and implicitly all subdomains) which will never be considered for UDR") = "";
  ::arg().setSwitch("use-incoming-edns-subnet", "Pass along received EDNS Client Subnet information") = "no";
  ::arg().setCmd("version", "Print version string");
  ::arg().set("version-string", "string reported on version.pdns or version.bind") = "*runtime determined*";
  ::arg().setSwitch("webserver", "Start a webserver (for REST API)") = "no";
  ::arg().set("webserver-address", "IP Address of webserver to listen on") = "127.0.0.1";
  ::arg().set("webserver-allow-from", "Webserver access is only allowed from these subnets") = "127.0.0.1, ::1";
  ::arg().setSwitch("webserver-hash-plaintext-credentials", "Whether to hash passwords and api keys supplied in plaintext, to prevent keeping the plaintext version in memory at runtime") = "no";
  ::arg().set("webserver-loglevel", "Amount of logging in the webserver (none, normal, detailed)") = "normal";
  ::arg().set("webserver-password", "Password required for accessing the webserver") = "";
  ::arg().set("webserver-port", "Port of webserver to listen on") = "8082";
  ::arg().setSwitch("write-pid", "Write a PID file") = "yes";
  ::arg().set("x-dnssec-names", "Collect DNSSEC statistics for names or suffixes in this list in separate x-dnssec counters") = "";
  ::arg().set("system-resolver-ttl", "Set TTL of system resolver feature, 0 (default) is disabled") = "0";
  ::arg().set("system-resolver-interval", "Set interval (in seconds) of the re-resolve checks of system resolver subsystem.") = "0";
  ::arg().setSwitch("system-resolver-self-resolve-check", "Check for potential self-resolve, default enabled.") = "yes";
  ::arg().set("trustanchors", "Sequence of trust anchors") = "[{name: ., dsrecords: ['20326 8 2 e06d44b80b8f1d39a95c0b0d7c65d08458e880409bbc683457104237c7f8ec8d', '38696 8 2 683d2d0acb8c9b712a1948b27f741219298d0a450d612c483af444a4c0fb2b16']}]";
  ::arg().set("negative-trustanchors", "A sequence of negative trust anchors") = "";
  ::arg().set("trustanchorfile", "A path to a zone file containing trust anchors") = "";
  ::arg().set("trustanchorfile-interval", "Interval (in hours) to read the trust anchors file") = "24";
  ::arg().set("protobuf-servers", "Sequence of protobuf servers") = "";
  ::arg().set("outgoing-protobuf-servers", "List of outgoing protobuf servers") = "";
  ::arg().set("protobuf-mask-v4", "Network mask to apply for client IPv4 addresses in protobuf messages") = "32";
  ::arg().set("protobuf-mask-v6", "Network mask to apply for client IPv6 addresses in protobuf messages") = "128";
  ::arg().set("dnstap-framestream-servers", "Sequence of dnstap servers") = "";
  ::arg().set("dnstap-nod-framestream-servers", "Sequence of NOD dnstap servers") = "";
  ::arg().set("sortlists", "Sequence of sort lists") = "";
  ::arg().set("rpzs", "Sequence of RPZ entries") = "";
  ::arg().set("zonetocaches", "Sequence of ZoneToCache entries ") = "";
  ::arg().set("allowed-additional-qtypes", "Sequence of AllowedAdditionalQType") = "";
  ::arg().set("proxymappings", "Sequence of ProxyMapping") = "";
  ::arg().set("lua-start-stop-script", "Lua script containing functions to run on startup and shutdown") = "";
  ::arg().set("forwarding-catalog-zones", "Sequence of ForwardingCatalogZone") = "";
}

void pdns::settings::rec::oldStyleSettingsToBridgeStruct(Recursorsettings& settings)
{
  settings.dnssec.aggressive_nsec_cache_size = static_cast<uint64_t>(arg().asNum("aggressive-nsec-cache-size"));
  settings.dnssec.aggressive_cache_min_nsec3_hit_ratio = static_cast<uint64_t>(arg().asNum("aggressive-cache-min-nsec3-hit-ratio"));
  settings.incoming.allow_from = getStrings("allow-from");
  settings.incoming.allow_from_file = arg()["allow-from-file"];
  settings.incoming.allow_notify_for = getStrings("allow-notify-for");
  settings.incoming.allow_notify_for_file = arg()["allow-notify-for-file"];
  settings.incoming.allow_notify_from = getStrings("allow-notify-from");
  settings.incoming.allow_notify_from_file = arg()["allow-notify-from-file"];
  settings.incoming.allow_no_rd = arg().mustDo("allow-no-rd");
  settings.recursor.any_to_tcp = arg().mustDo("any-to-tcp");
  settings.recursor.allow_trust_anchor_query = arg().mustDo("allow-trust-anchor-query");
  settings.webservice.api_dir = arg()["api-config-dir"];
  settings.webservice.api_key = arg()["api-key"];
  settings.recursor.auth_zones = getAuthZones("auth-zones");
  settings.carbon.interval = static_cast<uint64_t>(arg().asNum("carbon-interval"));
  settings.carbon.ns = arg()["carbon-namespace"];
  settings.carbon.ourname = arg()["carbon-ourname"];
  settings.carbon.instance = arg()["carbon-instance"];
  settings.carbon.server = getStrings("carbon-server");
  settings.recursor.chroot = arg()["chroot"];
  settings.incoming.tcp_timeout = static_cast<uint64_t>(arg().asNum("client-tcp-timeout"));
  settings.recursor.config_dir = arg()["config-dir"];
  settings.recursor.config_name = arg()["config-name"];
  settings.recursor.cpu_map = arg()["cpu-map"];
  settings.recursor.daemon = arg().mustDo("daemon");
  settings.outgoing.dont_throttle_names = getStrings("dont-throttle-names");
  settings.outgoing.dont_throttle_netmasks = getStrings("dont-throttle-netmasks");
  settings.recursor.devonly_regression_test_mode = arg().mustDo("devonly-regression-test-mode");
  settings.packetcache.disable = arg().mustDo("disable-packetcache");
  settings.logging.disable_syslog = arg().mustDo("disable-syslog");
  settings.incoming.distribution_load_factor = arg().asDouble("distribution-load-factor");
  settings.incoming.distribution_pipe_buffer_size = static_cast<uint64_t>(arg().asNum("distribution-pipe-buffer-size"));
  settings.incoming.distributor_threads = static_cast<uint64_t>(arg().asNum("distributor-threads"));
  settings.outgoing.dot_to_auth_names = getStrings("dot-to-auth-names");
  settings.outgoing.dot_to_port_853 = arg().mustDo("dot-to-port-853");
  settings.recursor.dns64_prefix = arg()["dns64-prefix"];
  settings.dnssec.validation = arg()["dnssec"];
  settings.dnssec.disabled_algorithms = getStrings("dnssec-disabled-algorithms");
  settings.dnssec.log_bogus = arg().mustDo("dnssec-log-bogus");
  settings.outgoing.dont_query = getStrings("dont-query");
  settings.ecs.add_for = getStrings("ecs-add-for");
  settings.ecs.ipv4_bits = static_cast<uint64_t>(arg().asNum("ecs-ipv4-bits"));
  settings.ecs.ipv4_cache_bits = static_cast<uint64_t>(arg().asNum("ecs-ipv4-cache-bits"));
  settings.ecs.ipv6_bits = static_cast<uint64_t>(arg().asNum("ecs-ipv6-bits"));
  settings.ecs.ipv6_cache_bits = static_cast<uint64_t>(arg().asNum("ecs-ipv6-cache-bits"));
  settings.ecs.ipv4_never_cache = arg().mustDo("ecs-ipv4-never-cache");
  settings.ecs.ipv6_never_cache = arg().mustDo("ecs-ipv6-never-cache");
  settings.ecs.minimum_ttl_override = static_cast<uint64_t>(arg().asNum("ecs-minimum-ttl-override"));
  settings.ecs.cache_limit_ttl = static_cast<uint64_t>(arg().asNum("ecs-cache-limit-ttl"));
  settings.ecs.scope_zero_address = arg()["ecs-scope-zero-address"];
  settings.outgoing.edns_bufsize = static_cast<uint64_t>(arg().asNum("edns-outgoing-bufsize"));
  settings.incoming.edns_padding_from = getStrings("edns-padding-from");
  settings.incoming.edns_padding_mode = arg()["edns-padding-mode"];
  settings.outgoing.edns_padding = arg().mustDo("edns-padding-out");
  settings.incoming.edns_padding_tag = static_cast<uint64_t>(arg().asNum("edns-padding-tag"));
  settings.outgoing.edns_subnet_allow_list = getStrings("edns-subnet-allow-list");
  settings.recursor.etc_hosts_file = arg()["etc-hosts-file"];
  settings.recursor.event_trace_enabled = static_cast<uint64_t>(arg().asNum("event-trace-enabled"));
  settings.recursor.export_etc_hosts = arg().mustDo("export-etc-hosts");
  settings.recursor.export_etc_hosts_search_suffix = arg()["export-etc-hosts-search-suffix"];
  settings.recursor.extended_resolution_errors = arg().mustDo("extended-resolution-errors");
  settings.recursor.forward_zones = getForwardZones("forward-zones");
  settings.recursor.forward_zones_file = arg()["forward-zones-file"];
  settings.recursor.forward_zones_recurse = getForwardZones("forward-zones-recurse");
  settings.incoming.gettag_needs_edns_options = arg().mustDo("gettag-needs-edns-options");
  settings.recursor.hint_file = arg()["hint-file"];
  settings.recursor.ignore_unknown_settings = getStrings("ignore-unknown-settings");
  settings.recursor.include_dir = arg()["include-dir"];
  settings.recursor.latency_statistic_size = static_cast<uint64_t>(arg().asNum("latency-statistic-size"));
  settings.incoming.listen = getStrings("local-address");
  settings.incoming.port = static_cast<uint64_t>(arg().asNum("local-port"));
  settings.logging.timestamp = arg().mustDo("log-timestamp");
  settings.incoming.non_local_bind = arg().mustDo("non-local-bind");
  settings.logging.loglevel = static_cast<uint64_t>(arg().asNum("loglevel"));
  settings.logging.common_errors = arg().mustDo("log-common-errors");
  settings.logging.rpz_changes = arg().mustDo("log-rpz-changes");
  settings.logging.facility = arg()["logging-facility"];
  settings.outgoing.lowercase = arg().mustDo("lowercase-outgoing");
  settings.recursor.lua_config_file = arg()["lua-config-file"];
  settings.recursor.lua_global_include_dir = arg()["lua-global-include-dir"];
  settings.recursor.lua_dns_script = arg()["lua-dns-script"];
  settings.recursor.lua_maintenance_interval = static_cast<uint64_t>(arg().asNum("lua-maintenance-interval"));
  settings.outgoing.max_busy_dot_probes = static_cast<uint64_t>(arg().asNum("max-busy-dot-probes"));
  settings.recordcache.max_cache_bogus_ttl = static_cast<uint64_t>(arg().asNum("max-cache-bogus-ttl"));
  settings.recordcache.max_entries = static_cast<uint64_t>(arg().asNum("max-cache-entries"));
  settings.recordcache.max_ttl = static_cast<uint64_t>(arg().asNum("max-cache-ttl"));
  settings.incoming.max_concurrent_requests_per_tcp_connection = static_cast<uint64_t>(arg().asNum("max-concurrent-requests-per-tcp-connection"));
  settings.recursor.max_chain_length = static_cast<uint64_t>(arg().asNum("max-chain-length"));
  settings.recursor.max_include_depth = static_cast<uint64_t>(arg().asNum("max-include-depth"));
  settings.recursor.max_generate_steps = static_cast<uint64_t>(arg().asNum("max-generate-steps"));
  settings.recursor.max_mthreads = static_cast<uint64_t>(arg().asNum("max-mthreads"));
  settings.packetcache.max_entries = static_cast<uint64_t>(arg().asNum("max-packetcache-entries"));
  settings.outgoing.max_qperq = static_cast<uint64_t>(arg().asNum("max-qperq"));
  settings.recursor.max_cnames_followed = static_cast<uint64_t>(arg().asNum("max-cnames-followed"));
  settings.recordcache.limit_qtype_any = arg().mustDo("limit-qtype-any");
  settings.recordcache.max_rrset_size = static_cast<uint64_t>(arg().asNum("max-rrset-size"));
  settings.outgoing.max_ns_address_qperq = static_cast<uint64_t>(arg().asNum("max-ns-address-qperq"));
  settings.outgoing.max_ns_per_resolve = static_cast<uint64_t>(arg().asNum("max-ns-per-resolve"));
  settings.recordcache.max_negative_ttl = static_cast<uint64_t>(arg().asNum("max-negative-ttl"));
  settings.recursor.max_recursion_depth = static_cast<uint64_t>(arg().asNum("max-recursion-depth"));
  settings.incoming.max_tcp_clients = static_cast<uint64_t>(arg().asNum("max-tcp-clients"));
  settings.incoming.max_tcp_per_client = static_cast<uint64_t>(arg().asNum("max-tcp-per-client"));
  settings.incoming.max_tcp_queries_per_connection = static_cast<uint64_t>(arg().asNum("max-tcp-queries-per-connection"));
  settings.recursor.max_total_msec = static_cast<uint64_t>(arg().asNum("max-total-msec"));
  settings.incoming.max_udp_queries_per_round = static_cast<uint64_t>(arg().asNum("max-udp-queries-per-round"));
  settings.recursor.minimum_ttl_override = static_cast<uint64_t>(arg().asNum("minimum-ttl-override"));
  settings.nod.tracking = arg().mustDo("new-domain-tracking");
  settings.nod.log = arg().mustDo("new-domain-log");
  settings.nod.lookup = arg()["new-domain-lookup"];
  settings.nod.db_size = static_cast<uint64_t>(arg().asNum("new-domain-db-size"));
  settings.nod.history_dir = arg()["new-domain-history-dir"];
  settings.nod.db_snapshot_interval = static_cast<uint64_t>(arg().asNum("new-domain-db-snapshot-interval"));
  settings.nod.ignore_list = getStrings("new-domain-ignore-list");
  settings.nod.ignore_list_file = arg()["new-domain-ignore-list-file"];
  settings.nod.pb_tag = arg()["new-domain-pb-tag"];
  settings.outgoing.network_timeout = static_cast<uint64_t>(arg().asNum("network-timeout"));
  settings.outgoing.non_resolving_ns_max_fails = static_cast<uint64_t>(arg().asNum("non-resolving-ns-max-fails"));
  settings.outgoing.non_resolving_ns_throttle_time = static_cast<uint64_t>(arg().asNum("non-resolving-ns-throttle-time"));
  settings.recursor.nothing_below_nxdomain = arg()["nothing-below-nxdomain"];
  settings.dnssec.nsec3_max_iterations = static_cast<uint64_t>(arg().asNum("nsec3-max-iterations"));
  settings.dnssec.max_rrsigs_per_record = static_cast<uint64_t>(arg().asNum("max-rrsigs-per-record"));
  settings.dnssec.max_nsec3s_per_record = static_cast<uint64_t>(arg().asNum("max-nsec3s-per-record"));
  settings.dnssec.max_signature_validations_per_query = static_cast<uint64_t>(arg().asNum("max-signature-validations-per-query"));
  settings.dnssec.max_nsec3_hash_computations_per_query = static_cast<uint64_t>(arg().asNum("max-nsec3-hash-computations-per-query"));
  settings.dnssec.aggressive_cache_max_nsec3_hash_cost = static_cast<uint64_t>(arg().asNum("aggressive-cache-max-nsec3-hash-cost"));
  settings.dnssec.max_ds_per_zone = static_cast<uint64_t>(arg().asNum("max-ds-per-zone"));
  settings.dnssec.max_dnskeys = static_cast<uint64_t>(arg().asNum("max-dnskeys"));
  settings.packetcache.ttl = static_cast<uint64_t>(arg().asNum("packetcache-ttl"));
  settings.packetcache.negative_ttl = static_cast<uint64_t>(arg().asNum("packetcache-negative-ttl"));
  settings.packetcache.servfail_ttl = static_cast<uint64_t>(arg().asNum("packetcache-servfail-ttl"));
  settings.packetcache.shards = static_cast<uint64_t>(arg().asNum("packetcache-shards"));
  settings.incoming.pdns_distributes_queries = arg().mustDo("pdns-distributes-queries");
  settings.logging.protobuf_use_kernel_timestamp = arg().mustDo("protobuf-use-kernel-timestamp");
  settings.incoming.proxy_protocol_from = getStrings("proxy-protocol-from");
  settings.incoming.proxy_protocol_exceptions = getStrings("proxy-protocol-exceptions");
  settings.incoming.proxy_protocol_maximum_size = static_cast<uint64_t>(arg().asNum("proxy-protocol-maximum-size"));
  settings.recursor.public_suffix_list_file = arg()["public-suffix-list-file"];
  settings.recursor.qname_minimization = arg().mustDo("qname-minimization");
  settings.recursor.qname_max_minimize_count = static_cast<uint64_t>(arg().asNum("qname-max-minimize-count"));
  settings.recursor.qname_minimize_one_label = static_cast<uint64_t>(arg().asNum("qname-minimize-one-label"));
  settings.outgoing.source_address = getStrings("query-local-address");
  settings.logging.quiet = arg().mustDo("quiet");
  settings.recordcache.locked_ttl_perc = static_cast<uint64_t>(arg().asNum("record-cache-locked-ttl-perc"));
  settings.recordcache.shards = static_cast<uint64_t>(arg().asNum("record-cache-shards"));
  settings.recordcache.refresh_on_ttl_perc = static_cast<uint64_t>(arg().asNum("refresh-on-ttl-perc"));
  settings.incoming.reuseport = arg().mustDo("reuseport");
  settings.recursor.root_nx_trust = arg().mustDo("root-nx-trust");
  settings.recursor.save_parent_ns_set = arg().mustDo("save-parent-ns-set");
  settings.recursor.security_poll_suffix = arg()["security-poll-suffix"];
  settings.recursor.serve_rfc1918 = arg().mustDo("serve-rfc1918");
  settings.recursor.serve_rfc6303 = arg().mustDo("serve-rfc6303");
  settings.recordcache.serve_stale_extensions = static_cast<uint64_t>(arg().asNum("serve-stale-extensions"));
  settings.outgoing.server_down_max_fails = static_cast<uint64_t>(arg().asNum("server-down-max-fails"));
  settings.outgoing.server_down_throttle_time = static_cast<uint64_t>(arg().asNum("server-down-throttle-time"));
  settings.outgoing.bypass_server_throttling_probability = static_cast<uint64_t>(arg().asNum("bypass-server-throttling-probability"));
  settings.recursor.server_id = arg()["server-id"];
  settings.recursor.setgid = arg()["setgid"];
  settings.recursor.setuid = arg()["setuid"];
  settings.dnssec.signature_inception_skew = static_cast<uint64_t>(arg().asNum("signature-inception-skew"));
  settings.outgoing.single_socket = arg().mustDo("single-socket");
  settings.snmp.agent = arg().mustDo("snmp-agent");
  settings.snmp.daemon_socket = arg()["snmp-daemon-socket"];
  settings.recursor.socket_dir = arg()["socket-dir"];
  settings.recursor.socket_group = arg()["socket-group"];
  settings.recursor.socket_mode = arg()["socket-mode"];
  settings.recursor.socket_owner = arg()["socket-owner"];
  settings.recursor.spoof_nearmiss_max = static_cast<uint64_t>(arg().asNum("spoof-nearmiss-max"));
  settings.recursor.stack_cache_size = static_cast<uint64_t>(arg().asNum("stack-cache-size"));
  settings.recursor.stack_size = static_cast<uint64_t>(arg().asNum("stack-size"));
  settings.logging.statistics_interval = static_cast<uint64_t>(arg().asNum("statistics-interval"));
  settings.recursor.stats_api_disabled_list = getStrings("stats-api-disabled-list");
  settings.recursor.stats_carbon_disabled_list = getStrings("stats-carbon-disabled-list");
  settings.recursor.stats_rec_control_disabled_list = getStrings("stats-rec-control-disabled-list");
  settings.recursor.stats_ringbuffer_entries = static_cast<uint64_t>(arg().asNum("stats-ringbuffer-entries"));
  settings.recursor.stats_snmp_disabled_list = getStrings("stats-snmp-disabled-list");
  settings.logging.structured_logging = arg().mustDo("structured-logging");
  settings.logging.structured_logging_backend = arg()["structured-logging-backend"];
  settings.incoming.tcp_fast_open = static_cast<uint64_t>(arg().asNum("tcp-fast-open"));
  settings.outgoing.tcp_fast_open_connect = arg().mustDo("tcp-fast-open-connect");
  settings.outgoing.tcp_max_idle_ms = static_cast<uint64_t>(arg().asNum("tcp-out-max-idle-ms"));
  settings.outgoing.tcp_max_idle_per_auth = static_cast<uint64_t>(arg().asNum("tcp-out-max-idle-per-auth"));
  settings.outgoing.tcp_max_queries = static_cast<uint64_t>(arg().asNum("tcp-out-max-queries"));
  settings.outgoing.tcp_max_idle_per_thread = static_cast<uint64_t>(arg().asNum("tcp-out-max-idle-per-thread"));
  settings.recursor.threads = static_cast<uint64_t>(arg().asNum("threads"));
  settings.recursor.tcp_threads = static_cast<uint64_t>(arg().asNum("tcp-threads"));
  settings.logging.trace = arg()["trace"];
  settings.outgoing.udp_source_port_min = static_cast<uint64_t>(arg().asNum("udp-source-port-min"));
  settings.outgoing.udp_source_port_max = static_cast<uint64_t>(arg().asNum("udp-source-port-max"));
  settings.outgoing.udp_source_port_avoid = getStrings("udp-source-port-avoid");
  settings.incoming.udp_truncation_threshold = static_cast<uint64_t>(arg().asNum("udp-truncation-threshold"));
  settings.nod.unique_response_tracking = arg().mustDo("unique-response-tracking");
  settings.nod.unique_response_log = arg().mustDo("unique-response-log");
  settings.nod.unique_response_db_size = static_cast<uint64_t>(arg().asNum("unique-response-db-size"));
  settings.nod.unique_response_history_dir = arg()["unique-response-history-dir"];
  settings.nod.unique_response_pb_tag = arg()["unique-response-pb-tag"];
  settings.nod.unique_response_ignore_list = getStrings("unique-response-ignore-list");
  settings.nod.unique_response_ignore_list_file = arg()["unique-response-ignore-list-file"];
  settings.incoming.use_incoming_edns_subnet = arg().mustDo("use-incoming-edns-subnet");
  settings.recursor.version_string = arg()["version-string"];
  settings.webservice.webserver = arg().mustDo("webserver");
  settings.webservice.address = arg()["webserver-address"];
  settings.webservice.allow_from = getStrings("webserver-allow-from");
  settings.webservice.hash_plaintext_credentials = arg().mustDo("webserver-hash-plaintext-credentials");
  settings.webservice.loglevel = arg()["webserver-loglevel"];
  settings.webservice.password = arg()["webserver-password"];
  settings.webservice.port = static_cast<uint64_t>(arg().asNum("webserver-port"));
  settings.recursor.write_pid = arg().mustDo("write-pid");
  settings.dnssec.x_dnssec_names = getStrings("x-dnssec-names");
  settings.recursor.system_resolver_ttl = static_cast<uint64_t>(arg().asNum("system-resolver-ttl"));
  settings.recursor.system_resolver_interval = static_cast<uint64_t>(arg().asNum("system-resolver-interval"));
  settings.recursor.system_resolver_self_resolve_check = arg().mustDo("system-resolver-self-resolve-check");
}

// Inefficient, but only meant to be used for one-time conversion purposes
bool pdns::settings::rec::oldKVToBridgeStruct(std::string& key, const std::string& value, ::rust::String& section, ::rust::String& fieldname, ::rust::String& type_name, pdns::rust::settings::rec::Value& rustvalue){ // NOLINT(readability-function-cognitive-complexity)
  if (const auto newname = arg().isDeprecated(key); !newname.empty()) {
    key = newname;
  }
  if (key == "aggressive-nsec-cache-size") {
    section = "dnssec";
    fieldname = "aggressive_nsec_cache_size";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "aggressive-cache-min-nsec3-hit-ratio") {
    section = "dnssec";
    fieldname = "aggressive_cache_min_nsec3_hit_ratio";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "allow-from") {
    section = "incoming";
    fieldname = "allow_from";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "allow-from-file") {
    section = "incoming";
    fieldname = "allow_from_file";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "allow-notify-for") {
    section = "incoming";
    fieldname = "allow_notify_for";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "allow-notify-for-file") {
    section = "incoming";
    fieldname = "allow_notify_for_file";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "allow-notify-from") {
    section = "incoming";
    fieldname = "allow_notify_from";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "allow-notify-from-file") {
    section = "incoming";
    fieldname = "allow_notify_from_file";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "allow-no-rd") {
    section = "incoming";
    fieldname = "allow_no_rd";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "any-to-tcp") {
    section = "recursor";
    fieldname = "any_to_tcp";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "allow-trust-anchor-query") {
    section = "recursor";
    fieldname = "allow_trust_anchor_query";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "api-config-dir") {
    section = "webservice";
    fieldname = "api_dir";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "api-key") {
    section = "webservice";
    fieldname = "api_key";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "auth-zones") {
    section = "recursor";
    fieldname = "auth_zones";
    type_name = "Vec<AuthZone>";
    to_yaml(rustvalue.vec_authzone_val, value);
    return true;
  }
  if (key == "carbon-interval") {
    section = "carbon";
    fieldname = "interval";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "carbon-namespace") {
    section = "carbon";
    fieldname = "ns";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "carbon-ourname") {
    section = "carbon";
    fieldname = "ourname";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "carbon-instance") {
    section = "carbon";
    fieldname = "instance";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "carbon-server") {
    section = "carbon";
    fieldname = "server";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "chroot") {
    section = "recursor";
    fieldname = "chroot";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "client-tcp-timeout") {
    section = "incoming";
    fieldname = "tcp_timeout";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "config-dir") {
    section = "recursor";
    fieldname = "config_dir";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "config-name") {
    section = "recursor";
    fieldname = "config_name";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "cpu-map") {
    section = "recursor";
    fieldname = "cpu_map";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "daemon") {
    section = "recursor";
    fieldname = "daemon";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "dont-throttle-names") {
    section = "outgoing";
    fieldname = "dont_throttle_names";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "dont-throttle-netmasks") {
    section = "outgoing";
    fieldname = "dont_throttle_netmasks";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "devonly-regression-test-mode") {
    section = "recursor";
    fieldname = "devonly_regression_test_mode";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "disable-packetcache") {
    section = "packetcache";
    fieldname = "disable";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "disable-syslog") {
    section = "logging";
    fieldname = "disable_syslog";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "distribution-load-factor") {
    section = "incoming";
    fieldname = "distribution_load_factor";
    type_name = "f64";
    to_yaml(rustvalue.f64_val, value);
    return true;
  }
  if (key == "distribution-pipe-buffer-size") {
    section = "incoming";
    fieldname = "distribution_pipe_buffer_size";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "distributor-threads") {
    section = "incoming";
    fieldname = "distributor_threads";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "dot-to-auth-names") {
    section = "outgoing";
    fieldname = "dot_to_auth_names";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "dot-to-port-853") {
    section = "outgoing";
    fieldname = "dot_to_port_853";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "dns64-prefix") {
    section = "recursor";
    fieldname = "dns64_prefix";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "dnssec") {
    section = "dnssec";
    fieldname = "validation";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "dnssec-disabled-algorithms") {
    section = "dnssec";
    fieldname = "disabled_algorithms";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "dnssec-log-bogus") {
    section = "dnssec";
    fieldname = "log_bogus";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "dont-query") {
    section = "outgoing";
    fieldname = "dont_query";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "ecs-add-for") {
    section = "ecs";
    fieldname = "add_for";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "ecs-ipv4-bits") {
    section = "ecs";
    fieldname = "ipv4_bits";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "ecs-ipv4-cache-bits") {
    section = "ecs";
    fieldname = "ipv4_cache_bits";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "ecs-ipv6-bits") {
    section = "ecs";
    fieldname = "ipv6_bits";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "ecs-ipv6-cache-bits") {
    section = "ecs";
    fieldname = "ipv6_cache_bits";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "ecs-ipv4-never-cache") {
    section = "ecs";
    fieldname = "ipv4_never_cache";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "ecs-ipv6-never-cache") {
    section = "ecs";
    fieldname = "ipv6_never_cache";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "ecs-minimum-ttl-override") {
    section = "ecs";
    fieldname = "minimum_ttl_override";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "ecs-cache-limit-ttl") {
    section = "ecs";
    fieldname = "cache_limit_ttl";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "ecs-scope-zero-address") {
    section = "ecs";
    fieldname = "scope_zero_address";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "edns-outgoing-bufsize") {
    section = "outgoing";
    fieldname = "edns_bufsize";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "edns-padding-from") {
    section = "incoming";
    fieldname = "edns_padding_from";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "edns-padding-mode") {
    section = "incoming";
    fieldname = "edns_padding_mode";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "edns-padding-out") {
    section = "outgoing";
    fieldname = "edns_padding";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "edns-padding-tag") {
    section = "incoming";
    fieldname = "edns_padding_tag";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "edns-subnet-allow-list") {
    section = "outgoing";
    fieldname = "edns_subnet_allow_list";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "etc-hosts-file") {
    section = "recursor";
    fieldname = "etc_hosts_file";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "event-trace-enabled") {
    section = "recursor";
    fieldname = "event_trace_enabled";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "export-etc-hosts") {
    section = "recursor";
    fieldname = "export_etc_hosts";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "export-etc-hosts-search-suffix") {
    section = "recursor";
    fieldname = "export_etc_hosts_search_suffix";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "extended-resolution-errors") {
    section = "recursor";
    fieldname = "extended_resolution_errors";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "forward-zones") {
    section = "recursor";
    fieldname = "forward_zones";
    type_name = "Vec<ForwardZone>";
    to_yaml(rustvalue.vec_forwardzone_val, value);
    return true;
  }
  if (key == "forward-zones-file") {
    section = "recursor";
    fieldname = "forward_zones_file";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "forward-zones-recurse") {
    section = "recursor";
    fieldname = "forward_zones_recurse";
    type_name = "Vec<ForwardZone>";
    to_yaml(rustvalue.vec_forwardzone_val, value, true);
    return true;
  }
  if (key == "gettag-needs-edns-options") {
    section = "incoming";
    fieldname = "gettag_needs_edns_options";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "hint-file") {
    section = "recursor";
    fieldname = "hint_file";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "ignore-unknown-settings") {
    section = "recursor";
    fieldname = "ignore_unknown_settings";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "include-dir") {
    section = "recursor";
    fieldname = "include_dir";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "latency-statistic-size") {
    section = "recursor";
    fieldname = "latency_statistic_size";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "local-address") {
    section = "incoming";
    fieldname = "listen";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "local-port") {
    section = "incoming";
    fieldname = "port";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "log-timestamp") {
    section = "logging";
    fieldname = "timestamp";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "non-local-bind") {
    section = "incoming";
    fieldname = "non_local_bind";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "loglevel") {
    section = "logging";
    fieldname = "loglevel";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "log-common-errors") {
    section = "logging";
    fieldname = "common_errors";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "log-rpz-changes") {
    section = "logging";
    fieldname = "rpz_changes";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "logging-facility") {
    section = "logging";
    fieldname = "facility";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "lowercase-outgoing") {
    section = "outgoing";
    fieldname = "lowercase";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "lua-config-file") {
    section = "recursor";
    fieldname = "lua_config_file";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "lua-global-include-dir") {
    section = "recursor";
    fieldname = "lua_global_include_dir";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "lua-dns-script") {
    section = "recursor";
    fieldname = "lua_dns_script";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "lua-maintenance-interval") {
    section = "recursor";
    fieldname = "lua_maintenance_interval";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-busy-dot-probes") {
    section = "outgoing";
    fieldname = "max_busy_dot_probes";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-cache-bogus-ttl") {
    section = "recordcache";
    fieldname = "max_cache_bogus_ttl";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-cache-entries") {
    section = "recordcache";
    fieldname = "max_entries";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-cache-ttl") {
    section = "recordcache";
    fieldname = "max_ttl";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-concurrent-requests-per-tcp-connection") {
    section = "incoming";
    fieldname = "max_concurrent_requests_per_tcp_connection";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-chain-length") {
    section = "recursor";
    fieldname = "max_chain_length";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-include-depth") {
    section = "recursor";
    fieldname = "max_include_depth";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-generate-steps") {
    section = "recursor";
    fieldname = "max_generate_steps";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-mthreads") {
    section = "recursor";
    fieldname = "max_mthreads";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-packetcache-entries") {
    section = "packetcache";
    fieldname = "max_entries";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-qperq") {
    section = "outgoing";
    fieldname = "max_qperq";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-cnames-followed") {
    section = "recursor";
    fieldname = "max_cnames_followed";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "limit-qtype-any") {
    section = "recordcache";
    fieldname = "limit_qtype_any";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "max-rrset-size") {
    section = "recordcache";
    fieldname = "max_rrset_size";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-ns-address-qperq") {
    section = "outgoing";
    fieldname = "max_ns_address_qperq";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-ns-per-resolve") {
    section = "outgoing";
    fieldname = "max_ns_per_resolve";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-negative-ttl") {
    section = "recordcache";
    fieldname = "max_negative_ttl";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-recursion-depth") {
    section = "recursor";
    fieldname = "max_recursion_depth";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-tcp-clients") {
    section = "incoming";
    fieldname = "max_tcp_clients";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-tcp-per-client") {
    section = "incoming";
    fieldname = "max_tcp_per_client";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-tcp-queries-per-connection") {
    section = "incoming";
    fieldname = "max_tcp_queries_per_connection";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-total-msec") {
    section = "recursor";
    fieldname = "max_total_msec";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-udp-queries-per-round") {
    section = "incoming";
    fieldname = "max_udp_queries_per_round";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "minimum-ttl-override") {
    section = "recursor";
    fieldname = "minimum_ttl_override";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "new-domain-tracking") {
    section = "nod";
    fieldname = "tracking";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "new-domain-log") {
    section = "nod";
    fieldname = "log";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "new-domain-lookup") {
    section = "nod";
    fieldname = "lookup";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "new-domain-db-size") {
    section = "nod";
    fieldname = "db_size";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "new-domain-history-dir") {
    section = "nod";
    fieldname = "history_dir";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "new-domain-db-snapshot-interval") {
    section = "nod";
    fieldname = "db_snapshot_interval";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "new-domain-ignore-list") {
    section = "nod";
    fieldname = "ignore_list";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "new-domain-ignore-list-file") {
    section = "nod";
    fieldname = "ignore_list_file";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "new-domain-pb-tag") {
    section = "nod";
    fieldname = "pb_tag";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "network-timeout") {
    section = "outgoing";
    fieldname = "network_timeout";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "non-resolving-ns-max-fails") {
    section = "outgoing";
    fieldname = "non_resolving_ns_max_fails";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "non-resolving-ns-throttle-time") {
    section = "outgoing";
    fieldname = "non_resolving_ns_throttle_time";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "nothing-below-nxdomain") {
    section = "recursor";
    fieldname = "nothing_below_nxdomain";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "nsec3-max-iterations") {
    section = "dnssec";
    fieldname = "nsec3_max_iterations";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-rrsigs-per-record") {
    section = "dnssec";
    fieldname = "max_rrsigs_per_record";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-nsec3s-per-record") {
    section = "dnssec";
    fieldname = "max_nsec3s_per_record";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-signature-validations-per-query") {
    section = "dnssec";
    fieldname = "max_signature_validations_per_query";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-nsec3-hash-computations-per-query") {
    section = "dnssec";
    fieldname = "max_nsec3_hash_computations_per_query";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "aggressive-cache-max-nsec3-hash-cost") {
    section = "dnssec";
    fieldname = "aggressive_cache_max_nsec3_hash_cost";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-ds-per-zone") {
    section = "dnssec";
    fieldname = "max_ds_per_zone";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "max-dnskeys") {
    section = "dnssec";
    fieldname = "max_dnskeys";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "packetcache-ttl") {
    section = "packetcache";
    fieldname = "ttl";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "packetcache-negative-ttl") {
    section = "packetcache";
    fieldname = "negative_ttl";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "packetcache-servfail-ttl") {
    section = "packetcache";
    fieldname = "servfail_ttl";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "packetcache-shards") {
    section = "packetcache";
    fieldname = "shards";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "pdns-distributes-queries") {
    section = "incoming";
    fieldname = "pdns_distributes_queries";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "protobuf-use-kernel-timestamp") {
    section = "logging";
    fieldname = "protobuf_use_kernel_timestamp";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "proxy-protocol-from") {
    section = "incoming";
    fieldname = "proxy_protocol_from";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "proxy-protocol-exceptions") {
    section = "incoming";
    fieldname = "proxy_protocol_exceptions";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "proxy-protocol-maximum-size") {
    section = "incoming";
    fieldname = "proxy_protocol_maximum_size";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "public-suffix-list-file") {
    section = "recursor";
    fieldname = "public_suffix_list_file";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "qname-minimization") {
    section = "recursor";
    fieldname = "qname_minimization";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "qname-max-minimize-count") {
    section = "recursor";
    fieldname = "qname_max_minimize_count";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "qname-minimize-one-label") {
    section = "recursor";
    fieldname = "qname_minimize_one_label";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "query-local-address") {
    section = "outgoing";
    fieldname = "source_address";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "quiet") {
    section = "logging";
    fieldname = "quiet";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "record-cache-locked-ttl-perc") {
    section = "recordcache";
    fieldname = "locked_ttl_perc";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "record-cache-shards") {
    section = "recordcache";
    fieldname = "shards";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "refresh-on-ttl-perc") {
    section = "recordcache";
    fieldname = "refresh_on_ttl_perc";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "reuseport") {
    section = "incoming";
    fieldname = "reuseport";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "root-nx-trust") {
    section = "recursor";
    fieldname = "root_nx_trust";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "save-parent-ns-set") {
    section = "recursor";
    fieldname = "save_parent_ns_set";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "security-poll-suffix") {
    section = "recursor";
    fieldname = "security_poll_suffix";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "serve-rfc1918") {
    section = "recursor";
    fieldname = "serve_rfc1918";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "serve-rfc6303") {
    section = "recursor";
    fieldname = "serve_rfc6303";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "serve-stale-extensions") {
    section = "recordcache";
    fieldname = "serve_stale_extensions";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "server-down-max-fails") {
    section = "outgoing";
    fieldname = "server_down_max_fails";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "server-down-throttle-time") {
    section = "outgoing";
    fieldname = "server_down_throttle_time";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "bypass-server-throttling-probability") {
    section = "outgoing";
    fieldname = "bypass_server_throttling_probability";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "server-id") {
    section = "recursor";
    fieldname = "server_id";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "setgid") {
    section = "recursor";
    fieldname = "setgid";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "setuid") {
    section = "recursor";
    fieldname = "setuid";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "signature-inception-skew") {
    section = "dnssec";
    fieldname = "signature_inception_skew";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "single-socket") {
    section = "outgoing";
    fieldname = "single_socket";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "snmp-agent") {
    section = "snmp";
    fieldname = "agent";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "snmp-daemon-socket") {
    section = "snmp";
    fieldname = "daemon_socket";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "socket-dir") {
    section = "recursor";
    fieldname = "socket_dir";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "socket-group") {
    section = "recursor";
    fieldname = "socket_group";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "socket-mode") {
    section = "recursor";
    fieldname = "socket_mode";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "socket-owner") {
    section = "recursor";
    fieldname = "socket_owner";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "spoof-nearmiss-max") {
    section = "recursor";
    fieldname = "spoof_nearmiss_max";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "stack-cache-size") {
    section = "recursor";
    fieldname = "stack_cache_size";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "stack-size") {
    section = "recursor";
    fieldname = "stack_size";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "statistics-interval") {
    section = "logging";
    fieldname = "statistics_interval";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "stats-api-disabled-list") {
    section = "recursor";
    fieldname = "stats_api_disabled_list";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "stats-carbon-disabled-list") {
    section = "recursor";
    fieldname = "stats_carbon_disabled_list";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "stats-rec-control-disabled-list") {
    section = "recursor";
    fieldname = "stats_rec_control_disabled_list";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "stats-ringbuffer-entries") {
    section = "recursor";
    fieldname = "stats_ringbuffer_entries";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "stats-snmp-disabled-list") {
    section = "recursor";
    fieldname = "stats_snmp_disabled_list";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "structured-logging") {
    section = "logging";
    fieldname = "structured_logging";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "structured-logging-backend") {
    section = "logging";
    fieldname = "structured_logging_backend";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "tcp-fast-open") {
    section = "incoming";
    fieldname = "tcp_fast_open";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "tcp-fast-open-connect") {
    section = "outgoing";
    fieldname = "tcp_fast_open_connect";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "tcp-out-max-idle-ms") {
    section = "outgoing";
    fieldname = "tcp_max_idle_ms";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "tcp-out-max-idle-per-auth") {
    section = "outgoing";
    fieldname = "tcp_max_idle_per_auth";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "tcp-out-max-queries") {
    section = "outgoing";
    fieldname = "tcp_max_queries";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "tcp-out-max-idle-per-thread") {
    section = "outgoing";
    fieldname = "tcp_max_idle_per_thread";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "threads") {
    section = "recursor";
    fieldname = "threads";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "tcp-threads") {
    section = "recursor";
    fieldname = "tcp_threads";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "trace") {
    section = "logging";
    fieldname = "trace";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "udp-source-port-min") {
    section = "outgoing";
    fieldname = "udp_source_port_min";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "udp-source-port-max") {
    section = "outgoing";
    fieldname = "udp_source_port_max";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "udp-source-port-avoid") {
    section = "outgoing";
    fieldname = "udp_source_port_avoid";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "udp-truncation-threshold") {
    section = "incoming";
    fieldname = "udp_truncation_threshold";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "unique-response-tracking") {
    section = "nod";
    fieldname = "unique_response_tracking";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "unique-response-log") {
    section = "nod";
    fieldname = "unique_response_log";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "unique-response-db-size") {
    section = "nod";
    fieldname = "unique_response_db_size";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "unique-response-history-dir") {
    section = "nod";
    fieldname = "unique_response_history_dir";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "unique-response-pb-tag") {
    section = "nod";
    fieldname = "unique_response_pb_tag";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "unique-response-ignore-list") {
    section = "nod";
    fieldname = "unique_response_ignore_list";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "unique-response-ignore-list-file") {
    section = "nod";
    fieldname = "unique_response_ignore_list_file";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "use-incoming-edns-subnet") {
    section = "incoming";
    fieldname = "use_incoming_edns_subnet";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "version-string") {
    section = "recursor";
    fieldname = "version_string";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "webserver") {
    section = "webservice";
    fieldname = "webserver";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "webserver-address") {
    section = "webservice";
    fieldname = "address";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "webserver-allow-from") {
    section = "webservice";
    fieldname = "allow_from";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "webserver-hash-plaintext-credentials") {
    section = "webservice";
    fieldname = "hash_plaintext_credentials";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "webserver-loglevel") {
    section = "webservice";
    fieldname = "loglevel";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "webserver-password") {
    section = "webservice";
    fieldname = "password";
    type_name = "String";
    to_yaml(rustvalue.string_val, value);
    return true;
  }
  if (key == "webserver-port") {
    section = "webservice";
    fieldname = "port";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "write-pid") {
    section = "recursor";
    fieldname = "write_pid";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  if (key == "x-dnssec-names") {
    section = "dnssec";
    fieldname = "x_dnssec_names";
    type_name = "Vec<String>";
    to_yaml(rustvalue.vec_string_val, value);
    return true;
  }
  if (key == "system-resolver-ttl") {
    section = "recursor";
    fieldname = "system_resolver_ttl";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "system-resolver-interval") {
    section = "recursor";
    fieldname = "system_resolver_interval";
    type_name = "u64";
    to_yaml(rustvalue.u64_val, value);
    return true;
  }
  if (key == "system-resolver-self-resolve-check") {
    section = "recursor";
    fieldname = "system_resolver_self_resolve_check";
    type_name = "bool";
    to_yaml(rustvalue.bool_val, value);
    return true;
  }
  return false;
}

void pdns::settings::rec::bridgeStructToOldStyleSettings(const Recursorsettings& settings)
{
  ::arg().set("aggressive-nsec-cache-size") = to_arg(settings.dnssec.aggressive_nsec_cache_size);
  ::arg().set("aggressive-cache-min-nsec3-hit-ratio") = to_arg(settings.dnssec.aggressive_cache_min_nsec3_hit_ratio);
  ::arg().set("allow-from") = to_arg(settings.incoming.allow_from);
  ::arg().set("allow-from-file") = to_arg(settings.incoming.allow_from_file);
  ::arg().set("allow-notify-for") = to_arg(settings.incoming.allow_notify_for);
  ::arg().set("allow-notify-for-file") = to_arg(settings.incoming.allow_notify_for_file);
  ::arg().set("allow-notify-from") = to_arg(settings.incoming.allow_notify_from);
  ::arg().set("allow-notify-from-file") = to_arg(settings.incoming.allow_notify_from_file);
  ::arg().set("allow-no-rd") = to_arg(settings.incoming.allow_no_rd);
  ::arg().set("any-to-tcp") = to_arg(settings.recursor.any_to_tcp);
  ::arg().set("allow-trust-anchor-query") = to_arg(settings.recursor.allow_trust_anchor_query);
  ::arg().set("api-config-dir") = to_arg(settings.webservice.api_dir);
  ::arg().set("api-key") = to_arg(settings.webservice.api_key);
  ::arg().set("auth-zones") = to_arg(settings.recursor.auth_zones);
  ::arg().set("carbon-interval") = to_arg(settings.carbon.interval);
  ::arg().set("carbon-namespace") = to_arg(settings.carbon.ns);
  ::arg().set("carbon-ourname") = to_arg(settings.carbon.ourname);
  ::arg().set("carbon-instance") = to_arg(settings.carbon.instance);
  ::arg().set("carbon-server") = to_arg(settings.carbon.server);
  ::arg().set("chroot") = to_arg(settings.recursor.chroot);
  ::arg().set("client-tcp-timeout") = to_arg(settings.incoming.tcp_timeout);
  ::arg().set("config-dir") = to_arg(settings.recursor.config_dir);
  ::arg().set("config-name") = to_arg(settings.recursor.config_name);
  ::arg().set("cpu-map") = to_arg(settings.recursor.cpu_map);
  ::arg().set("daemon") = to_arg(settings.recursor.daemon);
  ::arg().set("dont-throttle-names") = to_arg(settings.outgoing.dont_throttle_names);
  ::arg().set("dont-throttle-netmasks") = to_arg(settings.outgoing.dont_throttle_netmasks);
  ::arg().set("devonly-regression-test-mode") = to_arg(settings.recursor.devonly_regression_test_mode);
  ::arg().set("disable-packetcache") = to_arg(settings.packetcache.disable);
  ::arg().set("disable-syslog") = to_arg(settings.logging.disable_syslog);
  ::arg().set("distribution-load-factor") = to_arg(settings.incoming.distribution_load_factor);
  ::arg().set("distribution-pipe-buffer-size") = to_arg(settings.incoming.distribution_pipe_buffer_size);
  ::arg().set("distributor-threads") = to_arg(settings.incoming.distributor_threads);
  ::arg().set("dot-to-auth-names") = to_arg(settings.outgoing.dot_to_auth_names);
  ::arg().set("dot-to-port-853") = to_arg(settings.outgoing.dot_to_port_853);
  ::arg().set("dns64-prefix") = to_arg(settings.recursor.dns64_prefix);
  ::arg().set("dnssec") = to_arg(settings.dnssec.validation);
  ::arg().set("dnssec-disabled-algorithms") = to_arg(settings.dnssec.disabled_algorithms);
  ::arg().set("dnssec-log-bogus") = to_arg(settings.dnssec.log_bogus);
  ::arg().set("dont-query") = to_arg(settings.outgoing.dont_query);
  ::arg().set("ecs-add-for") = to_arg(settings.ecs.add_for);
  ::arg().set("ecs-ipv4-bits") = to_arg(settings.ecs.ipv4_bits);
  ::arg().set("ecs-ipv4-cache-bits") = to_arg(settings.ecs.ipv4_cache_bits);
  ::arg().set("ecs-ipv6-bits") = to_arg(settings.ecs.ipv6_bits);
  ::arg().set("ecs-ipv6-cache-bits") = to_arg(settings.ecs.ipv6_cache_bits);
  ::arg().set("ecs-ipv4-never-cache") = to_arg(settings.ecs.ipv4_never_cache);
  ::arg().set("ecs-ipv6-never-cache") = to_arg(settings.ecs.ipv6_never_cache);
  ::arg().set("ecs-minimum-ttl-override") = to_arg(settings.ecs.minimum_ttl_override);
  ::arg().set("ecs-cache-limit-ttl") = to_arg(settings.ecs.cache_limit_ttl);
  ::arg().set("ecs-scope-zero-address") = to_arg(settings.ecs.scope_zero_address);
  ::arg().set("edns-outgoing-bufsize") = to_arg(settings.outgoing.edns_bufsize);
  ::arg().set("edns-padding-from") = to_arg(settings.incoming.edns_padding_from);
  ::arg().set("edns-padding-mode") = to_arg(settings.incoming.edns_padding_mode);
  ::arg().set("edns-padding-out") = to_arg(settings.outgoing.edns_padding);
  ::arg().set("edns-padding-tag") = to_arg(settings.incoming.edns_padding_tag);
  ::arg().set("edns-subnet-allow-list") = to_arg(settings.outgoing.edns_subnet_allow_list);
  ::arg().set("etc-hosts-file") = to_arg(settings.recursor.etc_hosts_file);
  ::arg().set("event-trace-enabled") = to_arg(settings.recursor.event_trace_enabled);
  ::arg().set("export-etc-hosts") = to_arg(settings.recursor.export_etc_hosts);
  ::arg().set("export-etc-hosts-search-suffix") = to_arg(settings.recursor.export_etc_hosts_search_suffix);
  ::arg().set("extended-resolution-errors") = to_arg(settings.recursor.extended_resolution_errors);
  ::arg().set("forward-zones") = to_arg(settings.recursor.forward_zones);
  ::arg().set("forward-zones-file") = to_arg(settings.recursor.forward_zones_file);
  ::arg().set("forward-zones-recurse") = to_arg(settings.recursor.forward_zones_recurse);
  ::arg().set("gettag-needs-edns-options") = to_arg(settings.incoming.gettag_needs_edns_options);
  ::arg().set("hint-file") = to_arg(settings.recursor.hint_file);
  ::arg().set("ignore-unknown-settings") = to_arg(settings.recursor.ignore_unknown_settings);
  ::arg().set("include-dir") = to_arg(settings.recursor.include_dir);
  ::arg().set("latency-statistic-size") = to_arg(settings.recursor.latency_statistic_size);
  ::arg().set("local-address") = to_arg(settings.incoming.listen);
  ::arg().set("local-port") = to_arg(settings.incoming.port);
  ::arg().set("log-timestamp") = to_arg(settings.logging.timestamp);
  ::arg().set("non-local-bind") = to_arg(settings.incoming.non_local_bind);
  ::arg().set("loglevel") = to_arg(settings.logging.loglevel);
  ::arg().set("log-common-errors") = to_arg(settings.logging.common_errors);
  ::arg().set("log-rpz-changes") = to_arg(settings.logging.rpz_changes);
  ::arg().set("logging-facility") = to_arg(settings.logging.facility);
  ::arg().set("lowercase-outgoing") = to_arg(settings.outgoing.lowercase);
  ::arg().set("lua-config-file") = to_arg(settings.recursor.lua_config_file);
  ::arg().set("lua-global-include-dir") = to_arg(settings.recursor.lua_global_include_dir);
  ::arg().set("lua-dns-script") = to_arg(settings.recursor.lua_dns_script);
  ::arg().set("lua-maintenance-interval") = to_arg(settings.recursor.lua_maintenance_interval);
  ::arg().set("max-busy-dot-probes") = to_arg(settings.outgoing.max_busy_dot_probes);
  ::arg().set("max-cache-bogus-ttl") = to_arg(settings.recordcache.max_cache_bogus_ttl);
  ::arg().set("max-cache-entries") = to_arg(settings.recordcache.max_entries);
  ::arg().set("max-cache-ttl") = to_arg(settings.recordcache.max_ttl);
  ::arg().set("max-concurrent-requests-per-tcp-connection") = to_arg(settings.incoming.max_concurrent_requests_per_tcp_connection);
  ::arg().set("max-chain-length") = to_arg(settings.recursor.max_chain_length);
  ::arg().set("max-include-depth") = to_arg(settings.recursor.max_include_depth);
  ::arg().set("max-generate-steps") = to_arg(settings.recursor.max_generate_steps);
  ::arg().set("max-mthreads") = to_arg(settings.recursor.max_mthreads);
  ::arg().set("max-packetcache-entries") = to_arg(settings.packetcache.max_entries);
  ::arg().set("max-qperq") = to_arg(settings.outgoing.max_qperq);
  ::arg().set("max-cnames-followed") = to_arg(settings.recursor.max_cnames_followed);
  ::arg().set("limit-qtype-any") = to_arg(settings.recordcache.limit_qtype_any);
  ::arg().set("max-rrset-size") = to_arg(settings.recordcache.max_rrset_size);
  ::arg().set("max-ns-address-qperq") = to_arg(settings.outgoing.max_ns_address_qperq);
  ::arg().set("max-ns-per-resolve") = to_arg(settings.outgoing.max_ns_per_resolve);
  ::arg().set("max-negative-ttl") = to_arg(settings.recordcache.max_negative_ttl);
  ::arg().set("max-recursion-depth") = to_arg(settings.recursor.max_recursion_depth);
  ::arg().set("max-tcp-clients") = to_arg(settings.incoming.max_tcp_clients);
  ::arg().set("max-tcp-per-client") = to_arg(settings.incoming.max_tcp_per_client);
  ::arg().set("max-tcp-queries-per-connection") = to_arg(settings.incoming.max_tcp_queries_per_connection);
  ::arg().set("max-total-msec") = to_arg(settings.recursor.max_total_msec);
  ::arg().set("max-udp-queries-per-round") = to_arg(settings.incoming.max_udp_queries_per_round);
  ::arg().set("minimum-ttl-override") = to_arg(settings.recursor.minimum_ttl_override);
  ::arg().set("new-domain-tracking") = to_arg(settings.nod.tracking);
  ::arg().set("new-domain-log") = to_arg(settings.nod.log);
  ::arg().set("new-domain-lookup") = to_arg(settings.nod.lookup);
  ::arg().set("new-domain-db-size") = to_arg(settings.nod.db_size);
  ::arg().set("new-domain-history-dir") = to_arg(settings.nod.history_dir);
  ::arg().set("new-domain-db-snapshot-interval") = to_arg(settings.nod.db_snapshot_interval);
  ::arg().set("new-domain-ignore-list") = to_arg(settings.nod.ignore_list);
  ::arg().set("new-domain-ignore-list-file") = to_arg(settings.nod.ignore_list_file);
  ::arg().set("new-domain-pb-tag") = to_arg(settings.nod.pb_tag);
  ::arg().set("network-timeout") = to_arg(settings.outgoing.network_timeout);
  ::arg().set("non-resolving-ns-max-fails") = to_arg(settings.outgoing.non_resolving_ns_max_fails);
  ::arg().set("non-resolving-ns-throttle-time") = to_arg(settings.outgoing.non_resolving_ns_throttle_time);
  ::arg().set("nothing-below-nxdomain") = to_arg(settings.recursor.nothing_below_nxdomain);
  ::arg().set("nsec3-max-iterations") = to_arg(settings.dnssec.nsec3_max_iterations);
  ::arg().set("max-rrsigs-per-record") = to_arg(settings.dnssec.max_rrsigs_per_record);
  ::arg().set("max-nsec3s-per-record") = to_arg(settings.dnssec.max_nsec3s_per_record);
  ::arg().set("max-signature-validations-per-query") = to_arg(settings.dnssec.max_signature_validations_per_query);
  ::arg().set("max-nsec3-hash-computations-per-query") = to_arg(settings.dnssec.max_nsec3_hash_computations_per_query);
  ::arg().set("aggressive-cache-max-nsec3-hash-cost") = to_arg(settings.dnssec.aggressive_cache_max_nsec3_hash_cost);
  ::arg().set("max-ds-per-zone") = to_arg(settings.dnssec.max_ds_per_zone);
  ::arg().set("max-dnskeys") = to_arg(settings.dnssec.max_dnskeys);
  ::arg().set("packetcache-ttl") = to_arg(settings.packetcache.ttl);
  ::arg().set("packetcache-negative-ttl") = to_arg(settings.packetcache.negative_ttl);
  ::arg().set("packetcache-servfail-ttl") = to_arg(settings.packetcache.servfail_ttl);
  ::arg().set("packetcache-shards") = to_arg(settings.packetcache.shards);
  ::arg().set("pdns-distributes-queries") = to_arg(settings.incoming.pdns_distributes_queries);
  ::arg().set("protobuf-use-kernel-timestamp") = to_arg(settings.logging.protobuf_use_kernel_timestamp);
  ::arg().set("proxy-protocol-from") = to_arg(settings.incoming.proxy_protocol_from);
  ::arg().set("proxy-protocol-exceptions") = to_arg(settings.incoming.proxy_protocol_exceptions);
  ::arg().set("proxy-protocol-maximum-size") = to_arg(settings.incoming.proxy_protocol_maximum_size);
  ::arg().set("public-suffix-list-file") = to_arg(settings.recursor.public_suffix_list_file);
  ::arg().set("qname-minimization") = to_arg(settings.recursor.qname_minimization);
  ::arg().set("qname-max-minimize-count") = to_arg(settings.recursor.qname_max_minimize_count);
  ::arg().set("qname-minimize-one-label") = to_arg(settings.recursor.qname_minimize_one_label);
  ::arg().set("query-local-address") = to_arg(settings.outgoing.source_address);
  ::arg().set("quiet") = to_arg(settings.logging.quiet);
  ::arg().set("record-cache-locked-ttl-perc") = to_arg(settings.recordcache.locked_ttl_perc);
  ::arg().set("record-cache-shards") = to_arg(settings.recordcache.shards);
  ::arg().set("refresh-on-ttl-perc") = to_arg(settings.recordcache.refresh_on_ttl_perc);
  ::arg().set("reuseport") = to_arg(settings.incoming.reuseport);
  ::arg().set("root-nx-trust") = to_arg(settings.recursor.root_nx_trust);
  ::arg().set("save-parent-ns-set") = to_arg(settings.recursor.save_parent_ns_set);
  ::arg().set("security-poll-suffix") = to_arg(settings.recursor.security_poll_suffix);
  ::arg().set("serve-rfc1918") = to_arg(settings.recursor.serve_rfc1918);
  ::arg().set("serve-rfc6303") = to_arg(settings.recursor.serve_rfc6303);
  ::arg().set("serve-stale-extensions") = to_arg(settings.recordcache.serve_stale_extensions);
  ::arg().set("server-down-max-fails") = to_arg(settings.outgoing.server_down_max_fails);
  ::arg().set("server-down-throttle-time") = to_arg(settings.outgoing.server_down_throttle_time);
  ::arg().set("bypass-server-throttling-probability") = to_arg(settings.outgoing.bypass_server_throttling_probability);
  ::arg().set("server-id") = to_arg(settings.recursor.server_id);
  ::arg().set("setgid") = to_arg(settings.recursor.setgid);
  ::arg().set("setuid") = to_arg(settings.recursor.setuid);
  ::arg().set("signature-inception-skew") = to_arg(settings.dnssec.signature_inception_skew);
  ::arg().set("single-socket") = to_arg(settings.outgoing.single_socket);
  ::arg().set("snmp-agent") = to_arg(settings.snmp.agent);
  ::arg().set("snmp-daemon-socket") = to_arg(settings.snmp.daemon_socket);
  ::arg().set("socket-dir") = to_arg(settings.recursor.socket_dir);
  ::arg().set("socket-group") = to_arg(settings.recursor.socket_group);
  ::arg().set("socket-mode") = to_arg(settings.recursor.socket_mode);
  ::arg().set("socket-owner") = to_arg(settings.recursor.socket_owner);
  ::arg().set("spoof-nearmiss-max") = to_arg(settings.recursor.spoof_nearmiss_max);
  ::arg().set("stack-cache-size") = to_arg(settings.recursor.stack_cache_size);
  ::arg().set("stack-size") = to_arg(settings.recursor.stack_size);
  ::arg().set("statistics-interval") = to_arg(settings.logging.statistics_interval);
  ::arg().set("stats-api-disabled-list") = to_arg(settings.recursor.stats_api_disabled_list);
  ::arg().set("stats-carbon-disabled-list") = to_arg(settings.recursor.stats_carbon_disabled_list);
  ::arg().set("stats-rec-control-disabled-list") = to_arg(settings.recursor.stats_rec_control_disabled_list);
  ::arg().set("stats-ringbuffer-entries") = to_arg(settings.recursor.stats_ringbuffer_entries);
  ::arg().set("stats-snmp-disabled-list") = to_arg(settings.recursor.stats_snmp_disabled_list);
  ::arg().set("structured-logging") = to_arg(settings.logging.structured_logging);
  ::arg().set("structured-logging-backend") = to_arg(settings.logging.structured_logging_backend);
  ::arg().set("tcp-fast-open") = to_arg(settings.incoming.tcp_fast_open);
  ::arg().set("tcp-fast-open-connect") = to_arg(settings.outgoing.tcp_fast_open_connect);
  ::arg().set("tcp-out-max-idle-ms") = to_arg(settings.outgoing.tcp_max_idle_ms);
  ::arg().set("tcp-out-max-idle-per-auth") = to_arg(settings.outgoing.tcp_max_idle_per_auth);
  ::arg().set("tcp-out-max-queries") = to_arg(settings.outgoing.tcp_max_queries);
  ::arg().set("tcp-out-max-idle-per-thread") = to_arg(settings.outgoing.tcp_max_idle_per_thread);
  ::arg().set("threads") = to_arg(settings.recursor.threads);
  ::arg().set("tcp-threads") = to_arg(settings.recursor.tcp_threads);
  ::arg().set("trace") = to_arg(settings.logging.trace);
  ::arg().set("udp-source-port-min") = to_arg(settings.outgoing.udp_source_port_min);
  ::arg().set("udp-source-port-max") = to_arg(settings.outgoing.udp_source_port_max);
  ::arg().set("udp-source-port-avoid") = to_arg(settings.outgoing.udp_source_port_avoid);
  ::arg().set("udp-truncation-threshold") = to_arg(settings.incoming.udp_truncation_threshold);
  ::arg().set("unique-response-tracking") = to_arg(settings.nod.unique_response_tracking);
  ::arg().set("unique-response-log") = to_arg(settings.nod.unique_response_log);
  ::arg().set("unique-response-db-size") = to_arg(settings.nod.unique_response_db_size);
  ::arg().set("unique-response-history-dir") = to_arg(settings.nod.unique_response_history_dir);
  ::arg().set("unique-response-pb-tag") = to_arg(settings.nod.unique_response_pb_tag);
  ::arg().set("unique-response-ignore-list") = to_arg(settings.nod.unique_response_ignore_list);
  ::arg().set("unique-response-ignore-list-file") = to_arg(settings.nod.unique_response_ignore_list_file);
  ::arg().set("use-incoming-edns-subnet") = to_arg(settings.incoming.use_incoming_edns_subnet);
  ::arg().set("version-string") = to_arg(settings.recursor.version_string);
  ::arg().set("webserver") = to_arg(settings.webservice.webserver);
  ::arg().set("webserver-address") = to_arg(settings.webservice.address);
  ::arg().set("webserver-allow-from") = to_arg(settings.webservice.allow_from);
  ::arg().set("webserver-hash-plaintext-credentials") = to_arg(settings.webservice.hash_plaintext_credentials);
  ::arg().set("webserver-loglevel") = to_arg(settings.webservice.loglevel);
  ::arg().set("webserver-password") = to_arg(settings.webservice.password);
  ::arg().set("webserver-port") = to_arg(settings.webservice.port);
  ::arg().set("write-pid") = to_arg(settings.recursor.write_pid);
  ::arg().set("x-dnssec-names") = to_arg(settings.dnssec.x_dnssec_names);
  ::arg().set("system-resolver-ttl") = to_arg(settings.recursor.system_resolver_ttl);
  ::arg().set("system-resolver-interval") = to_arg(settings.recursor.system_resolver_interval);
  ::arg().set("system-resolver-self-resolve-check") = to_arg(settings.recursor.system_resolver_self_resolve_check);
}
