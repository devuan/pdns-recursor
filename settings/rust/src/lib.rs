// THIS IS A GENERATED FILE. DO NOT EDIT. SOURCE: see settings dir
// START INCLUDE rust-preable-in.rs
/*
 * This file is part of PowerDNS or dnsdist.
 * Copyright -- PowerDNS.COM B.V. and its contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * In addition, for the avoidance of any doubt, permission is granted to
 * link this program with OpenSSL and to (re)distribute the binaries
 * produced as the result of such linking.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

// This file (rust-preamble-in.rs) is included at the start of lib.rs

use serde::{Deserialize, Serialize};

mod helpers;
use helpers::*;

mod bridge;
use bridge::*;

// Suppresses "Deserialize unused" warning
#[derive(Deserialize, Serialize)]
struct UnusedStruct {}

trait Validate {
    fn validate(&self) -> Result<(), ValidationError>;
}

#[derive(Debug)]
pub struct ValidationError {
    msg: String,
}

trait Merge {
    fn merge(&mut self, rhs: &mut Self, map: Option<&serde_yaml::Mapping>);
}
// END INCLUDE rust-preamble-in.rs

#[cxx::bridge(namespace = "pdns::rust::settings::rec")]
mod recsettings {
    // START INCLUDE rust-bridge-in.rs
    // This file (rust-bridge-in.rs) is included into lib.rs inside the bridge module
    /*
     * Implement non-generated structs that need to be handled by Serde and CXX
     */
    
    // A single forward zone
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct ForwardZone {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        zone: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        forwarders: Vec<String>,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        recurse: bool,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        notify_allowed: bool,
    }
    
    // A single auth zone
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct AuthZone {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        zone: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        file: String,
    }
    
    // A single trust anchor
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct TrustAnchor {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        name: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        dsrecords: Vec<String>,
    }
    
    // A single negative trust anchor
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct NegativeTrustAnchor {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        name: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        reason: String,
    }
    
    // A protobuf logging server
    #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct ProtobufServer {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        servers: Vec<String>,
        #[serde(default = "crate::U64::<2>::value", skip_serializing_if = "crate::U64::<2>::is_equal")]
        timeout: u64,
        #[serde(default = "crate::U64::<100>::value", skip_serializing_if = "crate::U64::<100>::is_equal")]
        maxQueuedEntries: u64,
        #[serde(default = "crate::U64::<1>::value", skip_serializing_if = "crate::U64::<1>::is_equal")]
        reconnectWaitTime: u64,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        taggedOnly: bool,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        asyncConnect: bool,
        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        logQueries: bool,
        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        logResponses: bool,
        #[serde(default = "crate::def_pb_export_qtypes", skip_serializing_if = "crate::default_value_equal_pb_export_qtypes")]
        exportTypes: Vec<String>,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        logMappedFrom: bool,
    }
    
    // A dnstap logging server
    #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct DNSTapFrameStreamServer {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        servers: Vec<String>,
        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        logQueries: bool,
        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        logResponses: bool,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        bufferHint: u64,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        flushTimeout: u64,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        inputQueueSize: u64,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        outputQueueSize: u64,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        queueNotifyThreshold: u64,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        reopenInterval: u64,
    }
    
    // A dnstap logging NOD server
    #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct DNSTapNODFrameStreamServer {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        servers: Vec<String>,
        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        logNODs: bool,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        logUDRs: bool,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        bufferHint: u64,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        flushTimeout: u64,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        inputQueueSize: u64,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        outputQueueSize: u64,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        queueNotifyThreshold: u64,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        reopenInterval: u64,
    }
    
    #[derive(Default, Deserialize, Serialize, Clone, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct TSIGTriplet {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        name: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        algo: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        secret: String,
    }
    
    #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct RPZ {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        name: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        addresses: Vec<String>,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        defcontent: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        defpol: String,
        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        defpolOverrideLocalData: bool,
        #[serde(default = "crate::U32::<{u32::MAX}>::value", skip_serializing_if = "crate::U32::<{u32::MAX}>::is_equal")]
        defttl: u32,
        #[serde(default = "crate::U32::<{u32::MAX}>::value", skip_serializing_if = "crate::U32::<{u32::MAX}>::is_equal")]
        extendedErrorCode: u32,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        extendedErrorExtra: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        includeSOA: bool,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        ignoreDuplicates: bool,
        #[serde(default = "crate::U32::<{u32::MAX}>::value", skip_serializing_if = "crate::U32::<{u32::MAX}>::is_equal")]
        maxTTL: u32,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        policyName: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        tags: Vec<String>,
        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        overridesGettag: bool,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        zoneSizeHint: u32,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        tsig: TSIGTriplet,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        refresh: u32,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        maxReceivedMBytes: u32,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        localAddress: String,
        #[serde(default = "crate::U32::<20>::value", skip_serializing_if = "crate::U32::<20>::is_equal")]
        axfrTimeout: u32,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        dumpFile: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        seedFile: String,
    }
    
    #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct ZoneToCache {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        zone: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        method: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        sources: Vec<String>,
        #[serde(default = "crate::U64::<20>::value", skip_serializing_if = "crate::U64::<20>::is_equal")]
        timeout: u64,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        tsig: TSIGTriplet,
        #[serde(default = "crate::U64::<86400>::value", skip_serializing_if = "crate::U64::<86400>::is_equal")]
        refreshPeriod: u64,
        #[serde(default = "crate::U64::<60>::value", skip_serializing_if = "crate::U64::<60>::is_equal")]
        retryOnErrorPeriod: u64,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        maxReceivedMBytes: u64,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        localAddress: String,
        #[serde(default = "crate::def_ztc_validate", skip_serializing_if = "crate::def_value_equals_ztc_validate")]
        zonemd: String,
        #[serde(default = "crate::def_ztc_validate", skip_serializing_if = "crate::def_value_equals_ztc_validate")]
        dnssec: String,
    }
    
    #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct SubnetOrder {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        subnet: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        order: u32,
    }
    
    #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct SortList {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        key: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        subnets: Vec<SubnetOrder>,
    }
    
    #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct AllowedAdditionalQType {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        qtype: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        targets: Vec<String>,
        #[serde(default = "crate::def_additional_mode", skip_serializing_if = "crate::default_value_equals_additional_mode")]
        mode: String,
    }
    
    #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct ProxyMapping {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        subnet: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        address: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        domains: Vec<String>,
    }
    
    // A struct holding both a vector of forward zones and a vector of auth zones, used by REST API code
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct ApiZones {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        auth_zones: Vec<AuthZone>,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        forward_zones: Vec<ForwardZone>,
    }
    
    #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct XFR {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        addresses: Vec<String>,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        zoneSizeHint: u32,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        tsig: TSIGTriplet,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        refresh: u32,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        maxReceivedMBytes: u32,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        localAddress: String,
        #[serde(default = "crate::U32::<20>::value", skip_serializing_if = "crate::U32::<20>::is_equal")]
        axfrTimeout: u32,
    }
    
    #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct FCZDefault {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        name: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        forwarders: Vec<String>,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        recurse: bool,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        notify_allowed: bool,
     }
    
    #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct ForwardingCatalogZone {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        zone: String,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        notify_allowed: bool,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        xfr: XFR,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        groups: Vec<FCZDefault>,
    }
    
    // Two structs used to generated YAML based on a vector of name to value mappings
    // Cannot use Enum as CXX has only very basic Enum support
    struct Value {
        bool_val: bool,
        u64_val: u64,
        f64_val: f64,
        string_val: String,
        vec_string_val: Vec<String>,
        vec_forwardzone_val: Vec<ForwardZone>,
        vec_authzone_val: Vec<AuthZone>,
        vec_trustanchor_val: Vec<TrustAnchor>,
        vec_negativetrustanchor_val: Vec<NegativeTrustAnchor>,
        vec_protobufserver_val: Vec<ProtobufServer>,
        vec_dnstap_framestream_server_val: Vec<DNSTapFrameStreamServer>,
        vec_dnstap_nod_framestream_server_val: Vec<DNSTapNODFrameStreamServer>,
        vec_rpz_val: Vec<RPZ>,
        vec_sortlist_val: Vec<SortList>,
        vec_zonetocache_val: Vec<ZoneToCache>,
        vec_allowedadditionalqtype_val: Vec<AllowedAdditionalQType>,
        vec_proxymapping_val: Vec<ProxyMapping>,
        vec_forwardingcatalogzone_val: Vec<ForwardingCatalogZone>,
    }
    
    struct OldStyle {
        section: String,
        name: String,
        old_name: String,
        type_name: String,
        value: Value,
        overriding: bool,
    }
    
    /*
     * Functions callable from C++
     */
    extern "Rust" {
        // Parse a string representing YAML text and produce the corresponding data structure known to Serde
        // The settings that can be stored in individual files get their own parse function
        // Main recursor settings
        fn parse_yaml_string(str: &str) -> Result<Recursorsettings>;
        // Allow from sequence
        fn parse_yaml_string_to_allow_from(str: &str) -> Result<Vec<String>>;
        // Forward zones sequence
        fn parse_yaml_string_to_forward_zones(str: &str) -> Result<Vec<ForwardZone>>;
        // Allow notify for sequence
        fn parse_yaml_string_to_allow_notify_for(str: &str) -> Result<Vec<String>>;
        // REST API zones
        fn parse_yaml_string_to_api_zones(str: &str) -> Result<ApiZones>;
    
        // Prdoduce a YAML formatted string given a data structure known to Serde
        fn to_yaml_string(self: &Recursorsettings) -> Result<String>;
        // When doing a conversion of old-style to YAML style we use a vector of OldStyle structs
        fn map_to_yaml_string(map: &Vec<OldStyle>) -> Result<String>;
        fn forward_zones_to_yaml_string(vec: &Vec<ForwardZone>) -> Result<String>;
        fn allow_from_to_yaml_string(vec: &Vec<String>) -> Result<String>;
        fn allow_from_to_yaml_string_incoming(key: &String, filekey: &String, vec: &Vec<String>) -> Result<String>;
        fn allow_for_to_yaml_string(vec: &Vec<String>) -> Result<String>;
    
        // Merge a string representing YAML settings into a existing setttings struct
        fn merge(lhs: &mut Recursorsettings, rhs: &str) -> Result<()>;
    
        // Validate the sections inside the main settings struct, sections themselves will validate their fields
        fn validate(self: &Recursorsettings) -> Result<()>;
        // The validate function below are "hand-crafted" as their structs are not generated
        fn validate(self: &AuthZone, field: &str) -> Result<()>;
        fn validate(self: &ForwardZone, field: &str) -> Result<()>;
        fn validate(self: &TrustAnchor, field: &str) -> Result<()>;
        fn validate(self: &NegativeTrustAnchor, field: &str) -> Result<()>;
        fn validate(self: &ApiZones, field: &str) -> Result<()>;
    
        // Helper functions to call the proper validate function on vectors of various kinds
        fn validate_auth_zones(field: &str, vec: &Vec<AuthZone>) -> Result<()>;
        fn validate_forward_zones(field: &str, vec: &Vec<ForwardZone>) -> Result<()>;
        fn validate_allow_for(field: &str, vec: &Vec<String>) -> Result<()>;
        fn validate_allow_notify_for(field: &str, vec: &Vec<String>) -> Result<()>;
        fn validate_allow_from(field: &str, vec: &Vec<String>) -> Result<()>;
    
        // The functions to maintain REST API managed zones
        fn api_read_zones(path: &str) ->  Result<UniquePtr<ApiZones>>;
        fn api_add_auth_zone(file: &str, authzone: AuthZone) -> Result<()>;
        fn api_add_forward_zone(file: &str, forwardzone: ForwardZone) -> Result<()>;
        fn api_add_forward_zones(file: &str, forwardzones: &mut Vec<ForwardZone>) -> Result<()>;
        fn validate_trustanchors(field: &str, vec: &Vec<TrustAnchor>) -> Result<()>;
        fn validate_negativetrustanchors(field: &str, vec: &Vec<NegativeTrustAnchor>) -> Result<()>;
        fn api_delete_zone(file: &str, zone: &str) -> Result<()>;
        fn api_delete_zones(file: &str) -> Result<()>;
    }
    
    unsafe extern "C++" {
        include!("bridge.hh");
        fn qTypeStringToCode(name: &str) -> u16;
        fn isValidHostname(name: &str) -> bool;
    }
    // END INCLUDE rust-bridge-in.rs

    // SECTION Dnssec
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct Dnssec {
        #[serde(default = "crate::U64::<100000>::value", skip_serializing_if = "crate::U64::<100000>::is_equal")]
        aggressive_nsec_cache_size: u64,

        #[serde(default = "crate::U64::<2000>::value", skip_serializing_if = "crate::U64::<2000>::is_equal")]
        aggressive_cache_min_nsec3_hit_ratio: u64,

        #[serde(default = "crate::default_value_dnssec_validation", skip_serializing_if = "crate::default_value_equal_dnssec_validation")]
        validation: String,

        #[serde(default = "crate::default_value_dnssec_disabled_algorithms", skip_serializing_if = "crate::default_value_equal_dnssec_disabled_algorithms")]
        disabled_algorithms: Vec<String>,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        log_bogus: bool,

        #[serde(default = "crate::U64::<50>::value", skip_serializing_if = "crate::U64::<50>::is_equal")]
        nsec3_max_iterations: u64,

        #[serde(default = "crate::U64::<2>::value", skip_serializing_if = "crate::U64::<2>::is_equal")]
        max_rrsigs_per_record: u64,

        #[serde(default = "crate::U64::<10>::value", skip_serializing_if = "crate::U64::<10>::is_equal")]
        max_nsec3s_per_record: u64,

        #[serde(default = "crate::U64::<30>::value", skip_serializing_if = "crate::U64::<30>::is_equal")]
        max_signature_validations_per_query: u64,

        #[serde(default = "crate::U64::<600>::value", skip_serializing_if = "crate::U64::<600>::is_equal")]
        max_nsec3_hash_computations_per_query: u64,

        #[serde(default = "crate::U64::<150>::value", skip_serializing_if = "crate::U64::<150>::is_equal")]
        aggressive_cache_max_nsec3_hash_cost: u64,

        #[serde(default = "crate::U64::<8>::value", skip_serializing_if = "crate::U64::<8>::is_equal")]
        max_ds_per_zone: u64,

        #[serde(default = "crate::U64::<2>::value", skip_serializing_if = "crate::U64::<2>::is_equal")]
        max_dnskeys: u64,

        #[serde(default = "crate::U64::<60>::value", skip_serializing_if = "crate::U64::<60>::is_equal")]
        signature_inception_skew: u64,

        #[serde(default = "crate::default_value_dnssec_x_dnssec_names", skip_serializing_if = "crate::default_value_equal_dnssec_x_dnssec_names")]
        x_dnssec_names: Vec<String>,

        #[serde(default = "crate::default_value_dnssec_trustanchors", skip_serializing_if = "crate::default_value_equal_dnssec_trustanchors")]
        trustanchors: Vec<TrustAnchor>,

        #[serde(default = "crate::default_value_dnssec_negative_trustanchors", skip_serializing_if = "crate::default_value_equal_dnssec_negative_trustanchors")]
        negative_trustanchors: Vec<NegativeTrustAnchor>,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        trustanchorfile: String,

        #[serde(default = "crate::U64::<24>::value", skip_serializing_if = "crate::U64::<24>::is_equal")]
        trustanchorfile_interval: u64,

    }
    // END SECTION Dnssec

    // SECTION Incoming
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct Incoming {
        #[serde(default = "crate::default_value_incoming_allow_from", skip_serializing_if = "crate::default_value_equal_incoming_allow_from")]
        allow_from: Vec<String>,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        allow_from_file: String,

        #[serde(default = "crate::default_value_incoming_allow_notify_for", skip_serializing_if = "crate::default_value_equal_incoming_allow_notify_for")]
        allow_notify_for: Vec<String>,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        allow_notify_for_file: String,

        #[serde(default = "crate::default_value_incoming_allow_notify_from", skip_serializing_if = "crate::default_value_equal_incoming_allow_notify_from")]
        allow_notify_from: Vec<String>,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        allow_notify_from_file: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        allow_no_rd: bool,

        #[serde(default = "crate::U64::<2>::value", skip_serializing_if = "crate::U64::<2>::is_equal")]
        tcp_timeout: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        distribution_load_factor: f64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        distribution_pipe_buffer_size: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        distributor_threads: u64,

        #[serde(default = "crate::default_value_incoming_edns_padding_from", skip_serializing_if = "crate::default_value_equal_incoming_edns_padding_from")]
        edns_padding_from: Vec<String>,

        #[serde(default = "crate::default_value_incoming_edns_padding_mode", skip_serializing_if = "crate::default_value_equal_incoming_edns_padding_mode")]
        edns_padding_mode: String,

        #[serde(default = "crate::U64::<7830>::value", skip_serializing_if = "crate::U64::<7830>::is_equal")]
        edns_padding_tag: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        gettag_needs_edns_options: bool,

        #[serde(default = "crate::default_value_incoming_listen", skip_serializing_if = "crate::default_value_equal_incoming_listen")]
        listen: Vec<String>,

        #[serde(default = "crate::U64::<53>::value", skip_serializing_if = "crate::U64::<53>::is_equal")]
        port: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        non_local_bind: bool,

        #[serde(default = "crate::U64::<10>::value", skip_serializing_if = "crate::U64::<10>::is_equal")]
        max_concurrent_requests_per_tcp_connection: u64,

        #[serde(default = "crate::U64::<1024>::value", skip_serializing_if = "crate::U64::<1024>::is_equal")]
        max_tcp_clients: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        max_tcp_per_client: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        max_tcp_queries_per_connection: u64,

        #[serde(default = "crate::U64::<10000>::value", skip_serializing_if = "crate::U64::<10000>::is_equal")]
        max_udp_queries_per_round: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        pdns_distributes_queries: bool,

        #[serde(default = "crate::default_value_incoming_proxy_protocol_from", skip_serializing_if = "crate::default_value_equal_incoming_proxy_protocol_from")]
        proxy_protocol_from: Vec<String>,

        #[serde(default = "crate::default_value_incoming_proxy_protocol_exceptions", skip_serializing_if = "crate::default_value_equal_incoming_proxy_protocol_exceptions")]
        proxy_protocol_exceptions: Vec<String>,

        #[serde(default = "crate::U64::<512>::value", skip_serializing_if = "crate::U64::<512>::is_equal")]
        proxy_protocol_maximum_size: u64,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        reuseport: bool,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        tcp_fast_open: u64,

        #[serde(default = "crate::U64::<1232>::value", skip_serializing_if = "crate::U64::<1232>::is_equal")]
        udp_truncation_threshold: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        use_incoming_edns_subnet: bool,

        #[serde(default = "crate::default_value_incoming_proxymappings", skip_serializing_if = "crate::default_value_equal_incoming_proxymappings")]
        proxymappings: Vec<ProxyMapping>,

    }
    // END SECTION Incoming

    // SECTION Recursor
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct Recursor {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        any_to_tcp: bool,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        allow_trust_anchor_query: bool,

        #[serde(default = "crate::default_value_recursor_auth_zones", skip_serializing_if = "crate::default_value_equal_recursor_auth_zones")]
        auth_zones: Vec<AuthZone>,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        chroot: String,

        #[serde(default = "crate::default_value_recursor_config_dir", skip_serializing_if = "crate::default_value_equal_recursor_config_dir")]
        config_dir: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        config_name: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        cpu_map: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        daemon: bool,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        devonly_regression_test_mode: bool,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        dns64_prefix: String,

        #[serde(default = "crate::default_value_recursor_etc_hosts_file", skip_serializing_if = "crate::default_value_equal_recursor_etc_hosts_file")]
        etc_hosts_file: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        event_trace_enabled: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        export_etc_hosts: bool,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        export_etc_hosts_search_suffix: String,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        extended_resolution_errors: bool,

        #[serde(default = "crate::default_value_recursor_forward_zones", skip_serializing_if = "crate::default_value_equal_recursor_forward_zones")]
        forward_zones: Vec<ForwardZone>,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        forward_zones_file: String,

        #[serde(default = "crate::default_value_recursor_forward_zones_recurse", skip_serializing_if = "crate::default_value_equal_recursor_forward_zones_recurse")]
        forward_zones_recurse: Vec<ForwardZone>,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        hint_file: String,

        #[serde(default = "crate::default_value_recursor_ignore_unknown_settings", skip_serializing_if = "crate::default_value_equal_recursor_ignore_unknown_settings")]
        ignore_unknown_settings: Vec<String>,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        include_dir: String,

        #[serde(default = "crate::U64::<10000>::value", skip_serializing_if = "crate::U64::<10000>::is_equal")]
        latency_statistic_size: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        lua_config_file: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        lua_global_include_dir: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        lua_dns_script: String,

        #[serde(default = "crate::U64::<1>::value", skip_serializing_if = "crate::U64::<1>::is_equal")]
        lua_maintenance_interval: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        max_chain_length: u64,

        #[serde(default = "crate::U64::<20>::value", skip_serializing_if = "crate::U64::<20>::is_equal")]
        max_include_depth: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        max_generate_steps: u64,

        #[serde(default = "crate::U64::<2048>::value", skip_serializing_if = "crate::U64::<2048>::is_equal")]
        max_mthreads: u64,

        #[serde(default = "crate::U64::<10>::value", skip_serializing_if = "crate::U64::<10>::is_equal")]
        max_cnames_followed: u64,

        #[serde(default = "crate::U64::<16>::value", skip_serializing_if = "crate::U64::<16>::is_equal")]
        max_recursion_depth: u64,

        #[serde(default = "crate::U64::<7000>::value", skip_serializing_if = "crate::U64::<7000>::is_equal")]
        max_total_msec: u64,

        #[serde(default = "crate::U64::<1>::value", skip_serializing_if = "crate::U64::<1>::is_equal")]
        minimum_ttl_override: u64,

        #[serde(default = "crate::default_value_recursor_nothing_below_nxdomain", skip_serializing_if = "crate::default_value_equal_recursor_nothing_below_nxdomain")]
        nothing_below_nxdomain: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        public_suffix_list_file: String,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        qname_minimization: bool,

        #[serde(default = "crate::U64::<10>::value", skip_serializing_if = "crate::U64::<10>::is_equal")]
        qname_max_minimize_count: u64,

        #[serde(default = "crate::U64::<4>::value", skip_serializing_if = "crate::U64::<4>::is_equal")]
        qname_minimize_one_label: u64,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        root_nx_trust: bool,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        save_parent_ns_set: bool,

        #[serde(default = "crate::default_value_recursor_security_poll_suffix", skip_serializing_if = "crate::default_value_equal_recursor_security_poll_suffix")]
        security_poll_suffix: String,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        serve_rfc1918: bool,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        serve_rfc6303: bool,

        #[serde(default = "crate::default_value_recursor_server_id", skip_serializing_if = "crate::default_value_equal_recursor_server_id")]
        server_id: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        setgid: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        setuid: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        socket_dir: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        socket_group: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        socket_mode: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        socket_owner: String,

        #[serde(default = "crate::U64::<1>::value", skip_serializing_if = "crate::U64::<1>::is_equal")]
        spoof_nearmiss_max: u64,

        #[serde(default = "crate::U64::<100>::value", skip_serializing_if = "crate::U64::<100>::is_equal")]
        stack_cache_size: u64,

        #[serde(default = "crate::U64::<200000>::value", skip_serializing_if = "crate::U64::<200000>::is_equal")]
        stack_size: u64,

        #[serde(default = "crate::default_value_recursor_stats_api_disabled_list", skip_serializing_if = "crate::default_value_equal_recursor_stats_api_disabled_list")]
        stats_api_disabled_list: Vec<String>,

        #[serde(default = "crate::default_value_recursor_stats_carbon_disabled_list", skip_serializing_if = "crate::default_value_equal_recursor_stats_carbon_disabled_list")]
        stats_carbon_disabled_list: Vec<String>,

        #[serde(default = "crate::default_value_recursor_stats_rec_control_disabled_list", skip_serializing_if = "crate::default_value_equal_recursor_stats_rec_control_disabled_list")]
        stats_rec_control_disabled_list: Vec<String>,

        #[serde(default = "crate::U64::<10000>::value", skip_serializing_if = "crate::U64::<10000>::is_equal")]
        stats_ringbuffer_entries: u64,

        #[serde(default = "crate::default_value_recursor_stats_snmp_disabled_list", skip_serializing_if = "crate::default_value_equal_recursor_stats_snmp_disabled_list")]
        stats_snmp_disabled_list: Vec<String>,

        #[serde(default = "crate::U64::<2>::value", skip_serializing_if = "crate::U64::<2>::is_equal")]
        threads: u64,

        #[serde(default = "crate::U64::<1>::value", skip_serializing_if = "crate::U64::<1>::is_equal")]
        tcp_threads: u64,

        #[serde(default = "crate::default_value_recursor_version_string", skip_serializing_if = "crate::default_value_equal_recursor_version_string")]
        version_string: String,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        write_pid: bool,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        system_resolver_ttl: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        system_resolver_interval: u64,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        system_resolver_self_resolve_check: bool,

        #[serde(default = "crate::default_value_recursor_sortlists", skip_serializing_if = "crate::default_value_equal_recursor_sortlists")]
        sortlists: Vec<SortList>,

        #[serde(default = "crate::default_value_recursor_rpzs", skip_serializing_if = "crate::default_value_equal_recursor_rpzs")]
        rpzs: Vec<RPZ>,

        #[serde(default = "crate::default_value_recursor_allowed_additional_qtypes", skip_serializing_if = "crate::default_value_equal_recursor_allowed_additional_qtypes")]
        allowed_additional_qtypes: Vec<AllowedAdditionalQType>,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        lua_start_stop_script: String,

        #[serde(default = "crate::default_value_recursor_forwarding_catalog_zones", skip_serializing_if = "crate::default_value_equal_recursor_forwarding_catalog_zones")]
        forwarding_catalog_zones: Vec<ForwardingCatalogZone>,

    }
    // END SECTION Recursor

    // SECTION Webservice
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct Webservice {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        api_dir: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        api_key: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        webserver: bool,

        #[serde(default = "crate::default_value_webservice_address", skip_serializing_if = "crate::default_value_equal_webservice_address")]
        address: String,

        #[serde(default = "crate::default_value_webservice_allow_from", skip_serializing_if = "crate::default_value_equal_webservice_allow_from")]
        allow_from: Vec<String>,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        hash_plaintext_credentials: bool,

        #[serde(default = "crate::default_value_webservice_loglevel", skip_serializing_if = "crate::default_value_equal_webservice_loglevel")]
        loglevel: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        password: String,

        #[serde(default = "crate::U64::<8082>::value", skip_serializing_if = "crate::U64::<8082>::is_equal")]
        port: u64,

    }
    // END SECTION Webservice

    // SECTION Carbon
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct Carbon {
        #[serde(default = "crate::U64::<30>::value", skip_serializing_if = "crate::U64::<30>::is_equal")]
        interval: u64,

        #[serde(default = "crate::default_value_carbon_ns", skip_serializing_if = "crate::default_value_equal_carbon_ns")]
        ns: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        ourname: String,

        #[serde(default = "crate::default_value_carbon_instance", skip_serializing_if = "crate::default_value_equal_carbon_instance")]
        instance: String,

        #[serde(default = "crate::default_value_carbon_server", skip_serializing_if = "crate::default_value_equal_carbon_server")]
        server: Vec<String>,

    }
    // END SECTION Carbon

    // SECTION Outgoing
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct Outgoing {
        #[serde(default = "crate::default_value_outgoing_dont_throttle_names", skip_serializing_if = "crate::default_value_equal_outgoing_dont_throttle_names")]
        dont_throttle_names: Vec<String>,

        #[serde(default = "crate::default_value_outgoing_dont_throttle_netmasks", skip_serializing_if = "crate::default_value_equal_outgoing_dont_throttle_netmasks")]
        dont_throttle_netmasks: Vec<String>,

        #[serde(default = "crate::default_value_outgoing_dot_to_auth_names", skip_serializing_if = "crate::default_value_equal_outgoing_dot_to_auth_names")]
        dot_to_auth_names: Vec<String>,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        dot_to_port_853: bool,

        #[serde(default = "crate::default_value_outgoing_dont_query", skip_serializing_if = "crate::default_value_equal_outgoing_dont_query")]
        dont_query: Vec<String>,

        #[serde(default = "crate::U64::<1232>::value", skip_serializing_if = "crate::U64::<1232>::is_equal")]
        edns_bufsize: u64,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        edns_padding: bool,

        #[serde(default = "crate::default_value_outgoing_edns_subnet_allow_list", skip_serializing_if = "crate::default_value_equal_outgoing_edns_subnet_allow_list")]
        edns_subnet_allow_list: Vec<String>,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        lowercase: bool,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        max_busy_dot_probes: u64,

        #[serde(default = "crate::U64::<50>::value", skip_serializing_if = "crate::U64::<50>::is_equal")]
        max_qperq: u64,

        #[serde(default = "crate::U64::<10>::value", skip_serializing_if = "crate::U64::<10>::is_equal")]
        max_ns_address_qperq: u64,

        #[serde(default = "crate::U64::<13>::value", skip_serializing_if = "crate::U64::<13>::is_equal")]
        max_ns_per_resolve: u64,

        #[serde(default = "crate::U64::<1500>::value", skip_serializing_if = "crate::U64::<1500>::is_equal")]
        network_timeout: u64,

        #[serde(default = "crate::U64::<5>::value", skip_serializing_if = "crate::U64::<5>::is_equal")]
        non_resolving_ns_max_fails: u64,

        #[serde(default = "crate::U64::<60>::value", skip_serializing_if = "crate::U64::<60>::is_equal")]
        non_resolving_ns_throttle_time: u64,

        #[serde(default = "crate::default_value_outgoing_source_address", skip_serializing_if = "crate::default_value_equal_outgoing_source_address")]
        source_address: Vec<String>,

        #[serde(default = "crate::U64::<64>::value", skip_serializing_if = "crate::U64::<64>::is_equal")]
        server_down_max_fails: u64,

        #[serde(default = "crate::U64::<60>::value", skip_serializing_if = "crate::U64::<60>::is_equal")]
        server_down_throttle_time: u64,

        #[serde(default = "crate::U64::<25>::value", skip_serializing_if = "crate::U64::<25>::is_equal")]
        bypass_server_throttling_probability: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        single_socket: bool,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        tcp_fast_open_connect: bool,

        #[serde(default = "crate::U64::<10000>::value", skip_serializing_if = "crate::U64::<10000>::is_equal")]
        tcp_max_idle_ms: u64,

        #[serde(default = "crate::U64::<10>::value", skip_serializing_if = "crate::U64::<10>::is_equal")]
        tcp_max_idle_per_auth: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        tcp_max_queries: u64,

        #[serde(default = "crate::U64::<100>::value", skip_serializing_if = "crate::U64::<100>::is_equal")]
        tcp_max_idle_per_thread: u64,

        #[serde(default = "crate::U64::<1024>::value", skip_serializing_if = "crate::U64::<1024>::is_equal")]
        udp_source_port_min: u64,

        #[serde(default = "crate::U64::<65535>::value", skip_serializing_if = "crate::U64::<65535>::is_equal")]
        udp_source_port_max: u64,

        #[serde(default = "crate::default_value_outgoing_udp_source_port_avoid", skip_serializing_if = "crate::default_value_equal_outgoing_udp_source_port_avoid")]
        udp_source_port_avoid: Vec<String>,

    }
    // END SECTION Outgoing

    // SECTION Packetcache
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct Packetcache {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        disable: bool,

        #[serde(default = "crate::U64::<500000>::value", skip_serializing_if = "crate::U64::<500000>::is_equal")]
        max_entries: u64,

        #[serde(default = "crate::U64::<86400>::value", skip_serializing_if = "crate::U64::<86400>::is_equal")]
        ttl: u64,

        #[serde(default = "crate::U64::<60>::value", skip_serializing_if = "crate::U64::<60>::is_equal")]
        negative_ttl: u64,

        #[serde(default = "crate::U64::<60>::value", skip_serializing_if = "crate::U64::<60>::is_equal")]
        servfail_ttl: u64,

        #[serde(default = "crate::U64::<1024>::value", skip_serializing_if = "crate::U64::<1024>::is_equal")]
        shards: u64,

    }
    // END SECTION Packetcache

    // SECTION Logging
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct Logging {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        disable_syslog: bool,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        timestamp: bool,

        #[serde(default = "crate::U64::<6>::value", skip_serializing_if = "crate::U64::<6>::is_equal")]
        loglevel: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        common_errors: bool,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        rpz_changes: bool,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        facility: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        protobuf_use_kernel_timestamp: bool,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        quiet: bool,

        #[serde(default = "crate::U64::<1800>::value", skip_serializing_if = "crate::U64::<1800>::is_equal")]
        statistics_interval: u64,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        structured_logging: bool,

        #[serde(default = "crate::default_value_logging_structured_logging_backend", skip_serializing_if = "crate::default_value_equal_logging_structured_logging_backend")]
        structured_logging_backend: String,

        #[serde(default = "crate::default_value_logging_trace", skip_serializing_if = "crate::default_value_equal_logging_trace")]
        trace: String,

        #[serde(default = "crate::default_value_logging_protobuf_servers", skip_serializing_if = "crate::default_value_equal_logging_protobuf_servers")]
        protobuf_servers: Vec<ProtobufServer>,

        #[serde(default = "crate::default_value_logging_outgoing_protobuf_servers", skip_serializing_if = "crate::default_value_equal_logging_outgoing_protobuf_servers")]
        outgoing_protobuf_servers: Vec<ProtobufServer>,

        #[serde(default = "crate::U64::<32>::value", skip_serializing_if = "crate::U64::<32>::is_equal")]
        protobuf_mask_v4: u64,

        #[serde(default = "crate::U64::<128>::value", skip_serializing_if = "crate::U64::<128>::is_equal")]
        protobuf_mask_v6: u64,

        #[serde(default = "crate::default_value_logging_dnstap_framestream_servers", skip_serializing_if = "crate::default_value_equal_logging_dnstap_framestream_servers")]
        dnstap_framestream_servers: Vec<DNSTapFrameStreamServer>,

        #[serde(default = "crate::default_value_logging_dnstap_nod_framestream_servers", skip_serializing_if = "crate::default_value_equal_logging_dnstap_nod_framestream_servers")]
        dnstap_nod_framestream_servers: Vec<DNSTapNODFrameStreamServer>,

    }
    // END SECTION Logging

    // SECTION Ecs
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct Ecs {
        #[serde(default = "crate::default_value_ecs_add_for", skip_serializing_if = "crate::default_value_equal_ecs_add_for")]
        add_for: Vec<String>,

        #[serde(default = "crate::U64::<24>::value", skip_serializing_if = "crate::U64::<24>::is_equal")]
        ipv4_bits: u64,

        #[serde(default = "crate::U64::<24>::value", skip_serializing_if = "crate::U64::<24>::is_equal")]
        ipv4_cache_bits: u64,

        #[serde(default = "crate::U64::<56>::value", skip_serializing_if = "crate::U64::<56>::is_equal")]
        ipv6_bits: u64,

        #[serde(default = "crate::U64::<56>::value", skip_serializing_if = "crate::U64::<56>::is_equal")]
        ipv6_cache_bits: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        ipv4_never_cache: bool,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        ipv6_never_cache: bool,

        #[serde(default = "crate::U64::<1>::value", skip_serializing_if = "crate::U64::<1>::is_equal")]
        minimum_ttl_override: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        cache_limit_ttl: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        scope_zero_address: String,

    }
    // END SECTION Ecs

    // SECTION Recordcache
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct Recordcache {
        #[serde(default = "crate::U64::<3600>::value", skip_serializing_if = "crate::U64::<3600>::is_equal")]
        max_cache_bogus_ttl: u64,

        #[serde(default = "crate::U64::<1000000>::value", skip_serializing_if = "crate::U64::<1000000>::is_equal")]
        max_entries: u64,

        #[serde(default = "crate::U64::<86400>::value", skip_serializing_if = "crate::U64::<86400>::is_equal")]
        max_ttl: u64,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        limit_qtype_any: bool,

        #[serde(default = "crate::U64::<256>::value", skip_serializing_if = "crate::U64::<256>::is_equal")]
        max_rrset_size: u64,

        #[serde(default = "crate::U64::<3600>::value", skip_serializing_if = "crate::U64::<3600>::is_equal")]
        max_negative_ttl: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        locked_ttl_perc: u64,

        #[serde(default = "crate::U64::<1024>::value", skip_serializing_if = "crate::U64::<1024>::is_equal")]
        shards: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        refresh_on_ttl_perc: u64,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        serve_stale_extensions: u64,

        #[serde(default = "crate::default_value_recordcache_zonetocaches", skip_serializing_if = "crate::default_value_equal_recordcache_zonetocaches")]
        zonetocaches: Vec<ZoneToCache>,

    }
    // END SECTION Recordcache

    // SECTION Nod
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct Nod {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        tracking: bool,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        log: bool,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        lookup: String,

        #[serde(default = "crate::U64::<67108864>::value", skip_serializing_if = "crate::U64::<67108864>::is_equal")]
        db_size: u64,

        #[serde(default = "crate::default_value_nod_history_dir", skip_serializing_if = "crate::default_value_equal_nod_history_dir")]
        history_dir: String,

        #[serde(default = "crate::U64::<600>::value", skip_serializing_if = "crate::U64::<600>::is_equal")]
        db_snapshot_interval: u64,

        #[serde(default = "crate::default_value_nod_ignore_list", skip_serializing_if = "crate::default_value_equal_nod_ignore_list")]
        ignore_list: Vec<String>,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        ignore_list_file: String,

        #[serde(default = "crate::default_value_nod_pb_tag", skip_serializing_if = "crate::default_value_equal_nod_pb_tag")]
        pb_tag: String,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        unique_response_tracking: bool,

        #[serde(default = "crate::Bool::<true>::value", skip_serializing_if = "crate::if_true")]
        unique_response_log: bool,

        #[serde(default = "crate::U64::<67108864>::value", skip_serializing_if = "crate::U64::<67108864>::is_equal")]
        unique_response_db_size: u64,

        #[serde(default = "crate::default_value_nod_unique_response_history_dir", skip_serializing_if = "crate::default_value_equal_nod_unique_response_history_dir")]
        unique_response_history_dir: String,

        #[serde(default = "crate::default_value_nod_unique_response_pb_tag", skip_serializing_if = "crate::default_value_equal_nod_unique_response_pb_tag")]
        unique_response_pb_tag: String,

        #[serde(default = "crate::default_value_nod_unique_response_ignore_list", skip_serializing_if = "crate::default_value_equal_nod_unique_response_ignore_list")]
        unique_response_ignore_list: Vec<String>,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        unique_response_ignore_list_file: String,

    }
    // END SECTION Nod

    // SECTION Snmp
    #[derive(Deserialize, Serialize, Debug, PartialEq)]
    #[serde(deny_unknown_fields)]
    pub struct Snmp {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        agent: bool,

        #[serde(default, skip_serializing_if = "crate::is_default")]
        daemon_socket: String,

    }
    // END SECTION Snmp

    #[derive(Serialize, Deserialize, Debug)]
    #[serde(deny_unknown_fields)]
    pub struct Recursorsettings {
        #[serde(default, skip_serializing_if = "crate::is_default")]
        dnssec: Dnssec,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        incoming: Incoming,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        recursor: Recursor,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        webservice: Webservice,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        carbon: Carbon,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        outgoing: Outgoing,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        packetcache: Packetcache,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        logging: Logging,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        ecs: Ecs,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        recordcache: Recordcache,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        nod: Nod,
        #[serde(default, skip_serializing_if = "crate::is_default")]
        snmp: Snmp,
}  // End of generated structs
}
impl Default for recsettings::Dnssec {
    fn default() -> Self {
        let deserialized: recsettings::Dnssec = serde_yaml::from_str("").unwrap();
        deserialized
    }
}

impl Default for recsettings::Incoming {
    fn default() -> Self {
        let deserialized: recsettings::Incoming = serde_yaml::from_str("").unwrap();
        deserialized
    }
}

impl Default for recsettings::Recursor {
    fn default() -> Self {
        let deserialized: recsettings::Recursor = serde_yaml::from_str("").unwrap();
        deserialized
    }
}

impl Default for recsettings::Webservice {
    fn default() -> Self {
        let deserialized: recsettings::Webservice = serde_yaml::from_str("").unwrap();
        deserialized
    }
}

impl Default for recsettings::Carbon {
    fn default() -> Self {
        let deserialized: recsettings::Carbon = serde_yaml::from_str("").unwrap();
        deserialized
    }
}

impl Default for recsettings::Outgoing {
    fn default() -> Self {
        let deserialized: recsettings::Outgoing = serde_yaml::from_str("").unwrap();
        deserialized
    }
}

impl Default for recsettings::Packetcache {
    fn default() -> Self {
        let deserialized: recsettings::Packetcache = serde_yaml::from_str("").unwrap();
        deserialized
    }
}

impl Default for recsettings::Logging {
    fn default() -> Self {
        let deserialized: recsettings::Logging = serde_yaml::from_str("").unwrap();
        deserialized
    }
}

impl Default for recsettings::Ecs {
    fn default() -> Self {
        let deserialized: recsettings::Ecs = serde_yaml::from_str("").unwrap();
        deserialized
    }
}

impl Default for recsettings::Recordcache {
    fn default() -> Self {
        let deserialized: recsettings::Recordcache = serde_yaml::from_str("").unwrap();
        deserialized
    }
}

impl Default for recsettings::Nod {
    fn default() -> Self {
        let deserialized: recsettings::Nod = serde_yaml::from_str("").unwrap();
        deserialized
    }
}

impl Default for recsettings::Snmp {
    fn default() -> Self {
        let deserialized: recsettings::Snmp = serde_yaml::from_str("").unwrap();
        deserialized
    }
}

impl Default for recsettings::Recursorsettings {
    fn default() -> Self {
        let deserialized: recsettings::Recursorsettings = serde_yaml::from_str("").unwrap();
        deserialized
    }
}

impl Validate for recsettings::Dnssec {
    fn validate(&self) -> Result<(), ValidationError> {
        let fieldname = "dnssec.trustanchors".to_string();
        validate_vec(&fieldname, &self.trustanchors, |field, element| element.validate(field))?;
        let fieldname = "dnssec.negative_trustanchors".to_string();
        validate_vec(&fieldname, &self.negative_trustanchors, |field, element| element.validate(field))?;
        validate_dnssec(self)
    }
}

impl Validate for recsettings::Incoming {
    fn validate(&self) -> Result<(), ValidationError> {
        let fieldname = "incoming.allow_from".to_string();
        validate_vec(&fieldname, &self.allow_from, validate_subnet)?;
        let fieldname = "incoming.allow_notify_from".to_string();
        validate_vec(&fieldname, &self.allow_notify_from, validate_subnet)?;
        let fieldname = "incoming.edns_padding_from".to_string();
        validate_vec(&fieldname, &self.edns_padding_from, validate_subnet)?;
        let fieldname = "incoming.listen".to_string();
        validate_vec(&fieldname, &self.listen, validate_socket_address)?;
        let fieldname = "incoming.proxy_protocol_from".to_string();
        validate_vec(&fieldname, &self.proxy_protocol_from, validate_subnet)?;
        let fieldname = "incoming.proxy_protocol_exceptions".to_string();
        validate_vec(&fieldname, &self.proxy_protocol_exceptions, validate_socket_address)?;
        let fieldname = "incoming.proxymappings".to_string();
        validate_vec(&fieldname, &self.proxymappings, |field, element| element.validate(field))?;
        validate_incoming(self)
    }
}

impl Validate for recsettings::Recursor {
    fn validate(&self) -> Result<(), ValidationError> {
        let fieldname = "recursor.auth_zones".to_string();
        validate_vec(&fieldname, &self.auth_zones, |field, element| element.validate(field))?;
        let fieldname = "recursor.forward_zones".to_string();
        validate_vec(&fieldname, &self.forward_zones, |field, element| element.validate(field))?;
        let fieldname = "recursor.forward_zones_recurse".to_string();
        validate_vec(&fieldname, &self.forward_zones_recurse, |field, element| element.validate(field))?;
        let fieldname = "recursor.sortlists".to_string();
        validate_vec(&fieldname, &self.sortlists, |field, element| element.validate(field))?;
        let fieldname = "recursor.rpzs".to_string();
        validate_vec(&fieldname, &self.rpzs, |field, element| element.validate(field))?;
        let fieldname = "recursor.allowed_additional_qtypes".to_string();
        validate_vec(&fieldname, &self.allowed_additional_qtypes, |field, element| element.validate(field))?;
        let fieldname = "recursor.forwarding_catalog_zones".to_string();
        validate_vec(&fieldname, &self.forwarding_catalog_zones, |field, element| element.validate(field))?;
        validate_recursor(self)
    }
}

impl Validate for recsettings::Webservice {
    fn validate(&self) -> Result<(), ValidationError> {
        let fieldname = "webservice.allow_from".to_string();
        validate_vec(&fieldname, &self.allow_from, validate_subnet)?;
        validate_webservice(self)
    }
}

impl Validate for recsettings::Carbon {
    fn validate(&self) -> Result<(), ValidationError> {
        let fieldname = "carbon.server".to_string();
        validate_vec(&fieldname, &self.server, validate_socket_address)?;
        validate_carbon(self)
    }
}

impl Validate for recsettings::Outgoing {
    fn validate(&self) -> Result<(), ValidationError> {
        let fieldname = "outgoing.dont_throttle_netmasks".to_string();
        validate_vec(&fieldname, &self.dont_throttle_netmasks, validate_subnet)?;
        let fieldname = "outgoing.dont_query".to_string();
        validate_vec(&fieldname, &self.dont_query, validate_subnet)?;
        let fieldname = "outgoing.source_address".to_string();
        validate_vec(&fieldname, &self.source_address, validate_subnet)?;
        validate_outgoing(self)
    }
}

impl Validate for recsettings::Packetcache {
    fn validate(&self) -> Result<(), ValidationError> {
        validate_packetcache(self)
    }
}

impl Validate for recsettings::Logging {
    fn validate(&self) -> Result<(), ValidationError> {
        let fieldname = "logging.protobuf_servers".to_string();
        validate_vec(&fieldname, &self.protobuf_servers, |field, element| element.validate(field))?;
        let fieldname = "logging.outgoing_protobuf_servers".to_string();
        validate_vec(&fieldname, &self.outgoing_protobuf_servers, |field, element| element.validate(field))?;
        let fieldname = "logging.dnstap_framestream_servers".to_string();
        validate_vec(&fieldname, &self.dnstap_framestream_servers, |field, element| element.validate(field))?;
        let fieldname = "logging.dnstap_nod_framestream_servers".to_string();
        validate_vec(&fieldname, &self.dnstap_nod_framestream_servers, |field, element| element.validate(field))?;
        validate_logging(self)
    }
}

impl Validate for recsettings::Ecs {
    fn validate(&self) -> Result<(), ValidationError> {
        let fieldname = "ecs.add_for".to_string();
        validate_vec(&fieldname, &self.add_for, validate_subnet)?;
        validate_ecs(self)
    }
}

impl Validate for recsettings::Recordcache {
    fn validate(&self) -> Result<(), ValidationError> {
        let fieldname = "recordcache.zonetocaches".to_string();
        validate_vec(&fieldname, &self.zonetocaches, |field, element| element.validate(field))?;
        validate_recordcache(self)
    }
}

impl Validate for recsettings::Nod {
    fn validate(&self) -> Result<(), ValidationError> {
        validate_nod(self)
    }
}

impl Validate for recsettings::Snmp {
    fn validate(&self) -> Result<(), ValidationError> {
        validate_snmp(self)
    }
}

impl crate::recsettings::Recursorsettings {
    fn validate(&self) -> Result<(), ValidationError> {
        self.dnssec.validate()?;
        self.incoming.validate()?;
        self.recursor.validate()?;
        self.webservice.validate()?;
        self.carbon.validate()?;
        self.outgoing.validate()?;
        self.packetcache.validate()?;
        self.logging.validate()?;
        self.ecs.validate()?;
        self.recordcache.validate()?;
        self.nod.validate()?;
        self.snmp.validate()?;
        Ok(())
    }
}

impl Merge for recsettings::Dnssec {
    fn merge(&mut self, rhs: &mut Self, map: Option<&serde_yaml::Mapping>) {
        if let Some(m) = map {
            if m.contains_key("aggressive_nsec_cache_size") {
                rhs.aggressive_nsec_cache_size.clone_into(&mut self.aggressive_nsec_cache_size);
            }
            if m.contains_key("aggressive_cache_min_nsec3_hit_ratio") {
                rhs.aggressive_cache_min_nsec3_hit_ratio.clone_into(&mut self.aggressive_cache_min_nsec3_hit_ratio);
            }
            if m.contains_key("validation") {
                rhs.validation.clone_into(&mut self.validation);
            }
            if m.contains_key("disabled_algorithms") {
                if is_overriding(m, "disabled_algorithms") || self.disabled_algorithms == DEFAULT_CONFIG.dnssec.disabled_algorithms {
                    self.disabled_algorithms.clear();
                }
                merge_vec(&mut self.disabled_algorithms, &mut rhs.disabled_algorithms);
            }
            if m.contains_key("log_bogus") {
                rhs.log_bogus.clone_into(&mut self.log_bogus);
            }
            if m.contains_key("nsec3_max_iterations") {
                rhs.nsec3_max_iterations.clone_into(&mut self.nsec3_max_iterations);
            }
            if m.contains_key("max_rrsigs_per_record") {
                rhs.max_rrsigs_per_record.clone_into(&mut self.max_rrsigs_per_record);
            }
            if m.contains_key("max_nsec3s_per_record") {
                rhs.max_nsec3s_per_record.clone_into(&mut self.max_nsec3s_per_record);
            }
            if m.contains_key("max_signature_validations_per_query") {
                rhs.max_signature_validations_per_query.clone_into(&mut self.max_signature_validations_per_query);
            }
            if m.contains_key("max_nsec3_hash_computations_per_query") {
                rhs.max_nsec3_hash_computations_per_query.clone_into(&mut self.max_nsec3_hash_computations_per_query);
            }
            if m.contains_key("aggressive_cache_max_nsec3_hash_cost") {
                rhs.aggressive_cache_max_nsec3_hash_cost.clone_into(&mut self.aggressive_cache_max_nsec3_hash_cost);
            }
            if m.contains_key("max_ds_per_zone") {
                rhs.max_ds_per_zone.clone_into(&mut self.max_ds_per_zone);
            }
            if m.contains_key("max_dnskeys") {
                rhs.max_dnskeys.clone_into(&mut self.max_dnskeys);
            }
            if m.contains_key("signature_inception_skew") {
                rhs.signature_inception_skew.clone_into(&mut self.signature_inception_skew);
            }
            if m.contains_key("x_dnssec_names") {
                if is_overriding(m, "x_dnssec_names") || self.x_dnssec_names == DEFAULT_CONFIG.dnssec.x_dnssec_names {
                    self.x_dnssec_names.clear();
                }
                merge_vec(&mut self.x_dnssec_names, &mut rhs.x_dnssec_names);
            }
            if m.contains_key("trustanchors") {
                if is_overriding(m, "trustanchors") || self.trustanchors == DEFAULT_CONFIG.dnssec.trustanchors {
                    self.trustanchors.clear();
                }
                merge_vec(&mut self.trustanchors, &mut rhs.trustanchors);
            }
            if m.contains_key("negative_trustanchors") {
                if is_overriding(m, "negative_trustanchors") || self.negative_trustanchors == DEFAULT_CONFIG.dnssec.negative_trustanchors {
                    self.negative_trustanchors.clear();
                }
                merge_vec(&mut self.negative_trustanchors, &mut rhs.negative_trustanchors);
            }
            if m.contains_key("trustanchorfile") {
                rhs.trustanchorfile.clone_into(&mut self.trustanchorfile);
            }
            if m.contains_key("trustanchorfile_interval") {
                rhs.trustanchorfile_interval.clone_into(&mut self.trustanchorfile_interval);
            }
        }
    }
}

impl Merge for recsettings::Incoming {
    fn merge(&mut self, rhs: &mut Self, map: Option<&serde_yaml::Mapping>) {
        if let Some(m) = map {
            if m.contains_key("allow_from") {
                if is_overriding(m, "allow_from") || self.allow_from == DEFAULT_CONFIG.incoming.allow_from {
                    self.allow_from.clear();
                }
                merge_vec(&mut self.allow_from, &mut rhs.allow_from);
            }
            if m.contains_key("allow_from_file") {
                rhs.allow_from_file.clone_into(&mut self.allow_from_file);
            }
            if m.contains_key("allow_notify_for") {
                if is_overriding(m, "allow_notify_for") || self.allow_notify_for == DEFAULT_CONFIG.incoming.allow_notify_for {
                    self.allow_notify_for.clear();
                }
                merge_vec(&mut self.allow_notify_for, &mut rhs.allow_notify_for);
            }
            if m.contains_key("allow_notify_for_file") {
                rhs.allow_notify_for_file.clone_into(&mut self.allow_notify_for_file);
            }
            if m.contains_key("allow_notify_from") {
                if is_overriding(m, "allow_notify_from") || self.allow_notify_from == DEFAULT_CONFIG.incoming.allow_notify_from {
                    self.allow_notify_from.clear();
                }
                merge_vec(&mut self.allow_notify_from, &mut rhs.allow_notify_from);
            }
            if m.contains_key("allow_notify_from_file") {
                rhs.allow_notify_from_file.clone_into(&mut self.allow_notify_from_file);
            }
            if m.contains_key("allow_no_rd") {
                rhs.allow_no_rd.clone_into(&mut self.allow_no_rd);
            }
            if m.contains_key("tcp_timeout") {
                rhs.tcp_timeout.clone_into(&mut self.tcp_timeout);
            }
            if m.contains_key("distribution_load_factor") {
                rhs.distribution_load_factor.clone_into(&mut self.distribution_load_factor);
            }
            if m.contains_key("distribution_pipe_buffer_size") {
                rhs.distribution_pipe_buffer_size.clone_into(&mut self.distribution_pipe_buffer_size);
            }
            if m.contains_key("distributor_threads") {
                rhs.distributor_threads.clone_into(&mut self.distributor_threads);
            }
            if m.contains_key("edns_padding_from") {
                if is_overriding(m, "edns_padding_from") || self.edns_padding_from == DEFAULT_CONFIG.incoming.edns_padding_from {
                    self.edns_padding_from.clear();
                }
                merge_vec(&mut self.edns_padding_from, &mut rhs.edns_padding_from);
            }
            if m.contains_key("edns_padding_mode") {
                rhs.edns_padding_mode.clone_into(&mut self.edns_padding_mode);
            }
            if m.contains_key("edns_padding_tag") {
                rhs.edns_padding_tag.clone_into(&mut self.edns_padding_tag);
            }
            if m.contains_key("gettag_needs_edns_options") {
                rhs.gettag_needs_edns_options.clone_into(&mut self.gettag_needs_edns_options);
            }
            if m.contains_key("listen") {
                if is_overriding(m, "listen") || self.listen == DEFAULT_CONFIG.incoming.listen {
                    self.listen.clear();
                }
                merge_vec(&mut self.listen, &mut rhs.listen);
            }
            if m.contains_key("port") {
                rhs.port.clone_into(&mut self.port);
            }
            if m.contains_key("non_local_bind") {
                rhs.non_local_bind.clone_into(&mut self.non_local_bind);
            }
            if m.contains_key("max_concurrent_requests_per_tcp_connection") {
                rhs.max_concurrent_requests_per_tcp_connection.clone_into(&mut self.max_concurrent_requests_per_tcp_connection);
            }
            if m.contains_key("max_tcp_clients") {
                rhs.max_tcp_clients.clone_into(&mut self.max_tcp_clients);
            }
            if m.contains_key("max_tcp_per_client") {
                rhs.max_tcp_per_client.clone_into(&mut self.max_tcp_per_client);
            }
            if m.contains_key("max_tcp_queries_per_connection") {
                rhs.max_tcp_queries_per_connection.clone_into(&mut self.max_tcp_queries_per_connection);
            }
            if m.contains_key("max_udp_queries_per_round") {
                rhs.max_udp_queries_per_round.clone_into(&mut self.max_udp_queries_per_round);
            }
            if m.contains_key("pdns_distributes_queries") {
                rhs.pdns_distributes_queries.clone_into(&mut self.pdns_distributes_queries);
            }
            if m.contains_key("proxy_protocol_from") {
                if is_overriding(m, "proxy_protocol_from") || self.proxy_protocol_from == DEFAULT_CONFIG.incoming.proxy_protocol_from {
                    self.proxy_protocol_from.clear();
                }
                merge_vec(&mut self.proxy_protocol_from, &mut rhs.proxy_protocol_from);
            }
            if m.contains_key("proxy_protocol_exceptions") {
                if is_overriding(m, "proxy_protocol_exceptions") || self.proxy_protocol_exceptions == DEFAULT_CONFIG.incoming.proxy_protocol_exceptions {
                    self.proxy_protocol_exceptions.clear();
                }
                merge_vec(&mut self.proxy_protocol_exceptions, &mut rhs.proxy_protocol_exceptions);
            }
            if m.contains_key("proxy_protocol_maximum_size") {
                rhs.proxy_protocol_maximum_size.clone_into(&mut self.proxy_protocol_maximum_size);
            }
            if m.contains_key("reuseport") {
                rhs.reuseport.clone_into(&mut self.reuseport);
            }
            if m.contains_key("tcp_fast_open") {
                rhs.tcp_fast_open.clone_into(&mut self.tcp_fast_open);
            }
            if m.contains_key("udp_truncation_threshold") {
                rhs.udp_truncation_threshold.clone_into(&mut self.udp_truncation_threshold);
            }
            if m.contains_key("use_incoming_edns_subnet") {
                rhs.use_incoming_edns_subnet.clone_into(&mut self.use_incoming_edns_subnet);
            }
            if m.contains_key("proxymappings") {
                if is_overriding(m, "proxymappings") || self.proxymappings == DEFAULT_CONFIG.incoming.proxymappings {
                    self.proxymappings.clear();
                }
                merge_vec(&mut self.proxymappings, &mut rhs.proxymappings);
            }
        }
    }
}

impl Merge for recsettings::Recursor {
    fn merge(&mut self, rhs: &mut Self, map: Option<&serde_yaml::Mapping>) {
        if let Some(m) = map {
            if m.contains_key("any_to_tcp") {
                rhs.any_to_tcp.clone_into(&mut self.any_to_tcp);
            }
            if m.contains_key("allow_trust_anchor_query") {
                rhs.allow_trust_anchor_query.clone_into(&mut self.allow_trust_anchor_query);
            }
            if m.contains_key("auth_zones") {
                if is_overriding(m, "auth_zones") || self.auth_zones == DEFAULT_CONFIG.recursor.auth_zones {
                    self.auth_zones.clear();
                }
                merge_vec(&mut self.auth_zones, &mut rhs.auth_zones);
            }
            if m.contains_key("chroot") {
                rhs.chroot.clone_into(&mut self.chroot);
            }
            if m.contains_key("config_dir") {
                rhs.config_dir.clone_into(&mut self.config_dir);
            }
            if m.contains_key("config_name") {
                rhs.config_name.clone_into(&mut self.config_name);
            }
            if m.contains_key("cpu_map") {
                rhs.cpu_map.clone_into(&mut self.cpu_map);
            }
            if m.contains_key("daemon") {
                rhs.daemon.clone_into(&mut self.daemon);
            }
            if m.contains_key("devonly_regression_test_mode") {
                rhs.devonly_regression_test_mode.clone_into(&mut self.devonly_regression_test_mode);
            }
            if m.contains_key("dns64_prefix") {
                rhs.dns64_prefix.clone_into(&mut self.dns64_prefix);
            }
            if m.contains_key("etc_hosts_file") {
                rhs.etc_hosts_file.clone_into(&mut self.etc_hosts_file);
            }
            if m.contains_key("event_trace_enabled") {
                rhs.event_trace_enabled.clone_into(&mut self.event_trace_enabled);
            }
            if m.contains_key("export_etc_hosts") {
                rhs.export_etc_hosts.clone_into(&mut self.export_etc_hosts);
            }
            if m.contains_key("export_etc_hosts_search_suffix") {
                rhs.export_etc_hosts_search_suffix.clone_into(&mut self.export_etc_hosts_search_suffix);
            }
            if m.contains_key("extended_resolution_errors") {
                rhs.extended_resolution_errors.clone_into(&mut self.extended_resolution_errors);
            }
            if m.contains_key("forward_zones") {
                if is_overriding(m, "forward_zones") || self.forward_zones == DEFAULT_CONFIG.recursor.forward_zones {
                    self.forward_zones.clear();
                }
                merge_vec(&mut self.forward_zones, &mut rhs.forward_zones);
            }
            if m.contains_key("forward_zones_file") {
                rhs.forward_zones_file.clone_into(&mut self.forward_zones_file);
            }
            if m.contains_key("forward_zones_recurse") {
                if is_overriding(m, "forward_zones_recurse") || self.forward_zones_recurse == DEFAULT_CONFIG.recursor.forward_zones_recurse {
                    self.forward_zones_recurse.clear();
                }
                merge_vec(&mut self.forward_zones_recurse, &mut rhs.forward_zones_recurse);
            }
            if m.contains_key("hint_file") {
                rhs.hint_file.clone_into(&mut self.hint_file);
            }
            if m.contains_key("ignore_unknown_settings") {
                if is_overriding(m, "ignore_unknown_settings") || self.ignore_unknown_settings == DEFAULT_CONFIG.recursor.ignore_unknown_settings {
                    self.ignore_unknown_settings.clear();
                }
                merge_vec(&mut self.ignore_unknown_settings, &mut rhs.ignore_unknown_settings);
            }
            if m.contains_key("include_dir") {
                rhs.include_dir.clone_into(&mut self.include_dir);
            }
            if m.contains_key("latency_statistic_size") {
                rhs.latency_statistic_size.clone_into(&mut self.latency_statistic_size);
            }
            if m.contains_key("lua_config_file") {
                rhs.lua_config_file.clone_into(&mut self.lua_config_file);
            }
            if m.contains_key("lua_global_include_dir") {
                rhs.lua_global_include_dir.clone_into(&mut self.lua_global_include_dir);
            }
            if m.contains_key("lua_dns_script") {
                rhs.lua_dns_script.clone_into(&mut self.lua_dns_script);
            }
            if m.contains_key("lua_maintenance_interval") {
                rhs.lua_maintenance_interval.clone_into(&mut self.lua_maintenance_interval);
            }
            if m.contains_key("max_chain_length") {
                rhs.max_chain_length.clone_into(&mut self.max_chain_length);
            }
            if m.contains_key("max_include_depth") {
                rhs.max_include_depth.clone_into(&mut self.max_include_depth);
            }
            if m.contains_key("max_generate_steps") {
                rhs.max_generate_steps.clone_into(&mut self.max_generate_steps);
            }
            if m.contains_key("max_mthreads") {
                rhs.max_mthreads.clone_into(&mut self.max_mthreads);
            }
            if m.contains_key("max_cnames_followed") {
                rhs.max_cnames_followed.clone_into(&mut self.max_cnames_followed);
            }
            if m.contains_key("max_recursion_depth") {
                rhs.max_recursion_depth.clone_into(&mut self.max_recursion_depth);
            }
            if m.contains_key("max_total_msec") {
                rhs.max_total_msec.clone_into(&mut self.max_total_msec);
            }
            if m.contains_key("minimum_ttl_override") {
                rhs.minimum_ttl_override.clone_into(&mut self.minimum_ttl_override);
            }
            if m.contains_key("nothing_below_nxdomain") {
                rhs.nothing_below_nxdomain.clone_into(&mut self.nothing_below_nxdomain);
            }
            if m.contains_key("public_suffix_list_file") {
                rhs.public_suffix_list_file.clone_into(&mut self.public_suffix_list_file);
            }
            if m.contains_key("qname_minimization") {
                rhs.qname_minimization.clone_into(&mut self.qname_minimization);
            }
            if m.contains_key("qname_max_minimize_count") {
                rhs.qname_max_minimize_count.clone_into(&mut self.qname_max_minimize_count);
            }
            if m.contains_key("qname_minimize_one_label") {
                rhs.qname_minimize_one_label.clone_into(&mut self.qname_minimize_one_label);
            }
            if m.contains_key("root_nx_trust") {
                rhs.root_nx_trust.clone_into(&mut self.root_nx_trust);
            }
            if m.contains_key("save_parent_ns_set") {
                rhs.save_parent_ns_set.clone_into(&mut self.save_parent_ns_set);
            }
            if m.contains_key("security_poll_suffix") {
                rhs.security_poll_suffix.clone_into(&mut self.security_poll_suffix);
            }
            if m.contains_key("serve_rfc1918") {
                rhs.serve_rfc1918.clone_into(&mut self.serve_rfc1918);
            }
            if m.contains_key("serve_rfc6303") {
                rhs.serve_rfc6303.clone_into(&mut self.serve_rfc6303);
            }
            if m.contains_key("server_id") {
                rhs.server_id.clone_into(&mut self.server_id);
            }
            if m.contains_key("setgid") {
                rhs.setgid.clone_into(&mut self.setgid);
            }
            if m.contains_key("setuid") {
                rhs.setuid.clone_into(&mut self.setuid);
            }
            if m.contains_key("socket_dir") {
                rhs.socket_dir.clone_into(&mut self.socket_dir);
            }
            if m.contains_key("socket_group") {
                rhs.socket_group.clone_into(&mut self.socket_group);
            }
            if m.contains_key("socket_mode") {
                rhs.socket_mode.clone_into(&mut self.socket_mode);
            }
            if m.contains_key("socket_owner") {
                rhs.socket_owner.clone_into(&mut self.socket_owner);
            }
            if m.contains_key("spoof_nearmiss_max") {
                rhs.spoof_nearmiss_max.clone_into(&mut self.spoof_nearmiss_max);
            }
            if m.contains_key("stack_cache_size") {
                rhs.stack_cache_size.clone_into(&mut self.stack_cache_size);
            }
            if m.contains_key("stack_size") {
                rhs.stack_size.clone_into(&mut self.stack_size);
            }
            if m.contains_key("stats_api_disabled_list") {
                if is_overriding(m, "stats_api_disabled_list") || self.stats_api_disabled_list == DEFAULT_CONFIG.recursor.stats_api_disabled_list {
                    self.stats_api_disabled_list.clear();
                }
                merge_vec(&mut self.stats_api_disabled_list, &mut rhs.stats_api_disabled_list);
            }
            if m.contains_key("stats_carbon_disabled_list") {
                if is_overriding(m, "stats_carbon_disabled_list") || self.stats_carbon_disabled_list == DEFAULT_CONFIG.recursor.stats_carbon_disabled_list {
                    self.stats_carbon_disabled_list.clear();
                }
                merge_vec(&mut self.stats_carbon_disabled_list, &mut rhs.stats_carbon_disabled_list);
            }
            if m.contains_key("stats_rec_control_disabled_list") {
                if is_overriding(m, "stats_rec_control_disabled_list") || self.stats_rec_control_disabled_list == DEFAULT_CONFIG.recursor.stats_rec_control_disabled_list {
                    self.stats_rec_control_disabled_list.clear();
                }
                merge_vec(&mut self.stats_rec_control_disabled_list, &mut rhs.stats_rec_control_disabled_list);
            }
            if m.contains_key("stats_ringbuffer_entries") {
                rhs.stats_ringbuffer_entries.clone_into(&mut self.stats_ringbuffer_entries);
            }
            if m.contains_key("stats_snmp_disabled_list") {
                if is_overriding(m, "stats_snmp_disabled_list") || self.stats_snmp_disabled_list == DEFAULT_CONFIG.recursor.stats_snmp_disabled_list {
                    self.stats_snmp_disabled_list.clear();
                }
                merge_vec(&mut self.stats_snmp_disabled_list, &mut rhs.stats_snmp_disabled_list);
            }
            if m.contains_key("threads") {
                rhs.threads.clone_into(&mut self.threads);
            }
            if m.contains_key("tcp_threads") {
                rhs.tcp_threads.clone_into(&mut self.tcp_threads);
            }
            if m.contains_key("version_string") {
                rhs.version_string.clone_into(&mut self.version_string);
            }
            if m.contains_key("write_pid") {
                rhs.write_pid.clone_into(&mut self.write_pid);
            }
            if m.contains_key("system_resolver_ttl") {
                rhs.system_resolver_ttl.clone_into(&mut self.system_resolver_ttl);
            }
            if m.contains_key("system_resolver_interval") {
                rhs.system_resolver_interval.clone_into(&mut self.system_resolver_interval);
            }
            if m.contains_key("system_resolver_self_resolve_check") {
                rhs.system_resolver_self_resolve_check.clone_into(&mut self.system_resolver_self_resolve_check);
            }
            if m.contains_key("sortlists") {
                if is_overriding(m, "sortlists") || self.sortlists == DEFAULT_CONFIG.recursor.sortlists {
                    self.sortlists.clear();
                }
                merge_vec(&mut self.sortlists, &mut rhs.sortlists);
            }
            if m.contains_key("rpzs") {
                if is_overriding(m, "rpzs") || self.rpzs == DEFAULT_CONFIG.recursor.rpzs {
                    self.rpzs.clear();
                }
                merge_vec(&mut self.rpzs, &mut rhs.rpzs);
            }
            if m.contains_key("allowed_additional_qtypes") {
                if is_overriding(m, "allowed_additional_qtypes") || self.allowed_additional_qtypes == DEFAULT_CONFIG.recursor.allowed_additional_qtypes {
                    self.allowed_additional_qtypes.clear();
                }
                merge_vec(&mut self.allowed_additional_qtypes, &mut rhs.allowed_additional_qtypes);
            }
            if m.contains_key("lua_start_stop_script") {
                rhs.lua_start_stop_script.clone_into(&mut self.lua_start_stop_script);
            }
            if m.contains_key("forwarding_catalog_zones") {
                if is_overriding(m, "forwarding_catalog_zones") || self.forwarding_catalog_zones == DEFAULT_CONFIG.recursor.forwarding_catalog_zones {
                    self.forwarding_catalog_zones.clear();
                }
                merge_vec(&mut self.forwarding_catalog_zones, &mut rhs.forwarding_catalog_zones);
            }
        }
    }
}

impl Merge for recsettings::Webservice {
    fn merge(&mut self, rhs: &mut Self, map: Option<&serde_yaml::Mapping>) {
        if let Some(m) = map {
            if m.contains_key("api_dir") {
                rhs.api_dir.clone_into(&mut self.api_dir);
            }
            if m.contains_key("api_key") {
                rhs.api_key.clone_into(&mut self.api_key);
            }
            if m.contains_key("webserver") {
                rhs.webserver.clone_into(&mut self.webserver);
            }
            if m.contains_key("address") {
                rhs.address.clone_into(&mut self.address);
            }
            if m.contains_key("allow_from") {
                if is_overriding(m, "allow_from") || self.allow_from == DEFAULT_CONFIG.webservice.allow_from {
                    self.allow_from.clear();
                }
                merge_vec(&mut self.allow_from, &mut rhs.allow_from);
            }
            if m.contains_key("hash_plaintext_credentials") {
                rhs.hash_plaintext_credentials.clone_into(&mut self.hash_plaintext_credentials);
            }
            if m.contains_key("loglevel") {
                rhs.loglevel.clone_into(&mut self.loglevel);
            }
            if m.contains_key("password") {
                rhs.password.clone_into(&mut self.password);
            }
            if m.contains_key("port") {
                rhs.port.clone_into(&mut self.port);
            }
        }
    }
}

impl Merge for recsettings::Carbon {
    fn merge(&mut self, rhs: &mut Self, map: Option<&serde_yaml::Mapping>) {
        if let Some(m) = map {
            if m.contains_key("interval") {
                rhs.interval.clone_into(&mut self.interval);
            }
            if m.contains_key("ns") {
                rhs.ns.clone_into(&mut self.ns);
            }
            if m.contains_key("ourname") {
                rhs.ourname.clone_into(&mut self.ourname);
            }
            if m.contains_key("instance") {
                rhs.instance.clone_into(&mut self.instance);
            }
            if m.contains_key("server") {
                if is_overriding(m, "server") || self.server == DEFAULT_CONFIG.carbon.server {
                    self.server.clear();
                }
                merge_vec(&mut self.server, &mut rhs.server);
            }
        }
    }
}

impl Merge for recsettings::Outgoing {
    fn merge(&mut self, rhs: &mut Self, map: Option<&serde_yaml::Mapping>) {
        if let Some(m) = map {
            if m.contains_key("dont_throttle_names") {
                if is_overriding(m, "dont_throttle_names") || self.dont_throttle_names == DEFAULT_CONFIG.outgoing.dont_throttle_names {
                    self.dont_throttle_names.clear();
                }
                merge_vec(&mut self.dont_throttle_names, &mut rhs.dont_throttle_names);
            }
            if m.contains_key("dont_throttle_netmasks") {
                if is_overriding(m, "dont_throttle_netmasks") || self.dont_throttle_netmasks == DEFAULT_CONFIG.outgoing.dont_throttle_netmasks {
                    self.dont_throttle_netmasks.clear();
                }
                merge_vec(&mut self.dont_throttle_netmasks, &mut rhs.dont_throttle_netmasks);
            }
            if m.contains_key("dot_to_auth_names") {
                if is_overriding(m, "dot_to_auth_names") || self.dot_to_auth_names == DEFAULT_CONFIG.outgoing.dot_to_auth_names {
                    self.dot_to_auth_names.clear();
                }
                merge_vec(&mut self.dot_to_auth_names, &mut rhs.dot_to_auth_names);
            }
            if m.contains_key("dot_to_port_853") {
                rhs.dot_to_port_853.clone_into(&mut self.dot_to_port_853);
            }
            if m.contains_key("dont_query") {
                if is_overriding(m, "dont_query") || self.dont_query == DEFAULT_CONFIG.outgoing.dont_query {
                    self.dont_query.clear();
                }
                merge_vec(&mut self.dont_query, &mut rhs.dont_query);
            }
            if m.contains_key("edns_bufsize") {
                rhs.edns_bufsize.clone_into(&mut self.edns_bufsize);
            }
            if m.contains_key("edns_padding") {
                rhs.edns_padding.clone_into(&mut self.edns_padding);
            }
            if m.contains_key("edns_subnet_allow_list") {
                if is_overriding(m, "edns_subnet_allow_list") || self.edns_subnet_allow_list == DEFAULT_CONFIG.outgoing.edns_subnet_allow_list {
                    self.edns_subnet_allow_list.clear();
                }
                merge_vec(&mut self.edns_subnet_allow_list, &mut rhs.edns_subnet_allow_list);
            }
            if m.contains_key("lowercase") {
                rhs.lowercase.clone_into(&mut self.lowercase);
            }
            if m.contains_key("max_busy_dot_probes") {
                rhs.max_busy_dot_probes.clone_into(&mut self.max_busy_dot_probes);
            }
            if m.contains_key("max_qperq") {
                rhs.max_qperq.clone_into(&mut self.max_qperq);
            }
            if m.contains_key("max_ns_address_qperq") {
                rhs.max_ns_address_qperq.clone_into(&mut self.max_ns_address_qperq);
            }
            if m.contains_key("max_ns_per_resolve") {
                rhs.max_ns_per_resolve.clone_into(&mut self.max_ns_per_resolve);
            }
            if m.contains_key("network_timeout") {
                rhs.network_timeout.clone_into(&mut self.network_timeout);
            }
            if m.contains_key("non_resolving_ns_max_fails") {
                rhs.non_resolving_ns_max_fails.clone_into(&mut self.non_resolving_ns_max_fails);
            }
            if m.contains_key("non_resolving_ns_throttle_time") {
                rhs.non_resolving_ns_throttle_time.clone_into(&mut self.non_resolving_ns_throttle_time);
            }
            if m.contains_key("source_address") {
                if is_overriding(m, "source_address") || self.source_address == DEFAULT_CONFIG.outgoing.source_address {
                    self.source_address.clear();
                }
                merge_vec(&mut self.source_address, &mut rhs.source_address);
            }
            if m.contains_key("server_down_max_fails") {
                rhs.server_down_max_fails.clone_into(&mut self.server_down_max_fails);
            }
            if m.contains_key("server_down_throttle_time") {
                rhs.server_down_throttle_time.clone_into(&mut self.server_down_throttle_time);
            }
            if m.contains_key("bypass_server_throttling_probability") {
                rhs.bypass_server_throttling_probability.clone_into(&mut self.bypass_server_throttling_probability);
            }
            if m.contains_key("single_socket") {
                rhs.single_socket.clone_into(&mut self.single_socket);
            }
            if m.contains_key("tcp_fast_open_connect") {
                rhs.tcp_fast_open_connect.clone_into(&mut self.tcp_fast_open_connect);
            }
            if m.contains_key("tcp_max_idle_ms") {
                rhs.tcp_max_idle_ms.clone_into(&mut self.tcp_max_idle_ms);
            }
            if m.contains_key("tcp_max_idle_per_auth") {
                rhs.tcp_max_idle_per_auth.clone_into(&mut self.tcp_max_idle_per_auth);
            }
            if m.contains_key("tcp_max_queries") {
                rhs.tcp_max_queries.clone_into(&mut self.tcp_max_queries);
            }
            if m.contains_key("tcp_max_idle_per_thread") {
                rhs.tcp_max_idle_per_thread.clone_into(&mut self.tcp_max_idle_per_thread);
            }
            if m.contains_key("udp_source_port_min") {
                rhs.udp_source_port_min.clone_into(&mut self.udp_source_port_min);
            }
            if m.contains_key("udp_source_port_max") {
                rhs.udp_source_port_max.clone_into(&mut self.udp_source_port_max);
            }
            if m.contains_key("udp_source_port_avoid") {
                if is_overriding(m, "udp_source_port_avoid") || self.udp_source_port_avoid == DEFAULT_CONFIG.outgoing.udp_source_port_avoid {
                    self.udp_source_port_avoid.clear();
                }
                merge_vec(&mut self.udp_source_port_avoid, &mut rhs.udp_source_port_avoid);
            }
        }
    }
}

impl Merge for recsettings::Packetcache {
    fn merge(&mut self, rhs: &mut Self, map: Option<&serde_yaml::Mapping>) {
        if let Some(m) = map {
            if m.contains_key("disable") {
                rhs.disable.clone_into(&mut self.disable);
            }
            if m.contains_key("max_entries") {
                rhs.max_entries.clone_into(&mut self.max_entries);
            }
            if m.contains_key("ttl") {
                rhs.ttl.clone_into(&mut self.ttl);
            }
            if m.contains_key("negative_ttl") {
                rhs.negative_ttl.clone_into(&mut self.negative_ttl);
            }
            if m.contains_key("servfail_ttl") {
                rhs.servfail_ttl.clone_into(&mut self.servfail_ttl);
            }
            if m.contains_key("shards") {
                rhs.shards.clone_into(&mut self.shards);
            }
        }
    }
}

impl Merge for recsettings::Logging {
    fn merge(&mut self, rhs: &mut Self, map: Option<&serde_yaml::Mapping>) {
        if let Some(m) = map {
            if m.contains_key("disable_syslog") {
                rhs.disable_syslog.clone_into(&mut self.disable_syslog);
            }
            if m.contains_key("timestamp") {
                rhs.timestamp.clone_into(&mut self.timestamp);
            }
            if m.contains_key("loglevel") {
                rhs.loglevel.clone_into(&mut self.loglevel);
            }
            if m.contains_key("common_errors") {
                rhs.common_errors.clone_into(&mut self.common_errors);
            }
            if m.contains_key("rpz_changes") {
                rhs.rpz_changes.clone_into(&mut self.rpz_changes);
            }
            if m.contains_key("facility") {
                rhs.facility.clone_into(&mut self.facility);
            }
            if m.contains_key("protobuf_use_kernel_timestamp") {
                rhs.protobuf_use_kernel_timestamp.clone_into(&mut self.protobuf_use_kernel_timestamp);
            }
            if m.contains_key("quiet") {
                rhs.quiet.clone_into(&mut self.quiet);
            }
            if m.contains_key("statistics_interval") {
                rhs.statistics_interval.clone_into(&mut self.statistics_interval);
            }
            if m.contains_key("structured_logging") {
                rhs.structured_logging.clone_into(&mut self.structured_logging);
            }
            if m.contains_key("structured_logging_backend") {
                rhs.structured_logging_backend.clone_into(&mut self.structured_logging_backend);
            }
            if m.contains_key("trace") {
                rhs.trace.clone_into(&mut self.trace);
            }
            if m.contains_key("protobuf_servers") {
                if is_overriding(m, "protobuf_servers") || self.protobuf_servers == DEFAULT_CONFIG.logging.protobuf_servers {
                    self.protobuf_servers.clear();
                }
                merge_vec(&mut self.protobuf_servers, &mut rhs.protobuf_servers);
            }
            if m.contains_key("outgoing_protobuf_servers") {
                if is_overriding(m, "outgoing_protobuf_servers") || self.outgoing_protobuf_servers == DEFAULT_CONFIG.logging.outgoing_protobuf_servers {
                    self.outgoing_protobuf_servers.clear();
                }
                merge_vec(&mut self.outgoing_protobuf_servers, &mut rhs.outgoing_protobuf_servers);
            }
            if m.contains_key("protobuf_mask_v4") {
                rhs.protobuf_mask_v4.clone_into(&mut self.protobuf_mask_v4);
            }
            if m.contains_key("protobuf_mask_v6") {
                rhs.protobuf_mask_v6.clone_into(&mut self.protobuf_mask_v6);
            }
            if m.contains_key("dnstap_framestream_servers") {
                if is_overriding(m, "dnstap_framestream_servers") || self.dnstap_framestream_servers == DEFAULT_CONFIG.logging.dnstap_framestream_servers {
                    self.dnstap_framestream_servers.clear();
                }
                merge_vec(&mut self.dnstap_framestream_servers, &mut rhs.dnstap_framestream_servers);
            }
            if m.contains_key("dnstap_nod_framestream_servers") {
                if is_overriding(m, "dnstap_nod_framestream_servers") || self.dnstap_nod_framestream_servers == DEFAULT_CONFIG.logging.dnstap_nod_framestream_servers {
                    self.dnstap_nod_framestream_servers.clear();
                }
                merge_vec(&mut self.dnstap_nod_framestream_servers, &mut rhs.dnstap_nod_framestream_servers);
            }
        }
    }
}

impl Merge for recsettings::Ecs {
    fn merge(&mut self, rhs: &mut Self, map: Option<&serde_yaml::Mapping>) {
        if let Some(m) = map {
            if m.contains_key("add_for") {
                if is_overriding(m, "add_for") || self.add_for == DEFAULT_CONFIG.ecs.add_for {
                    self.add_for.clear();
                }
                merge_vec(&mut self.add_for, &mut rhs.add_for);
            }
            if m.contains_key("ipv4_bits") {
                rhs.ipv4_bits.clone_into(&mut self.ipv4_bits);
            }
            if m.contains_key("ipv4_cache_bits") {
                rhs.ipv4_cache_bits.clone_into(&mut self.ipv4_cache_bits);
            }
            if m.contains_key("ipv6_bits") {
                rhs.ipv6_bits.clone_into(&mut self.ipv6_bits);
            }
            if m.contains_key("ipv6_cache_bits") {
                rhs.ipv6_cache_bits.clone_into(&mut self.ipv6_cache_bits);
            }
            if m.contains_key("ipv4_never_cache") {
                rhs.ipv4_never_cache.clone_into(&mut self.ipv4_never_cache);
            }
            if m.contains_key("ipv6_never_cache") {
                rhs.ipv6_never_cache.clone_into(&mut self.ipv6_never_cache);
            }
            if m.contains_key("minimum_ttl_override") {
                rhs.minimum_ttl_override.clone_into(&mut self.minimum_ttl_override);
            }
            if m.contains_key("cache_limit_ttl") {
                rhs.cache_limit_ttl.clone_into(&mut self.cache_limit_ttl);
            }
            if m.contains_key("scope_zero_address") {
                rhs.scope_zero_address.clone_into(&mut self.scope_zero_address);
            }
        }
    }
}

impl Merge for recsettings::Recordcache {
    fn merge(&mut self, rhs: &mut Self, map: Option<&serde_yaml::Mapping>) {
        if let Some(m) = map {
            if m.contains_key("max_cache_bogus_ttl") {
                rhs.max_cache_bogus_ttl.clone_into(&mut self.max_cache_bogus_ttl);
            }
            if m.contains_key("max_entries") {
                rhs.max_entries.clone_into(&mut self.max_entries);
            }
            if m.contains_key("max_ttl") {
                rhs.max_ttl.clone_into(&mut self.max_ttl);
            }
            if m.contains_key("limit_qtype_any") {
                rhs.limit_qtype_any.clone_into(&mut self.limit_qtype_any);
            }
            if m.contains_key("max_rrset_size") {
                rhs.max_rrset_size.clone_into(&mut self.max_rrset_size);
            }
            if m.contains_key("max_negative_ttl") {
                rhs.max_negative_ttl.clone_into(&mut self.max_negative_ttl);
            }
            if m.contains_key("locked_ttl_perc") {
                rhs.locked_ttl_perc.clone_into(&mut self.locked_ttl_perc);
            }
            if m.contains_key("shards") {
                rhs.shards.clone_into(&mut self.shards);
            }
            if m.contains_key("refresh_on_ttl_perc") {
                rhs.refresh_on_ttl_perc.clone_into(&mut self.refresh_on_ttl_perc);
            }
            if m.contains_key("serve_stale_extensions") {
                rhs.serve_stale_extensions.clone_into(&mut self.serve_stale_extensions);
            }
            if m.contains_key("zonetocaches") {
                if is_overriding(m, "zonetocaches") || self.zonetocaches == DEFAULT_CONFIG.recordcache.zonetocaches {
                    self.zonetocaches.clear();
                }
                merge_vec(&mut self.zonetocaches, &mut rhs.zonetocaches);
            }
        }
    }
}

impl Merge for recsettings::Nod {
    fn merge(&mut self, rhs: &mut Self, map: Option<&serde_yaml::Mapping>) {
        if let Some(m) = map {
            if m.contains_key("tracking") {
                rhs.tracking.clone_into(&mut self.tracking);
            }
            if m.contains_key("log") {
                rhs.log.clone_into(&mut self.log);
            }
            if m.contains_key("lookup") {
                rhs.lookup.clone_into(&mut self.lookup);
            }
            if m.contains_key("db_size") {
                rhs.db_size.clone_into(&mut self.db_size);
            }
            if m.contains_key("history_dir") {
                rhs.history_dir.clone_into(&mut self.history_dir);
            }
            if m.contains_key("db_snapshot_interval") {
                rhs.db_snapshot_interval.clone_into(&mut self.db_snapshot_interval);
            }
            if m.contains_key("ignore_list") {
                if is_overriding(m, "ignore_list") || self.ignore_list == DEFAULT_CONFIG.nod.ignore_list {
                    self.ignore_list.clear();
                }
                merge_vec(&mut self.ignore_list, &mut rhs.ignore_list);
            }
            if m.contains_key("ignore_list_file") {
                rhs.ignore_list_file.clone_into(&mut self.ignore_list_file);
            }
            if m.contains_key("pb_tag") {
                rhs.pb_tag.clone_into(&mut self.pb_tag);
            }
            if m.contains_key("unique_response_tracking") {
                rhs.unique_response_tracking.clone_into(&mut self.unique_response_tracking);
            }
            if m.contains_key("unique_response_log") {
                rhs.unique_response_log.clone_into(&mut self.unique_response_log);
            }
            if m.contains_key("unique_response_db_size") {
                rhs.unique_response_db_size.clone_into(&mut self.unique_response_db_size);
            }
            if m.contains_key("unique_response_history_dir") {
                rhs.unique_response_history_dir.clone_into(&mut self.unique_response_history_dir);
            }
            if m.contains_key("unique_response_pb_tag") {
                rhs.unique_response_pb_tag.clone_into(&mut self.unique_response_pb_tag);
            }
            if m.contains_key("unique_response_ignore_list") {
                if is_overriding(m, "unique_response_ignore_list") || self.unique_response_ignore_list == DEFAULT_CONFIG.nod.unique_response_ignore_list {
                    self.unique_response_ignore_list.clear();
                }
                merge_vec(&mut self.unique_response_ignore_list, &mut rhs.unique_response_ignore_list);
            }
            if m.contains_key("unique_response_ignore_list_file") {
                rhs.unique_response_ignore_list_file.clone_into(&mut self.unique_response_ignore_list_file);
            }
        }
    }
}

impl Merge for recsettings::Snmp {
    fn merge(&mut self, rhs: &mut Self, map: Option<&serde_yaml::Mapping>) {
        if let Some(m) = map {
            if m.contains_key("agent") {
                rhs.agent.clone_into(&mut self.agent);
            }
            if m.contains_key("daemon_socket") {
                rhs.daemon_socket.clone_into(&mut self.daemon_socket);
            }
        }
    }
}

impl Merge for crate::recsettings::Recursorsettings {
    fn merge(&mut self, rhs: &mut Self, map: Option<&serde_yaml::Mapping>) {
        if let Some(m) = map {
            if let Some(s) = m.get("dnssec") {
                if s.is_mapping() {
                    self.dnssec.merge(&mut rhs.dnssec, s.as_mapping());
                }
            }
            if let Some(s) = m.get("incoming") {
                if s.is_mapping() {
                    self.incoming.merge(&mut rhs.incoming, s.as_mapping());
                }
            }
            if let Some(s) = m.get("recursor") {
                if s.is_mapping() {
                    self.recursor.merge(&mut rhs.recursor, s.as_mapping());
                }
            }
            if let Some(s) = m.get("webservice") {
                if s.is_mapping() {
                    self.webservice.merge(&mut rhs.webservice, s.as_mapping());
                }
            }
            if let Some(s) = m.get("carbon") {
                if s.is_mapping() {
                    self.carbon.merge(&mut rhs.carbon, s.as_mapping());
                }
            }
            if let Some(s) = m.get("outgoing") {
                if s.is_mapping() {
                    self.outgoing.merge(&mut rhs.outgoing, s.as_mapping());
                }
            }
            if let Some(s) = m.get("packetcache") {
                if s.is_mapping() {
                    self.packetcache.merge(&mut rhs.packetcache, s.as_mapping());
                }
            }
            if let Some(s) = m.get("logging") {
                if s.is_mapping() {
                    self.logging.merge(&mut rhs.logging, s.as_mapping());
                }
            }
            if let Some(s) = m.get("ecs") {
                if s.is_mapping() {
                    self.ecs.merge(&mut rhs.ecs, s.as_mapping());
                }
            }
            if let Some(s) = m.get("recordcache") {
                if s.is_mapping() {
                    self.recordcache.merge(&mut rhs.recordcache, s.as_mapping());
                }
            }
            if let Some(s) = m.get("nod") {
                if s.is_mapping() {
                    self.nod.merge(&mut rhs.nod, s.as_mapping());
                }
            }
            if let Some(s) = m.get("snmp") {
                if s.is_mapping() {
                    self.snmp.merge(&mut rhs.snmp, s.as_mapping());
                }
            }
        }
    }
}

// DEFAULT HANDLING for dnssec_validation
fn default_value_dnssec_validation() -> String {
    String::from("process")
}
fn default_value_equal_dnssec_validation(value: &str)-> bool {
    value == default_value_dnssec_validation()
}

// DEFAULT HANDLING for dnssec_disabled_algorithms
fn default_value_dnssec_disabled_algorithms() -> Vec<String> {
    vec![
    ]
}
fn default_value_equal_dnssec_disabled_algorithms(value: &Vec<String>) -> bool {
    let def = default_value_dnssec_disabled_algorithms();
    &def == value
}

// DEFAULT HANDLING for dnssec_x_dnssec_names
fn default_value_dnssec_x_dnssec_names() -> Vec<String> {
    vec![
    ]
}
fn default_value_equal_dnssec_x_dnssec_names(value: &Vec<String>) -> bool {
    let def = default_value_dnssec_x_dnssec_names();
    &def == value
}

// DEFAULT HANDLING for dnssec_trustanchors
fn default_value_dnssec_trustanchors() -> Vec<recsettings::TrustAnchor> {
    let msg = "default value defined for `dnssec_trustanchors' should be valid YAML";    let deserialized: Vec<recsettings::TrustAnchor> = serde_yaml::from_str("[{name: ., dsrecords: ['20326 8 2 e06d44b80b8f1d39a95c0b0d7c65d08458e880409bbc683457104237c7f8ec8d', '38696 8 2 683d2d0acb8c9b712a1948b27f741219298d0a450d612c483af444a4c0fb2b16']}]").expect(msg);
    deserialized
}
fn default_value_equal_dnssec_trustanchors(value: &Vec<recsettings::TrustAnchor>)-> bool {
    let def = default_value_dnssec_trustanchors();
    &def == value
}

// DEFAULT HANDLING for dnssec_negative_trustanchors
fn default_value_dnssec_negative_trustanchors() -> Vec<recsettings::NegativeTrustAnchor> {
    let msg = "default value defined for `dnssec_negative_trustanchors' should be valid YAML";    let deserialized: Vec<recsettings::NegativeTrustAnchor> = serde_yaml::from_str("").expect(msg);
    deserialized
}
fn default_value_equal_dnssec_negative_trustanchors(value: &Vec<recsettings::NegativeTrustAnchor>)-> bool {
    let def = default_value_dnssec_negative_trustanchors();
    &def == value
}

// DEFAULT HANDLING for incoming_allow_from
fn default_value_incoming_allow_from() -> Vec<String> {
    vec![
        String::from("127.0.0.0/8"),
        String::from("10.0.0.0/8"),
        String::from("100.64.0.0/10"),
        String::from("169.254.0.0/16"),
        String::from("192.168.0.0/16"),
        String::from("172.16.0.0/12"),
        String::from("::1/128"),
        String::from("fc00::/7"),
        String::from("fe80::/10"),
    ]
}
fn default_value_equal_incoming_allow_from(value: &Vec<String>) -> bool {
    let def = default_value_incoming_allow_from();
    &def == value
}

// DEFAULT HANDLING for incoming_allow_notify_for
fn default_value_incoming_allow_notify_for() -> Vec<String> {
    vec![
    ]
}
fn default_value_equal_incoming_allow_notify_for(value: &Vec<String>) -> bool {
    let def = default_value_incoming_allow_notify_for();
    &def == value
}

// DEFAULT HANDLING for incoming_allow_notify_from
fn default_value_incoming_allow_notify_from() -> Vec<String> {
    vec![
    ]
}
fn default_value_equal_incoming_allow_notify_from(value: &Vec<String>) -> bool {
    let def = default_value_incoming_allow_notify_from();
    &def == value
}

// DEFAULT HANDLING for incoming_edns_padding_from
fn default_value_incoming_edns_padding_from() -> Vec<String> {
    vec![
    ]
}
fn default_value_equal_incoming_edns_padding_from(value: &Vec<String>) -> bool {
    let def = default_value_incoming_edns_padding_from();
    &def == value
}

// DEFAULT HANDLING for incoming_edns_padding_mode
fn default_value_incoming_edns_padding_mode() -> String {
    String::from("padded-queries-only")
}
fn default_value_equal_incoming_edns_padding_mode(value: &str)-> bool {
    value == default_value_incoming_edns_padding_mode()
}

// DEFAULT HANDLING for incoming_listen
fn default_value_incoming_listen() -> Vec<String> {
    vec![
        String::from("127.0.0.1"),
    ]
}
fn default_value_equal_incoming_listen(value: &Vec<String>) -> bool {
    let def = default_value_incoming_listen();
    &def == value
}

// DEFAULT HANDLING for incoming_proxy_protocol_from
fn default_value_incoming_proxy_protocol_from() -> Vec<String> {
    vec![
    ]
}
fn default_value_equal_incoming_proxy_protocol_from(value: &Vec<String>) -> bool {
    let def = default_value_incoming_proxy_protocol_from();
    &def == value
}

// DEFAULT HANDLING for incoming_proxy_protocol_exceptions
fn default_value_incoming_proxy_protocol_exceptions() -> Vec<String> {
    vec![
    ]
}
fn default_value_equal_incoming_proxy_protocol_exceptions(value: &Vec<String>) -> bool {
    let def = default_value_incoming_proxy_protocol_exceptions();
    &def == value
}

// DEFAULT HANDLING for incoming_proxymappings
fn default_value_incoming_proxymappings() -> Vec<recsettings::ProxyMapping> {
    let msg = "default value defined for `incoming_proxymappings' should be valid YAML";    let deserialized: Vec<recsettings::ProxyMapping> = serde_yaml::from_str("").expect(msg);
    deserialized
}
fn default_value_equal_incoming_proxymappings(value: &Vec<recsettings::ProxyMapping>)-> bool {
    let def = default_value_incoming_proxymappings();
    &def == value
}

// DEFAULT HANDLING for recursor_auth_zones
fn default_value_recursor_auth_zones() -> Vec<recsettings::AuthZone> {
    let msg = "default value defined for `recursor_auth_zones' should be valid YAML";    let deserialized: Vec<recsettings::AuthZone> = serde_yaml::from_str("").expect(msg);
    deserialized
}
fn default_value_equal_recursor_auth_zones(value: &Vec<recsettings::AuthZone>)-> bool {
    let def = default_value_recursor_auth_zones();
    &def == value
}

// DEFAULT HANDLING for recursor_config_dir
fn default_value_recursor_config_dir() -> String {
    String::from(env!("SYSCONFDIR"))
}
fn default_value_equal_recursor_config_dir(value: &str)-> bool {
    value == default_value_recursor_config_dir()
}

// DEFAULT HANDLING for recursor_etc_hosts_file
fn default_value_recursor_etc_hosts_file() -> String {
    String::from("/etc/hosts")
}
fn default_value_equal_recursor_etc_hosts_file(value: &str)-> bool {
    value == default_value_recursor_etc_hosts_file()
}

// DEFAULT HANDLING for recursor_forward_zones
fn default_value_recursor_forward_zones() -> Vec<recsettings::ForwardZone> {
    let msg = "default value defined for `recursor_forward_zones' should be valid YAML";    let deserialized: Vec<recsettings::ForwardZone> = serde_yaml::from_str("").expect(msg);
    deserialized
}
fn default_value_equal_recursor_forward_zones(value: &Vec<recsettings::ForwardZone>)-> bool {
    let def = default_value_recursor_forward_zones();
    &def == value
}

// DEFAULT HANDLING for recursor_forward_zones_recurse
fn default_value_recursor_forward_zones_recurse() -> Vec<recsettings::ForwardZone> {
    let msg = "default value defined for `recursor_forward_zones_recurse' should be valid YAML";    let deserialized: Vec<recsettings::ForwardZone> = serde_yaml::from_str("").expect(msg);
    deserialized
}
fn default_value_equal_recursor_forward_zones_recurse(value: &Vec<recsettings::ForwardZone>)-> bool {
    let def = default_value_recursor_forward_zones_recurse();
    &def == value
}

// DEFAULT HANDLING for recursor_ignore_unknown_settings
fn default_value_recursor_ignore_unknown_settings() -> Vec<String> {
    vec![
    ]
}
fn default_value_equal_recursor_ignore_unknown_settings(value: &Vec<String>) -> bool {
    let def = default_value_recursor_ignore_unknown_settings();
    &def == value
}

// DEFAULT HANDLING for recursor_nothing_below_nxdomain
fn default_value_recursor_nothing_below_nxdomain() -> String {
    String::from("dnssec")
}
fn default_value_equal_recursor_nothing_below_nxdomain(value: &str)-> bool {
    value == default_value_recursor_nothing_below_nxdomain()
}

// DEFAULT HANDLING for recursor_security_poll_suffix
fn default_value_recursor_security_poll_suffix() -> String {
    String::from("secpoll.powerdns.com.")
}
fn default_value_equal_recursor_security_poll_suffix(value: &str)-> bool {
    value == default_value_recursor_security_poll_suffix()
}

// DEFAULT HANDLING for recursor_server_id
fn default_value_recursor_server_id() -> String {
    String::from("*runtime determined*")
}
fn default_value_equal_recursor_server_id(value: &str)-> bool {
    value == default_value_recursor_server_id()
}

// DEFAULT HANDLING for recursor_stats_api_disabled_list
fn default_value_recursor_stats_api_disabled_list() -> Vec<String> {
    vec![
        String::from("cache-bytes"),
        String::from("packetcache-bytes"),
        String::from("special-memory-usage"),
        String::from("ecs-v4-response-bits-1"),
        String::from("ecs-v4-response-bits-2"),
        String::from("ecs-v4-response-bits-3"),
        String::from("ecs-v4-response-bits-4"),
        String::from("ecs-v4-response-bits-5"),
        String::from("ecs-v4-response-bits-6"),
        String::from("ecs-v4-response-bits-7"),
        String::from("ecs-v4-response-bits-8"),
        String::from("ecs-v4-response-bits-9"),
        String::from("ecs-v4-response-bits-10"),
        String::from("ecs-v4-response-bits-11"),
        String::from("ecs-v4-response-bits-12"),
        String::from("ecs-v4-response-bits-13"),
        String::from("ecs-v4-response-bits-14"),
        String::from("ecs-v4-response-bits-15"),
        String::from("ecs-v4-response-bits-16"),
        String::from("ecs-v4-response-bits-17"),
        String::from("ecs-v4-response-bits-18"),
        String::from("ecs-v4-response-bits-19"),
        String::from("ecs-v4-response-bits-20"),
        String::from("ecs-v4-response-bits-21"),
        String::from("ecs-v4-response-bits-22"),
        String::from("ecs-v4-response-bits-23"),
        String::from("ecs-v4-response-bits-24"),
        String::from("ecs-v4-response-bits-25"),
        String::from("ecs-v4-response-bits-26"),
        String::from("ecs-v4-response-bits-27"),
        String::from("ecs-v4-response-bits-28"),
        String::from("ecs-v4-response-bits-29"),
        String::from("ecs-v4-response-bits-30"),
        String::from("ecs-v4-response-bits-31"),
        String::from("ecs-v4-response-bits-32"),
        String::from("ecs-v6-response-bits-1"),
        String::from("ecs-v6-response-bits-2"),
        String::from("ecs-v6-response-bits-3"),
        String::from("ecs-v6-response-bits-4"),
        String::from("ecs-v6-response-bits-5"),
        String::from("ecs-v6-response-bits-6"),
        String::from("ecs-v6-response-bits-7"),
        String::from("ecs-v6-response-bits-8"),
        String::from("ecs-v6-response-bits-9"),
        String::from("ecs-v6-response-bits-10"),
        String::from("ecs-v6-response-bits-11"),
        String::from("ecs-v6-response-bits-12"),
        String::from("ecs-v6-response-bits-13"),
        String::from("ecs-v6-response-bits-14"),
        String::from("ecs-v6-response-bits-15"),
        String::from("ecs-v6-response-bits-16"),
        String::from("ecs-v6-response-bits-17"),
        String::from("ecs-v6-response-bits-18"),
        String::from("ecs-v6-response-bits-19"),
        String::from("ecs-v6-response-bits-20"),
        String::from("ecs-v6-response-bits-21"),
        String::from("ecs-v6-response-bits-22"),
        String::from("ecs-v6-response-bits-23"),
        String::from("ecs-v6-response-bits-24"),
        String::from("ecs-v6-response-bits-25"),
        String::from("ecs-v6-response-bits-26"),
        String::from("ecs-v6-response-bits-27"),
        String::from("ecs-v6-response-bits-28"),
        String::from("ecs-v6-response-bits-29"),
        String::from("ecs-v6-response-bits-30"),
        String::from("ecs-v6-response-bits-31"),
        String::from("ecs-v6-response-bits-32"),
        String::from("ecs-v6-response-bits-33"),
        String::from("ecs-v6-response-bits-34"),
        String::from("ecs-v6-response-bits-35"),
        String::from("ecs-v6-response-bits-36"),
        String::from("ecs-v6-response-bits-37"),
        String::from("ecs-v6-response-bits-38"),
        String::from("ecs-v6-response-bits-39"),
        String::from("ecs-v6-response-bits-40"),
        String::from("ecs-v6-response-bits-41"),
        String::from("ecs-v6-response-bits-42"),
        String::from("ecs-v6-response-bits-43"),
        String::from("ecs-v6-response-bits-44"),
        String::from("ecs-v6-response-bits-45"),
        String::from("ecs-v6-response-bits-46"),
        String::from("ecs-v6-response-bits-47"),
        String::from("ecs-v6-response-bits-48"),
        String::from("ecs-v6-response-bits-49"),
        String::from("ecs-v6-response-bits-50"),
        String::from("ecs-v6-response-bits-51"),
        String::from("ecs-v6-response-bits-52"),
        String::from("ecs-v6-response-bits-53"),
        String::from("ecs-v6-response-bits-54"),
        String::from("ecs-v6-response-bits-55"),
        String::from("ecs-v6-response-bits-56"),
        String::from("ecs-v6-response-bits-57"),
        String::from("ecs-v6-response-bits-58"),
        String::from("ecs-v6-response-bits-59"),
        String::from("ecs-v6-response-bits-60"),
        String::from("ecs-v6-response-bits-61"),
        String::from("ecs-v6-response-bits-62"),
        String::from("ecs-v6-response-bits-63"),
        String::from("ecs-v6-response-bits-64"),
        String::from("ecs-v6-response-bits-65"),
        String::from("ecs-v6-response-bits-66"),
        String::from("ecs-v6-response-bits-67"),
        String::from("ecs-v6-response-bits-68"),
        String::from("ecs-v6-response-bits-69"),
        String::from("ecs-v6-response-bits-70"),
        String::from("ecs-v6-response-bits-71"),
        String::from("ecs-v6-response-bits-72"),
        String::from("ecs-v6-response-bits-73"),
        String::from("ecs-v6-response-bits-74"),
        String::from("ecs-v6-response-bits-75"),
        String::from("ecs-v6-response-bits-76"),
        String::from("ecs-v6-response-bits-77"),
        String::from("ecs-v6-response-bits-78"),
        String::from("ecs-v6-response-bits-79"),
        String::from("ecs-v6-response-bits-80"),
        String::from("ecs-v6-response-bits-81"),
        String::from("ecs-v6-response-bits-82"),
        String::from("ecs-v6-response-bits-83"),
        String::from("ecs-v6-response-bits-84"),
        String::from("ecs-v6-response-bits-85"),
        String::from("ecs-v6-response-bits-86"),
        String::from("ecs-v6-response-bits-87"),
        String::from("ecs-v6-response-bits-88"),
        String::from("ecs-v6-response-bits-89"),
        String::from("ecs-v6-response-bits-90"),
        String::from("ecs-v6-response-bits-91"),
        String::from("ecs-v6-response-bits-92"),
        String::from("ecs-v6-response-bits-93"),
        String::from("ecs-v6-response-bits-94"),
        String::from("ecs-v6-response-bits-95"),
        String::from("ecs-v6-response-bits-96"),
        String::from("ecs-v6-response-bits-97"),
        String::from("ecs-v6-response-bits-98"),
        String::from("ecs-v6-response-bits-99"),
        String::from("ecs-v6-response-bits-100"),
        String::from("ecs-v6-response-bits-101"),
        String::from("ecs-v6-response-bits-102"),
        String::from("ecs-v6-response-bits-103"),
        String::from("ecs-v6-response-bits-104"),
        String::from("ecs-v6-response-bits-105"),
        String::from("ecs-v6-response-bits-106"),
        String::from("ecs-v6-response-bits-107"),
        String::from("ecs-v6-response-bits-108"),
        String::from("ecs-v6-response-bits-109"),
        String::from("ecs-v6-response-bits-110"),
        String::from("ecs-v6-response-bits-111"),
        String::from("ecs-v6-response-bits-112"),
        String::from("ecs-v6-response-bits-113"),
        String::from("ecs-v6-response-bits-114"),
        String::from("ecs-v6-response-bits-115"),
        String::from("ecs-v6-response-bits-116"),
        String::from("ecs-v6-response-bits-117"),
        String::from("ecs-v6-response-bits-118"),
        String::from("ecs-v6-response-bits-119"),
        String::from("ecs-v6-response-bits-120"),
        String::from("ecs-v6-response-bits-121"),
        String::from("ecs-v6-response-bits-122"),
        String::from("ecs-v6-response-bits-123"),
        String::from("ecs-v6-response-bits-124"),
        String::from("ecs-v6-response-bits-125"),
        String::from("ecs-v6-response-bits-126"),
        String::from("ecs-v6-response-bits-127"),
        String::from("ecs-v6-response-bits-128"),
    ]
}
fn default_value_equal_recursor_stats_api_disabled_list(value: &Vec<String>) -> bool {
    let def = default_value_recursor_stats_api_disabled_list();
    &def == value
}

// DEFAULT HANDLING for recursor_stats_carbon_disabled_list
fn default_value_recursor_stats_carbon_disabled_list() -> Vec<String> {
    vec![
        String::from("cache-bytes"),
        String::from("packetcache-bytes"),
        String::from("special-memory-usage"),
        String::from("ecs-v4-response-bits-1"),
        String::from("ecs-v4-response-bits-2"),
        String::from("ecs-v4-response-bits-3"),
        String::from("ecs-v4-response-bits-4"),
        String::from("ecs-v4-response-bits-5"),
        String::from("ecs-v4-response-bits-6"),
        String::from("ecs-v4-response-bits-7"),
        String::from("ecs-v4-response-bits-8"),
        String::from("ecs-v4-response-bits-9"),
        String::from("ecs-v4-response-bits-10"),
        String::from("ecs-v4-response-bits-11"),
        String::from("ecs-v4-response-bits-12"),
        String::from("ecs-v4-response-bits-13"),
        String::from("ecs-v4-response-bits-14"),
        String::from("ecs-v4-response-bits-15"),
        String::from("ecs-v4-response-bits-16"),
        String::from("ecs-v4-response-bits-17"),
        String::from("ecs-v4-response-bits-18"),
        String::from("ecs-v4-response-bits-19"),
        String::from("ecs-v4-response-bits-20"),
        String::from("ecs-v4-response-bits-21"),
        String::from("ecs-v4-response-bits-22"),
        String::from("ecs-v4-response-bits-23"),
        String::from("ecs-v4-response-bits-24"),
        String::from("ecs-v4-response-bits-25"),
        String::from("ecs-v4-response-bits-26"),
        String::from("ecs-v4-response-bits-27"),
        String::from("ecs-v4-response-bits-28"),
        String::from("ecs-v4-response-bits-29"),
        String::from("ecs-v4-response-bits-30"),
        String::from("ecs-v4-response-bits-31"),
        String::from("ecs-v4-response-bits-32"),
        String::from("ecs-v6-response-bits-1"),
        String::from("ecs-v6-response-bits-2"),
        String::from("ecs-v6-response-bits-3"),
        String::from("ecs-v6-response-bits-4"),
        String::from("ecs-v6-response-bits-5"),
        String::from("ecs-v6-response-bits-6"),
        String::from("ecs-v6-response-bits-7"),
        String::from("ecs-v6-response-bits-8"),
        String::from("ecs-v6-response-bits-9"),
        String::from("ecs-v6-response-bits-10"),
        String::from("ecs-v6-response-bits-11"),
        String::from("ecs-v6-response-bits-12"),
        String::from("ecs-v6-response-bits-13"),
        String::from("ecs-v6-response-bits-14"),
        String::from("ecs-v6-response-bits-15"),
        String::from("ecs-v6-response-bits-16"),
        String::from("ecs-v6-response-bits-17"),
        String::from("ecs-v6-response-bits-18"),
        String::from("ecs-v6-response-bits-19"),
        String::from("ecs-v6-response-bits-20"),
        String::from("ecs-v6-response-bits-21"),
        String::from("ecs-v6-response-bits-22"),
        String::from("ecs-v6-response-bits-23"),
        String::from("ecs-v6-response-bits-24"),
        String::from("ecs-v6-response-bits-25"),
        String::from("ecs-v6-response-bits-26"),
        String::from("ecs-v6-response-bits-27"),
        String::from("ecs-v6-response-bits-28"),
        String::from("ecs-v6-response-bits-29"),
        String::from("ecs-v6-response-bits-30"),
        String::from("ecs-v6-response-bits-31"),
        String::from("ecs-v6-response-bits-32"),
        String::from("ecs-v6-response-bits-33"),
        String::from("ecs-v6-response-bits-34"),
        String::from("ecs-v6-response-bits-35"),
        String::from("ecs-v6-response-bits-36"),
        String::from("ecs-v6-response-bits-37"),
        String::from("ecs-v6-response-bits-38"),
        String::from("ecs-v6-response-bits-39"),
        String::from("ecs-v6-response-bits-40"),
        String::from("ecs-v6-response-bits-41"),
        String::from("ecs-v6-response-bits-42"),
        String::from("ecs-v6-response-bits-43"),
        String::from("ecs-v6-response-bits-44"),
        String::from("ecs-v6-response-bits-45"),
        String::from("ecs-v6-response-bits-46"),
        String::from("ecs-v6-response-bits-47"),
        String::from("ecs-v6-response-bits-48"),
        String::from("ecs-v6-response-bits-49"),
        String::from("ecs-v6-response-bits-50"),
        String::from("ecs-v6-response-bits-51"),
        String::from("ecs-v6-response-bits-52"),
        String::from("ecs-v6-response-bits-53"),
        String::from("ecs-v6-response-bits-54"),
        String::from("ecs-v6-response-bits-55"),
        String::from("ecs-v6-response-bits-56"),
        String::from("ecs-v6-response-bits-57"),
        String::from("ecs-v6-response-bits-58"),
        String::from("ecs-v6-response-bits-59"),
        String::from("ecs-v6-response-bits-60"),
        String::from("ecs-v6-response-bits-61"),
        String::from("ecs-v6-response-bits-62"),
        String::from("ecs-v6-response-bits-63"),
        String::from("ecs-v6-response-bits-64"),
        String::from("ecs-v6-response-bits-65"),
        String::from("ecs-v6-response-bits-66"),
        String::from("ecs-v6-response-bits-67"),
        String::from("ecs-v6-response-bits-68"),
        String::from("ecs-v6-response-bits-69"),
        String::from("ecs-v6-response-bits-70"),
        String::from("ecs-v6-response-bits-71"),
        String::from("ecs-v6-response-bits-72"),
        String::from("ecs-v6-response-bits-73"),
        String::from("ecs-v6-response-bits-74"),
        String::from("ecs-v6-response-bits-75"),
        String::from("ecs-v6-response-bits-76"),
        String::from("ecs-v6-response-bits-77"),
        String::from("ecs-v6-response-bits-78"),
        String::from("ecs-v6-response-bits-79"),
        String::from("ecs-v6-response-bits-80"),
        String::from("ecs-v6-response-bits-81"),
        String::from("ecs-v6-response-bits-82"),
        String::from("ecs-v6-response-bits-83"),
        String::from("ecs-v6-response-bits-84"),
        String::from("ecs-v6-response-bits-85"),
        String::from("ecs-v6-response-bits-86"),
        String::from("ecs-v6-response-bits-87"),
        String::from("ecs-v6-response-bits-88"),
        String::from("ecs-v6-response-bits-89"),
        String::from("ecs-v6-response-bits-90"),
        String::from("ecs-v6-response-bits-91"),
        String::from("ecs-v6-response-bits-92"),
        String::from("ecs-v6-response-bits-93"),
        String::from("ecs-v6-response-bits-94"),
        String::from("ecs-v6-response-bits-95"),
        String::from("ecs-v6-response-bits-96"),
        String::from("ecs-v6-response-bits-97"),
        String::from("ecs-v6-response-bits-98"),
        String::from("ecs-v6-response-bits-99"),
        String::from("ecs-v6-response-bits-100"),
        String::from("ecs-v6-response-bits-101"),
        String::from("ecs-v6-response-bits-102"),
        String::from("ecs-v6-response-bits-103"),
        String::from("ecs-v6-response-bits-104"),
        String::from("ecs-v6-response-bits-105"),
        String::from("ecs-v6-response-bits-106"),
        String::from("ecs-v6-response-bits-107"),
        String::from("ecs-v6-response-bits-108"),
        String::from("ecs-v6-response-bits-109"),
        String::from("ecs-v6-response-bits-110"),
        String::from("ecs-v6-response-bits-111"),
        String::from("ecs-v6-response-bits-112"),
        String::from("ecs-v6-response-bits-113"),
        String::from("ecs-v6-response-bits-114"),
        String::from("ecs-v6-response-bits-115"),
        String::from("ecs-v6-response-bits-116"),
        String::from("ecs-v6-response-bits-117"),
        String::from("ecs-v6-response-bits-118"),
        String::from("ecs-v6-response-bits-119"),
        String::from("ecs-v6-response-bits-120"),
        String::from("ecs-v6-response-bits-121"),
        String::from("ecs-v6-response-bits-122"),
        String::from("ecs-v6-response-bits-123"),
        String::from("ecs-v6-response-bits-124"),
        String::from("ecs-v6-response-bits-125"),
        String::from("ecs-v6-response-bits-126"),
        String::from("ecs-v6-response-bits-127"),
        String::from("ecs-v6-response-bits-128"),
        String::from("cumul-clientanswers"),
        String::from("cumul-authanswers"),
        String::from("policy-hits"),
        String::from("proxy-mapping-total"),
        String::from("remote-logger-count"),
    ]
}
fn default_value_equal_recursor_stats_carbon_disabled_list(value: &Vec<String>) -> bool {
    let def = default_value_recursor_stats_carbon_disabled_list();
    &def == value
}

// DEFAULT HANDLING for recursor_stats_rec_control_disabled_list
fn default_value_recursor_stats_rec_control_disabled_list() -> Vec<String> {
    vec![
        String::from("cache-bytes"),
        String::from("packetcache-bytes"),
        String::from("special-memory-usage"),
        String::from("ecs-v4-response-bits-1"),
        String::from("ecs-v4-response-bits-2"),
        String::from("ecs-v4-response-bits-3"),
        String::from("ecs-v4-response-bits-4"),
        String::from("ecs-v4-response-bits-5"),
        String::from("ecs-v4-response-bits-6"),
        String::from("ecs-v4-response-bits-7"),
        String::from("ecs-v4-response-bits-8"),
        String::from("ecs-v4-response-bits-9"),
        String::from("ecs-v4-response-bits-10"),
        String::from("ecs-v4-response-bits-11"),
        String::from("ecs-v4-response-bits-12"),
        String::from("ecs-v4-response-bits-13"),
        String::from("ecs-v4-response-bits-14"),
        String::from("ecs-v4-response-bits-15"),
        String::from("ecs-v4-response-bits-16"),
        String::from("ecs-v4-response-bits-17"),
        String::from("ecs-v4-response-bits-18"),
        String::from("ecs-v4-response-bits-19"),
        String::from("ecs-v4-response-bits-20"),
        String::from("ecs-v4-response-bits-21"),
        String::from("ecs-v4-response-bits-22"),
        String::from("ecs-v4-response-bits-23"),
        String::from("ecs-v4-response-bits-24"),
        String::from("ecs-v4-response-bits-25"),
        String::from("ecs-v4-response-bits-26"),
        String::from("ecs-v4-response-bits-27"),
        String::from("ecs-v4-response-bits-28"),
        String::from("ecs-v4-response-bits-29"),
        String::from("ecs-v4-response-bits-30"),
        String::from("ecs-v4-response-bits-31"),
        String::from("ecs-v4-response-bits-32"),
        String::from("ecs-v6-response-bits-1"),
        String::from("ecs-v6-response-bits-2"),
        String::from("ecs-v6-response-bits-3"),
        String::from("ecs-v6-response-bits-4"),
        String::from("ecs-v6-response-bits-5"),
        String::from("ecs-v6-response-bits-6"),
        String::from("ecs-v6-response-bits-7"),
        String::from("ecs-v6-response-bits-8"),
        String::from("ecs-v6-response-bits-9"),
        String::from("ecs-v6-response-bits-10"),
        String::from("ecs-v6-response-bits-11"),
        String::from("ecs-v6-response-bits-12"),
        String::from("ecs-v6-response-bits-13"),
        String::from("ecs-v6-response-bits-14"),
        String::from("ecs-v6-response-bits-15"),
        String::from("ecs-v6-response-bits-16"),
        String::from("ecs-v6-response-bits-17"),
        String::from("ecs-v6-response-bits-18"),
        String::from("ecs-v6-response-bits-19"),
        String::from("ecs-v6-response-bits-20"),
        String::from("ecs-v6-response-bits-21"),
        String::from("ecs-v6-response-bits-22"),
        String::from("ecs-v6-response-bits-23"),
        String::from("ecs-v6-response-bits-24"),
        String::from("ecs-v6-response-bits-25"),
        String::from("ecs-v6-response-bits-26"),
        String::from("ecs-v6-response-bits-27"),
        String::from("ecs-v6-response-bits-28"),
        String::from("ecs-v6-response-bits-29"),
        String::from("ecs-v6-response-bits-30"),
        String::from("ecs-v6-response-bits-31"),
        String::from("ecs-v6-response-bits-32"),
        String::from("ecs-v6-response-bits-33"),
        String::from("ecs-v6-response-bits-34"),
        String::from("ecs-v6-response-bits-35"),
        String::from("ecs-v6-response-bits-36"),
        String::from("ecs-v6-response-bits-37"),
        String::from("ecs-v6-response-bits-38"),
        String::from("ecs-v6-response-bits-39"),
        String::from("ecs-v6-response-bits-40"),
        String::from("ecs-v6-response-bits-41"),
        String::from("ecs-v6-response-bits-42"),
        String::from("ecs-v6-response-bits-43"),
        String::from("ecs-v6-response-bits-44"),
        String::from("ecs-v6-response-bits-45"),
        String::from("ecs-v6-response-bits-46"),
        String::from("ecs-v6-response-bits-47"),
        String::from("ecs-v6-response-bits-48"),
        String::from("ecs-v6-response-bits-49"),
        String::from("ecs-v6-response-bits-50"),
        String::from("ecs-v6-response-bits-51"),
        String::from("ecs-v6-response-bits-52"),
        String::from("ecs-v6-response-bits-53"),
        String::from("ecs-v6-response-bits-54"),
        String::from("ecs-v6-response-bits-55"),
        String::from("ecs-v6-response-bits-56"),
        String::from("ecs-v6-response-bits-57"),
        String::from("ecs-v6-response-bits-58"),
        String::from("ecs-v6-response-bits-59"),
        String::from("ecs-v6-response-bits-60"),
        String::from("ecs-v6-response-bits-61"),
        String::from("ecs-v6-response-bits-62"),
        String::from("ecs-v6-response-bits-63"),
        String::from("ecs-v6-response-bits-64"),
        String::from("ecs-v6-response-bits-65"),
        String::from("ecs-v6-response-bits-66"),
        String::from("ecs-v6-response-bits-67"),
        String::from("ecs-v6-response-bits-68"),
        String::from("ecs-v6-response-bits-69"),
        String::from("ecs-v6-response-bits-70"),
        String::from("ecs-v6-response-bits-71"),
        String::from("ecs-v6-response-bits-72"),
        String::from("ecs-v6-response-bits-73"),
        String::from("ecs-v6-response-bits-74"),
        String::from("ecs-v6-response-bits-75"),
        String::from("ecs-v6-response-bits-76"),
        String::from("ecs-v6-response-bits-77"),
        String::from("ecs-v6-response-bits-78"),
        String::from("ecs-v6-response-bits-79"),
        String::from("ecs-v6-response-bits-80"),
        String::from("ecs-v6-response-bits-81"),
        String::from("ecs-v6-response-bits-82"),
        String::from("ecs-v6-response-bits-83"),
        String::from("ecs-v6-response-bits-84"),
        String::from("ecs-v6-response-bits-85"),
        String::from("ecs-v6-response-bits-86"),
        String::from("ecs-v6-response-bits-87"),
        String::from("ecs-v6-response-bits-88"),
        String::from("ecs-v6-response-bits-89"),
        String::from("ecs-v6-response-bits-90"),
        String::from("ecs-v6-response-bits-91"),
        String::from("ecs-v6-response-bits-92"),
        String::from("ecs-v6-response-bits-93"),
        String::from("ecs-v6-response-bits-94"),
        String::from("ecs-v6-response-bits-95"),
        String::from("ecs-v6-response-bits-96"),
        String::from("ecs-v6-response-bits-97"),
        String::from("ecs-v6-response-bits-98"),
        String::from("ecs-v6-response-bits-99"),
        String::from("ecs-v6-response-bits-100"),
        String::from("ecs-v6-response-bits-101"),
        String::from("ecs-v6-response-bits-102"),
        String::from("ecs-v6-response-bits-103"),
        String::from("ecs-v6-response-bits-104"),
        String::from("ecs-v6-response-bits-105"),
        String::from("ecs-v6-response-bits-106"),
        String::from("ecs-v6-response-bits-107"),
        String::from("ecs-v6-response-bits-108"),
        String::from("ecs-v6-response-bits-109"),
        String::from("ecs-v6-response-bits-110"),
        String::from("ecs-v6-response-bits-111"),
        String::from("ecs-v6-response-bits-112"),
        String::from("ecs-v6-response-bits-113"),
        String::from("ecs-v6-response-bits-114"),
        String::from("ecs-v6-response-bits-115"),
        String::from("ecs-v6-response-bits-116"),
        String::from("ecs-v6-response-bits-117"),
        String::from("ecs-v6-response-bits-118"),
        String::from("ecs-v6-response-bits-119"),
        String::from("ecs-v6-response-bits-120"),
        String::from("ecs-v6-response-bits-121"),
        String::from("ecs-v6-response-bits-122"),
        String::from("ecs-v6-response-bits-123"),
        String::from("ecs-v6-response-bits-124"),
        String::from("ecs-v6-response-bits-125"),
        String::from("ecs-v6-response-bits-126"),
        String::from("ecs-v6-response-bits-127"),
        String::from("ecs-v6-response-bits-128"),
        String::from("cumul-clientanswers"),
        String::from("cumul-authanswers"),
        String::from("policy-hits"),
        String::from("proxy-mapping-total"),
        String::from("remote-logger-count"),
    ]
}
fn default_value_equal_recursor_stats_rec_control_disabled_list(value: &Vec<String>) -> bool {
    let def = default_value_recursor_stats_rec_control_disabled_list();
    &def == value
}

// DEFAULT HANDLING for recursor_stats_snmp_disabled_list
fn default_value_recursor_stats_snmp_disabled_list() -> Vec<String> {
    vec![
        String::from("cache-bytes"),
        String::from("packetcache-bytes"),
        String::from("special-memory-usage"),
        String::from("ecs-v4-response-bits-1"),
        String::from("ecs-v4-response-bits-2"),
        String::from("ecs-v4-response-bits-3"),
        String::from("ecs-v4-response-bits-4"),
        String::from("ecs-v4-response-bits-5"),
        String::from("ecs-v4-response-bits-6"),
        String::from("ecs-v4-response-bits-7"),
        String::from("ecs-v4-response-bits-8"),
        String::from("ecs-v4-response-bits-9"),
        String::from("ecs-v4-response-bits-10"),
        String::from("ecs-v4-response-bits-11"),
        String::from("ecs-v4-response-bits-12"),
        String::from("ecs-v4-response-bits-13"),
        String::from("ecs-v4-response-bits-14"),
        String::from("ecs-v4-response-bits-15"),
        String::from("ecs-v4-response-bits-16"),
        String::from("ecs-v4-response-bits-17"),
        String::from("ecs-v4-response-bits-18"),
        String::from("ecs-v4-response-bits-19"),
        String::from("ecs-v4-response-bits-20"),
        String::from("ecs-v4-response-bits-21"),
        String::from("ecs-v4-response-bits-22"),
        String::from("ecs-v4-response-bits-23"),
        String::from("ecs-v4-response-bits-24"),
        String::from("ecs-v4-response-bits-25"),
        String::from("ecs-v4-response-bits-26"),
        String::from("ecs-v4-response-bits-27"),
        String::from("ecs-v4-response-bits-28"),
        String::from("ecs-v4-response-bits-29"),
        String::from("ecs-v4-response-bits-30"),
        String::from("ecs-v4-response-bits-31"),
        String::from("ecs-v4-response-bits-32"),
        String::from("ecs-v6-response-bits-1"),
        String::from("ecs-v6-response-bits-2"),
        String::from("ecs-v6-response-bits-3"),
        String::from("ecs-v6-response-bits-4"),
        String::from("ecs-v6-response-bits-5"),
        String::from("ecs-v6-response-bits-6"),
        String::from("ecs-v6-response-bits-7"),
        String::from("ecs-v6-response-bits-8"),
        String::from("ecs-v6-response-bits-9"),
        String::from("ecs-v6-response-bits-10"),
        String::from("ecs-v6-response-bits-11"),
        String::from("ecs-v6-response-bits-12"),
        String::from("ecs-v6-response-bits-13"),
        String::from("ecs-v6-response-bits-14"),
        String::from("ecs-v6-response-bits-15"),
        String::from("ecs-v6-response-bits-16"),
        String::from("ecs-v6-response-bits-17"),
        String::from("ecs-v6-response-bits-18"),
        String::from("ecs-v6-response-bits-19"),
        String::from("ecs-v6-response-bits-20"),
        String::from("ecs-v6-response-bits-21"),
        String::from("ecs-v6-response-bits-22"),
        String::from("ecs-v6-response-bits-23"),
        String::from("ecs-v6-response-bits-24"),
        String::from("ecs-v6-response-bits-25"),
        String::from("ecs-v6-response-bits-26"),
        String::from("ecs-v6-response-bits-27"),
        String::from("ecs-v6-response-bits-28"),
        String::from("ecs-v6-response-bits-29"),
        String::from("ecs-v6-response-bits-30"),
        String::from("ecs-v6-response-bits-31"),
        String::from("ecs-v6-response-bits-32"),
        String::from("ecs-v6-response-bits-33"),
        String::from("ecs-v6-response-bits-34"),
        String::from("ecs-v6-response-bits-35"),
        String::from("ecs-v6-response-bits-36"),
        String::from("ecs-v6-response-bits-37"),
        String::from("ecs-v6-response-bits-38"),
        String::from("ecs-v6-response-bits-39"),
        String::from("ecs-v6-response-bits-40"),
        String::from("ecs-v6-response-bits-41"),
        String::from("ecs-v6-response-bits-42"),
        String::from("ecs-v6-response-bits-43"),
        String::from("ecs-v6-response-bits-44"),
        String::from("ecs-v6-response-bits-45"),
        String::from("ecs-v6-response-bits-46"),
        String::from("ecs-v6-response-bits-47"),
        String::from("ecs-v6-response-bits-48"),
        String::from("ecs-v6-response-bits-49"),
        String::from("ecs-v6-response-bits-50"),
        String::from("ecs-v6-response-bits-51"),
        String::from("ecs-v6-response-bits-52"),
        String::from("ecs-v6-response-bits-53"),
        String::from("ecs-v6-response-bits-54"),
        String::from("ecs-v6-response-bits-55"),
        String::from("ecs-v6-response-bits-56"),
        String::from("ecs-v6-response-bits-57"),
        String::from("ecs-v6-response-bits-58"),
        String::from("ecs-v6-response-bits-59"),
        String::from("ecs-v6-response-bits-60"),
        String::from("ecs-v6-response-bits-61"),
        String::from("ecs-v6-response-bits-62"),
        String::from("ecs-v6-response-bits-63"),
        String::from("ecs-v6-response-bits-64"),
        String::from("ecs-v6-response-bits-65"),
        String::from("ecs-v6-response-bits-66"),
        String::from("ecs-v6-response-bits-67"),
        String::from("ecs-v6-response-bits-68"),
        String::from("ecs-v6-response-bits-69"),
        String::from("ecs-v6-response-bits-70"),
        String::from("ecs-v6-response-bits-71"),
        String::from("ecs-v6-response-bits-72"),
        String::from("ecs-v6-response-bits-73"),
        String::from("ecs-v6-response-bits-74"),
        String::from("ecs-v6-response-bits-75"),
        String::from("ecs-v6-response-bits-76"),
        String::from("ecs-v6-response-bits-77"),
        String::from("ecs-v6-response-bits-78"),
        String::from("ecs-v6-response-bits-79"),
        String::from("ecs-v6-response-bits-80"),
        String::from("ecs-v6-response-bits-81"),
        String::from("ecs-v6-response-bits-82"),
        String::from("ecs-v6-response-bits-83"),
        String::from("ecs-v6-response-bits-84"),
        String::from("ecs-v6-response-bits-85"),
        String::from("ecs-v6-response-bits-86"),
        String::from("ecs-v6-response-bits-87"),
        String::from("ecs-v6-response-bits-88"),
        String::from("ecs-v6-response-bits-89"),
        String::from("ecs-v6-response-bits-90"),
        String::from("ecs-v6-response-bits-91"),
        String::from("ecs-v6-response-bits-92"),
        String::from("ecs-v6-response-bits-93"),
        String::from("ecs-v6-response-bits-94"),
        String::from("ecs-v6-response-bits-95"),
        String::from("ecs-v6-response-bits-96"),
        String::from("ecs-v6-response-bits-97"),
        String::from("ecs-v6-response-bits-98"),
        String::from("ecs-v6-response-bits-99"),
        String::from("ecs-v6-response-bits-100"),
        String::from("ecs-v6-response-bits-101"),
        String::from("ecs-v6-response-bits-102"),
        String::from("ecs-v6-response-bits-103"),
        String::from("ecs-v6-response-bits-104"),
        String::from("ecs-v6-response-bits-105"),
        String::from("ecs-v6-response-bits-106"),
        String::from("ecs-v6-response-bits-107"),
        String::from("ecs-v6-response-bits-108"),
        String::from("ecs-v6-response-bits-109"),
        String::from("ecs-v6-response-bits-110"),
        String::from("ecs-v6-response-bits-111"),
        String::from("ecs-v6-response-bits-112"),
        String::from("ecs-v6-response-bits-113"),
        String::from("ecs-v6-response-bits-114"),
        String::from("ecs-v6-response-bits-115"),
        String::from("ecs-v6-response-bits-116"),
        String::from("ecs-v6-response-bits-117"),
        String::from("ecs-v6-response-bits-118"),
        String::from("ecs-v6-response-bits-119"),
        String::from("ecs-v6-response-bits-120"),
        String::from("ecs-v6-response-bits-121"),
        String::from("ecs-v6-response-bits-122"),
        String::from("ecs-v6-response-bits-123"),
        String::from("ecs-v6-response-bits-124"),
        String::from("ecs-v6-response-bits-125"),
        String::from("ecs-v6-response-bits-126"),
        String::from("ecs-v6-response-bits-127"),
        String::from("ecs-v6-response-bits-128"),
        String::from("cumul-clientanswers"),
        String::from("cumul-authanswers"),
        String::from("policy-hits"),
        String::from("proxy-mapping-total"),
        String::from("remote-logger-count"),
    ]
}
fn default_value_equal_recursor_stats_snmp_disabled_list(value: &Vec<String>) -> bool {
    let def = default_value_recursor_stats_snmp_disabled_list();
    &def == value
}

// DEFAULT HANDLING for recursor_version_string
fn default_value_recursor_version_string() -> String {
    String::from("*runtime determined*")
}
fn default_value_equal_recursor_version_string(value: &str)-> bool {
    value == default_value_recursor_version_string()
}

// DEFAULT HANDLING for recursor_sortlists
fn default_value_recursor_sortlists() -> Vec<recsettings::SortList> {
    let msg = "default value defined for `recursor_sortlists' should be valid YAML";    let deserialized: Vec<recsettings::SortList> = serde_yaml::from_str("").expect(msg);
    deserialized
}
fn default_value_equal_recursor_sortlists(value: &Vec<recsettings::SortList>)-> bool {
    let def = default_value_recursor_sortlists();
    &def == value
}

// DEFAULT HANDLING for recursor_rpzs
fn default_value_recursor_rpzs() -> Vec<recsettings::RPZ> {
    let msg = "default value defined for `recursor_rpzs' should be valid YAML";    let deserialized: Vec<recsettings::RPZ> = serde_yaml::from_str("").expect(msg);
    deserialized
}
fn default_value_equal_recursor_rpzs(value: &Vec<recsettings::RPZ>)-> bool {
    let def = default_value_recursor_rpzs();
    &def == value
}

// DEFAULT HANDLING for recursor_allowed_additional_qtypes
fn default_value_recursor_allowed_additional_qtypes() -> Vec<recsettings::AllowedAdditionalQType> {
    let msg = "default value defined for `recursor_allowed_additional_qtypes' should be valid YAML";    let deserialized: Vec<recsettings::AllowedAdditionalQType> = serde_yaml::from_str("").expect(msg);
    deserialized
}
fn default_value_equal_recursor_allowed_additional_qtypes(value: &Vec<recsettings::AllowedAdditionalQType>)-> bool {
    let def = default_value_recursor_allowed_additional_qtypes();
    &def == value
}

// DEFAULT HANDLING for recursor_forwarding_catalog_zones
fn default_value_recursor_forwarding_catalog_zones() -> Vec<recsettings::ForwardingCatalogZone> {
    let msg = "default value defined for `recursor_forwarding_catalog_zones' should be valid YAML";    let deserialized: Vec<recsettings::ForwardingCatalogZone> = serde_yaml::from_str("").expect(msg);
    deserialized
}
fn default_value_equal_recursor_forwarding_catalog_zones(value: &Vec<recsettings::ForwardingCatalogZone>)-> bool {
    let def = default_value_recursor_forwarding_catalog_zones();
    &def == value
}

// DEFAULT HANDLING for webservice_address
fn default_value_webservice_address() -> String {
    String::from("127.0.0.1")
}
fn default_value_equal_webservice_address(value: &str)-> bool {
    value == default_value_webservice_address()
}

// DEFAULT HANDLING for webservice_allow_from
fn default_value_webservice_allow_from() -> Vec<String> {
    vec![
        String::from("127.0.0.1"),
        String::from("::1"),
    ]
}
fn default_value_equal_webservice_allow_from(value: &Vec<String>) -> bool {
    let def = default_value_webservice_allow_from();
    &def == value
}

// DEFAULT HANDLING for webservice_loglevel
fn default_value_webservice_loglevel() -> String {
    String::from("normal")
}
fn default_value_equal_webservice_loglevel(value: &str)-> bool {
    value == default_value_webservice_loglevel()
}

// DEFAULT HANDLING for carbon_ns
fn default_value_carbon_ns() -> String {
    String::from("pdns")
}
fn default_value_equal_carbon_ns(value: &str)-> bool {
    value == default_value_carbon_ns()
}

// DEFAULT HANDLING for carbon_instance
fn default_value_carbon_instance() -> String {
    String::from("recursor")
}
fn default_value_equal_carbon_instance(value: &str)-> bool {
    value == default_value_carbon_instance()
}

// DEFAULT HANDLING for carbon_server
fn default_value_carbon_server() -> Vec<String> {
    vec![
    ]
}
fn default_value_equal_carbon_server(value: &Vec<String>) -> bool {
    let def = default_value_carbon_server();
    &def == value
}

// DEFAULT HANDLING for outgoing_dont_throttle_names
fn default_value_outgoing_dont_throttle_names() -> Vec<String> {
    vec![
    ]
}
fn default_value_equal_outgoing_dont_throttle_names(value: &Vec<String>) -> bool {
    let def = default_value_outgoing_dont_throttle_names();
    &def == value
}

// DEFAULT HANDLING for outgoing_dont_throttle_netmasks
fn default_value_outgoing_dont_throttle_netmasks() -> Vec<String> {
    vec![
    ]
}
fn default_value_equal_outgoing_dont_throttle_netmasks(value: &Vec<String>) -> bool {
    let def = default_value_outgoing_dont_throttle_netmasks();
    &def == value
}

// DEFAULT HANDLING for outgoing_dot_to_auth_names
fn default_value_outgoing_dot_to_auth_names() -> Vec<String> {
    vec![
    ]
}
fn default_value_equal_outgoing_dot_to_auth_names(value: &Vec<String>) -> bool {
    let def = default_value_outgoing_dot_to_auth_names();
    &def == value
}

// DEFAULT HANDLING for outgoing_dont_query
fn default_value_outgoing_dont_query() -> Vec<String> {
    vec![
        String::from("127.0.0.0/8"),
        String::from("10.0.0.0/8"),
        String::from("100.64.0.0/10"),
        String::from("169.254.0.0/16"),
        String::from("192.168.0.0/16"),
        String::from("172.16.0.0/12"),
        String::from("::1/128"),
        String::from("fc00::/7"),
        String::from("fe80::/10"),
        String::from("0.0.0.0/8"),
        String::from("192.0.0.0/24"),
        String::from("192.0.2.0/24"),
        String::from("198.51.100.0/24"),
        String::from("203.0.113.0/24"),
        String::from("240.0.0.0/4"),
        String::from("::/96"),
        String::from("::ffff:0:0/96"),
        String::from("100::/64"),
        String::from("2001:db8::/32"),
    ]
}
fn default_value_equal_outgoing_dont_query(value: &Vec<String>) -> bool {
    let def = default_value_outgoing_dont_query();
    &def == value
}

// DEFAULT HANDLING for outgoing_edns_subnet_allow_list
fn default_value_outgoing_edns_subnet_allow_list() -> Vec<String> {
    vec![
    ]
}
fn default_value_equal_outgoing_edns_subnet_allow_list(value: &Vec<String>) -> bool {
    let def = default_value_outgoing_edns_subnet_allow_list();
    &def == value
}

// DEFAULT HANDLING for outgoing_source_address
fn default_value_outgoing_source_address() -> Vec<String> {
    vec![
        String::from("0.0.0.0"),
    ]
}
fn default_value_equal_outgoing_source_address(value: &Vec<String>) -> bool {
    let def = default_value_outgoing_source_address();
    &def == value
}

// DEFAULT HANDLING for outgoing_udp_source_port_avoid
fn default_value_outgoing_udp_source_port_avoid() -> Vec<String> {
    vec![
        String::from("4791"),
        String::from("11211"),
    ]
}
fn default_value_equal_outgoing_udp_source_port_avoid(value: &Vec<String>) -> bool {
    let def = default_value_outgoing_udp_source_port_avoid();
    &def == value
}

// DEFAULT HANDLING for logging_structured_logging_backend
fn default_value_logging_structured_logging_backend() -> String {
    String::from("default")
}
fn default_value_equal_logging_structured_logging_backend(value: &str)-> bool {
    value == default_value_logging_structured_logging_backend()
}

// DEFAULT HANDLING for logging_trace
fn default_value_logging_trace() -> String {
    String::from("no")
}
fn default_value_equal_logging_trace(value: &str)-> bool {
    value == default_value_logging_trace()
}

// DEFAULT HANDLING for logging_protobuf_servers
fn default_value_logging_protobuf_servers() -> Vec<recsettings::ProtobufServer> {
    let msg = "default value defined for `logging_protobuf_servers' should be valid YAML";    let deserialized: Vec<recsettings::ProtobufServer> = serde_yaml::from_str("").expect(msg);
    deserialized
}
fn default_value_equal_logging_protobuf_servers(value: &Vec<recsettings::ProtobufServer>)-> bool {
    let def = default_value_logging_protobuf_servers();
    &def == value
}

// DEFAULT HANDLING for logging_outgoing_protobuf_servers
fn default_value_logging_outgoing_protobuf_servers() -> Vec<recsettings::ProtobufServer> {
    let msg = "default value defined for `logging_outgoing_protobuf_servers' should be valid YAML";    let deserialized: Vec<recsettings::ProtobufServer> = serde_yaml::from_str("").expect(msg);
    deserialized
}
fn default_value_equal_logging_outgoing_protobuf_servers(value: &Vec<recsettings::ProtobufServer>)-> bool {
    let def = default_value_logging_outgoing_protobuf_servers();
    &def == value
}

// DEFAULT HANDLING for logging_dnstap_framestream_servers
fn default_value_logging_dnstap_framestream_servers() -> Vec<recsettings::DNSTapFrameStreamServer> {
    let msg = "default value defined for `logging_dnstap_framestream_servers' should be valid YAML";    let deserialized: Vec<recsettings::DNSTapFrameStreamServer> = serde_yaml::from_str("").expect(msg);
    deserialized
}
fn default_value_equal_logging_dnstap_framestream_servers(value: &Vec<recsettings::DNSTapFrameStreamServer>)-> bool {
    let def = default_value_logging_dnstap_framestream_servers();
    &def == value
}

// DEFAULT HANDLING for logging_dnstap_nod_framestream_servers
fn default_value_logging_dnstap_nod_framestream_servers() -> Vec<recsettings::DNSTapNODFrameStreamServer> {
    let msg = "default value defined for `logging_dnstap_nod_framestream_servers' should be valid YAML";    let deserialized: Vec<recsettings::DNSTapNODFrameStreamServer> = serde_yaml::from_str("").expect(msg);
    deserialized
}
fn default_value_equal_logging_dnstap_nod_framestream_servers(value: &Vec<recsettings::DNSTapNODFrameStreamServer>)-> bool {
    let def = default_value_logging_dnstap_nod_framestream_servers();
    &def == value
}

// DEFAULT HANDLING for ecs_add_for
fn default_value_ecs_add_for() -> Vec<String> {
    vec![
        String::from("0.0.0.0/0"),
        String::from("::/0"),
        String::from("!127.0.0.0/8"),
        String::from("!10.0.0.0/8"),
        String::from("!100.64.0.0/10"),
        String::from("!169.254.0.0/16"),
        String::from("!192.168.0.0/16"),
        String::from("!172.16.0.0/12"),
        String::from("!::1/128"),
        String::from("!fc00::/7"),
        String::from("!fe80::/10"),
    ]
}
fn default_value_equal_ecs_add_for(value: &Vec<String>) -> bool {
    let def = default_value_ecs_add_for();
    &def == value
}

// DEFAULT HANDLING for recordcache_zonetocaches
fn default_value_recordcache_zonetocaches() -> Vec<recsettings::ZoneToCache> {
    let msg = "default value defined for `recordcache_zonetocaches' should be valid YAML";    let deserialized: Vec<recsettings::ZoneToCache> = serde_yaml::from_str("").expect(msg);
    deserialized
}
fn default_value_equal_recordcache_zonetocaches(value: &Vec<recsettings::ZoneToCache>)-> bool {
    let def = default_value_recordcache_zonetocaches();
    &def == value
}

// DEFAULT HANDLING for nod_history_dir
fn default_value_nod_history_dir() -> String {
    String::from(env!("NODCACHEDIRNOD"))
}
fn default_value_equal_nod_history_dir(value: &str)-> bool {
    value == default_value_nod_history_dir()
}

// DEFAULT HANDLING for nod_ignore_list
fn default_value_nod_ignore_list() -> Vec<String> {
    vec![
    ]
}
fn default_value_equal_nod_ignore_list(value: &Vec<String>) -> bool {
    let def = default_value_nod_ignore_list();
    &def == value
}

// DEFAULT HANDLING for nod_pb_tag
fn default_value_nod_pb_tag() -> String {
    String::from("pdns-nod")
}
fn default_value_equal_nod_pb_tag(value: &str)-> bool {
    value == default_value_nod_pb_tag()
}

// DEFAULT HANDLING for nod_unique_response_history_dir
fn default_value_nod_unique_response_history_dir() -> String {
    String::from(env!("NODCACHEDIRUDR"))
}
fn default_value_equal_nod_unique_response_history_dir(value: &str)-> bool {
    value == default_value_nod_unique_response_history_dir()
}

// DEFAULT HANDLING for nod_unique_response_pb_tag
fn default_value_nod_unique_response_pb_tag() -> String {
    String::from("pdns-udr")
}
fn default_value_equal_nod_unique_response_pb_tag(value: &str)-> bool {
    value == default_value_nod_unique_response_pb_tag()
}

// DEFAULT HANDLING for nod_unique_response_ignore_list
fn default_value_nod_unique_response_ignore_list() -> Vec<String> {
    vec![
    ]
}
fn default_value_equal_nod_unique_response_ignore_list(value: &Vec<String>) -> bool {
    let def = default_value_nod_unique_response_ignore_list();
    &def == value
}

