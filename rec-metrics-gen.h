// THIS IS A GENERATED FILE. DO NOT EDIT. SOURCE metrics.py AND metrics_table.py
addGetStat("questions", [] { return g_Counters.sum(rec::Counter::qcounter); });
addGetStat("ipv6-questions", [] { return g_Counters.sum(rec::Counter::ipv6qcounter); });
addGetStat("tcp-questions", [] { return g_Counters.sum(rec::Counter::tcpqcounter); });
addGetStat("cache-hits", []() { return g_recCache->getCacheHits(); });
addGetStat("cache-misses", []() { return g_recCache->getCacheMisses(); });
addGetStat("cache-entries", doGetCacheSize);
addGetStat("max-cache-entries", []() { return g_maxCacheEntries.load(); });
addGetStat("max-packetcache-entries", []() { return g_maxPacketCacheEntries.load(); });
addGetStat("cache-bytes", doGetCacheBytes);
addGetStat("packetcache-hits", [] { return g_packetCache ? g_packetCache->getHits() : 0; });
addGetStat("packetcache-misses", [] { return g_packetCache ? g_packetCache->getMisses() : 0; });
addGetStat("packetcache-entries", [] { return g_packetCache ? g_packetCache->size() : 0; });
addGetStat("packetcache-bytes", [] { return g_packetCache ? g_packetCache->bytes() : 0; });
addGetStat("malloc-bytes", doGetMallocated);
addGetStat("servfail-answers", [] { return g_Counters.sum(rec::Counter::servFails); });
addGetStat("nxdomain-answers", [] { return g_Counters.sum(rec::Counter::nxDomains); });
addGetStat("noerror-answers", [] { return g_Counters.sum(rec::Counter::noErrors); });
addGetStat("unauthorized-udp", [] { return g_Counters.sum(rec::Counter::unauthorizedUDP); });
addGetStat("unauthorized-tcp", [] { return g_Counters.sum(rec::Counter::unauthorizedTCP); });
addGetStat("tcp-client-overflow", [] { return g_Counters.sum(rec::Counter::tcpClientOverflow); });
addGetStat("client-parse-errors", [] { return g_Counters.sum(rec::Counter::clientParseError); });
addGetStat("server-parse-errors", [] { return g_Counters.sum(rec::Counter::serverParseError); });
addGetStat("too-old-drops", [] { return g_Counters.sum(rec::Counter::tooOldDrops); });
addGetStat("answers0-1", []() { return g_Counters.sum(rec::Histogram::answers).getCount(0); });
addGetStat("answers1-10", []() { return g_Counters.sum(rec::Histogram::answers).getCount(1); });
addGetStat("answers10-100", []() { return g_Counters.sum(rec::Histogram::answers).getCount(2); });
addGetStat("answers100-1000", []() { return g_Counters.sum(rec::Histogram::answers).getCount(3); });
addGetStat("answers-slow", []() { return g_Counters.sum(rec::Histogram::answers).getCount(4); });
addGetStat("x-ourtime0-1", []() { return g_Counters.sum(rec::Histogram::ourtime).getCount(0); });
addGetStat("x-ourtime1-2", []() { return g_Counters.sum(rec::Histogram::ourtime).getCount(1); });
addGetStat("x-ourtime2-4", []() { return g_Counters.sum(rec::Histogram::ourtime).getCount(2); });
addGetStat("x-ourtime4-8", []() { return g_Counters.sum(rec::Histogram::ourtime).getCount(3); });
addGetStat("x-ourtime8-16", []() { return g_Counters.sum(rec::Histogram::ourtime).getCount(4); });
addGetStat("x-ourtime16-32", []() { return g_Counters.sum(rec::Histogram::ourtime).getCount(5); });
addGetStat("x-ourtime-slow", []() { return g_Counters.sum(rec::Histogram::ourtime).getCount(6); });
addGetStat("auth4-answers0-1", []() { return g_Counters.sum(rec::Histogram::auth4Answers).getCount(0); });
addGetStat("auth4-answers1-10", []() { return g_Counters.sum(rec::Histogram::auth4Answers).getCount(1); });
addGetStat("auth4-answers10-100", []() { return g_Counters.sum(rec::Histogram::auth4Answers).getCount(2); });
addGetStat("auth4-answers100-1000", []() { return g_Counters.sum(rec::Histogram::auth4Answers).getCount(3); });
addGetStat("auth4-answers-slow", []() { return g_Counters.sum(rec::Histogram::auth4Answers).getCount(4); });
addGetStat("auth6-answers0-1", []() { return g_Counters.sum(rec::Histogram::auth6Answers).getCount(0); });
addGetStat("auth6-answers1-10", []() { return g_Counters.sum(rec::Histogram::auth6Answers).getCount(1); });
addGetStat("auth6-answers10-100", []() { return g_Counters.sum(rec::Histogram::auth6Answers).getCount(2); });
addGetStat("auth6-answers100-1000", []() { return g_Counters.sum(rec::Histogram::auth6Answers).getCount(3); });
addGetStat("auth6-answers-slow", []() { return g_Counters.sum(rec::Histogram::auth6Answers).getCount(4); });
addGetStat("qa-latency", []() { return round(g_Counters.avg(rec::DoubleWAvgCounter::avgLatencyUsec)); });
addGetStat("x-our-latency", []() { return round(g_Counters.avg(rec::DoubleWAvgCounter::avgLatencyOursUsec)); });
addGetStat("unexpected-packets", [] { return g_Counters.sum(rec::Counter::unexpectedCount); });
addGetStat("case-mismatches", [] { return g_Counters.sum(rec::Counter::caseMismatchCount); });
addGetStat("spoof-prevents", [] { return g_Counters.sum(rec::Counter::spoofCount); });
addGetStat("nsset-invalidations", [] { return g_Counters.sum(rec::Counter::nsSetInvalidations); });
addGetStat("resource-limits", [] { return g_Counters.sum(rec::Counter::resourceLimits); });
addGetStat("over-capacity-drops", [] { return g_Counters.sum(rec::Counter::overCapacityDrops); });
addGetStat("policy-drops", [] { return g_Counters.sum(rec::Counter::policyDrops); });
addGetStat("no-packet-error", [] { return g_Counters.sum(rec::Counter::noPacketError); });
addGetStat("ignored-packets", [] { return g_Counters.sum(rec::Counter::ignoredCount); });
addGetStat("max-mthread-stack", [] { return g_Counters.max(rec::Counter::maxMThreadStackUsage); });
addGetStat("negcache-entries", getNegCacheSize);
addGetStat("throttle-entries", SyncRes::getThrottledServersSize);
addGetStat("nsspeeds-entries", SyncRes::getNSSpeedsSize);
addGetStat("failed-host-entries", SyncRes::getFailedServersSize);
addGetStat("concurrent-queries", getConcurrentQueries);
addGetStat("security-status", &g_security_status);
addGetStat("outgoing-timeouts", [] { return g_Counters.sum(rec::Counter::outgoingtimeouts); });
addGetStat("outgoing4-timeouts", [] { return g_Counters.sum(rec::Counter::outgoing4timeouts); });
addGetStat("outgoing6-timeouts", [] { return g_Counters.sum(rec::Counter::outgoing6timeouts); });
addGetStat("auth-zone-queries", [] { return g_Counters.sum(rec::Counter::authzonequeries); });
addGetStat("tcp-outqueries", [] { return g_Counters.sum(rec::Counter::tcpoutqueries); });
addGetStat("all-outqueries", [] { return g_Counters.sum(rec::Counter::outqueries); });
addGetStat("ipv6-outqueries", [] { return g_Counters.sum(rec::Counter::ipv6queries); });
addGetStat("throttled-outqueries", [] { return g_Counters.sum(rec::Counter::throttledqueries); });
addGetStat("dont-outqueries", [] { return g_Counters.sum(rec::Counter::dontqueries); });
addGetStat("throttled-out", [] { return g_Counters.sum(rec::Counter::throttledqueries); });
addGetStat("unreachables", [] { return g_Counters.sum(rec::Counter::unreachables); });
addGetStat("ecs-queries", &SyncRes::s_ecsqueries);
addGetStat("ecs-responses", &SyncRes::s_ecsresponses);
addGetStat("chain-resends", [] { return g_Counters.sum(rec::Counter::chainResends); });
addGetStat("tcp-clients", [] { return TCPConnection::getCurrentConnections(); });
#ifdef __linux
addGetStat("udp-recvbuf-errors", [] { return udpErrorStats("udp-recvbuf-errors"); });
#endif
#ifdef __linux
addGetStat("udp-sndbuf-errors", [] { return udpErrorStats("udp-sndbuf-errors"); });
#endif
#ifdef __linux
addGetStat("udp-noport-errors", [] { return udpErrorStats("udp-noport-errors"); });
#endif
#ifdef __linux
addGetStat("udp-in-errors", [] { return udpErrorStats("udp-in-errors"); });
#endif
addGetStat("edns-ping-matches", [] { return g_Counters.sum(rec::Counter::ednsPingMatches); });
addGetStat("edns-ping-mismatches", [] { return g_Counters.sum(rec::Counter::ednsPingMismatches); });
addGetStat("dnssec-queries", [] { return g_Counters.sum(rec::Counter::dnssecQueries); });
addGetStat("noping-outqueries", [] { return g_Counters.sum(rec::Counter::noPingOutQueries); });
addGetStat("noedns-outqueries", [] { return g_Counters.sum(rec::Counter::noEdnsOutQueries); });
addGetStat("uptime", [] { return time(nullptr) - s_startupTime; });
addGetStat("real-memory-usage", [] { return getRealMemoryUsage(string()); });
addGetStat("fd-usage", [] { return getOpenFileDescriptors(string()); });
addGetStat("user-msec", getUserTimeMsec);
addGetStat("sys-msec", getSysTimeMsec);
addGetStat("dnssec-validations", [] { return g_Counters.sum(rec::Counter::dnssecValidations); });
addGetStat("dnssec-result-insecure", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::Insecure); });
addGetStat("dnssec-result-secure", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::Secure); });
addGetStat("dnssec-result-bogus", []() {
            std::set<vState> const bogusStates = {vState::BogusNoValidDNSKEY, vState::BogusInvalidDenial, vState::BogusUnableToGetDSs, vState::BogusUnableToGetDNSKEYs, vState::BogusSelfSignedDS, vState::BogusNoRRSIG, vState::BogusNoValidRRSIG, vState::BogusMissingNegativeIndication, vState::BogusSignatureNotYetValid, vState::BogusSignatureExpired, vState::BogusUnsupportedDNSKEYAlgo, vState::BogusUnsupportedDSDigestType, vState::BogusNoZoneKeyBitSet, vState::BogusRevokedDNSKEY, vState::BogusInvalidDNSKEYProtocol};
            auto counts = g_Counters.sum(rec::DNSSECHistogram::dnssec);
            uint64_t total = 0;
            for (const auto& state : bogusStates) {
              total += counts.at(state);
            }
            return total;
          });
addGetStat("dnssec-result-indeterminate", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::Indeterminate); });
addGetStat("dnssec-result-nta", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::NTA); });
addGetStat("policy-result-noaction", [] { return g_Counters.sum(rec::PolicyHistogram::policy).at(DNSFilterEngine::PolicyKind::NoAction); });
addGetStat("policy-result-drop", [] { return g_Counters.sum(rec::PolicyHistogram::policy).at(DNSFilterEngine::PolicyKind::Drop); });
addGetStat("policy-result-nxdomain", [] { return g_Counters.sum(rec::PolicyHistogram::policy).at(DNSFilterEngine::PolicyKind::NXDOMAIN); });
addGetStat("policy-result-nodata", [] { return g_Counters.sum(rec::PolicyHistogram::policy).at(DNSFilterEngine::PolicyKind::NODATA); });
addGetStat("policy-result-truncate", [] { return g_Counters.sum(rec::PolicyHistogram::policy).at(DNSFilterEngine::PolicyKind::Truncate); });
addGetStat("policy-result-custom", [] { return g_Counters.sum(rec::PolicyHistogram::policy).at(DNSFilterEngine::PolicyKind::Custom); });
addGetStat("query-pipe-full-drops", [] { return g_Counters.sum(rec::Counter::queryPipeFullDrops); });
addGetStat("truncated-drops", [] { return g_Counters.sum(rec::Counter::truncatedDrops); });
addGetStat("empty-queries", [] { return g_Counters.sum(rec::Counter::emptyQueriesCount); });
addGetStat("dnssec-authentic-data-queries", [] { return g_Counters.sum(rec::Counter::dnssecAuthenticDataQueries); });
addGetStat("dnssec-check-disabled-queries", [] { return g_Counters.sum(rec::Counter::dnssecCheckDisabledQueries); });
addGetStat("variable-responses", [] { return g_Counters.sum(rec::Counter::variableResponses); });
addGetStat("special-memory-usage", [] { return getSpecialMemoryUsage(string()); });
addGetStat("rebalanced-queries", [] { return g_Counters.sum(rec::Counter::rebalancedQueries); });
addGetStat("qname-min-fallback-success", [] { return g_Counters.sum(rec::Counter::qnameminfallbacksuccess); });
addGetStat("proxy-protocol-invalid", [] { return g_Counters.sum(rec::Counter::proxyProtocolInvalidCount); });
addGetStat("record-cache-contended", []() { return g_recCache->stats().first; });
addGetStat("record-cache-acquired", []() { return g_recCache->stats().second; });
addGetStat("nod-lookups-dropped-oversize", [] { return g_Counters.sum(rec::Counter::nodLookupsDroppedOversize); });
addGetStat("taskqueue-pushed", []() { return getTaskPushes(); });
addGetStat("taskqueue-expired", []() { return getTaskExpired(); });
addGetStat("taskqueue-size", []() { return getTaskSize(); });
addGetStat("aggressive-nsec-cache-entries", []() { return g_aggressiveNSECCache ? g_aggressiveNSECCache->getEntriesCount() : 0; });
addGetStat("aggressive-nsec-cache-nsec-hits", []() { return g_aggressiveNSECCache ? g_aggressiveNSECCache->getNSECHits() : 0; });
addGetStat("aggressive-nsec-cache-nsec3-hits", []() { return g_aggressiveNSECCache ? g_aggressiveNSECCache->getNSEC3Hits() : 0; });
addGetStat("aggressive-nsec-cache-nsec-wc-hits", []() { return g_aggressiveNSECCache ? g_aggressiveNSECCache->getNSECWildcardHits() : 0; });
addGetStat("aggressive-nsec-cache-nsec3-wc-hits", []() { return g_aggressiveNSECCache ? g_aggressiveNSECCache->getNSEC3WildcardHits() : 0; });
addGetStat("dot-outqueries", [] { return g_Counters.sum(rec::Counter::dotoutqueries); });
addGetStat("dns64-prefix-answers", [] { return g_Counters.sum(rec::Counter::dns64prefixanswers); });
addGetStat("almost-expired-pushed", []() { return getAlmostExpiredTasksPushed(); });
addGetStat("almost-expired-run", []() { return getAlmostExpiredTasksRun(); });
addGetStat("almost-expired-exceptions", []() { return getAlmostExpiredTaskExceptions(); });
#ifdef __linux
addGetStat("udp-in-csum-errors", [] { return udpErrorStats("udp-in-csum-errors"); });
#endif
#ifdef __linux
addGetStat("udp6-recvbuf-errors", [] { return udp6ErrorStats("udp6-recvbuf-errors"); });
#endif
#ifdef __linux
addGetStat("udp6-sndbuf-errors", [] { return udp6ErrorStats("udp6-sndbuf-errors"); });
#endif
#ifdef __linux
addGetStat("udp6-noport-errors", [] { return udp6ErrorStats("udp6-noport-errors"); });
#endif
#ifdef __linux
addGetStat("udp6-in-errors", [] { return udp6ErrorStats("udp6-in-errors"); });
#endif
#ifdef __linux
addGetStat("udp6-in-csum-errors", [] { return udp6ErrorStats("udp6-in-csum-errors"); });
#endif
#ifdef __linux
addGetStat("cpu-iowait", [] { return getCPUIOWait(string()); });
#endif
#ifdef __linux
addGetStat("cpu-steal", [] { return getCPUSteal(string()); });
#endif
addGetStat("cpu-msec-thread-n", []() { return toCPUStatsMap("cpu-msec"); });
#ifdef MALLOC_TRACE
addGetStat("memory-allocs", [] { return g_mtracer->getAllocs(string()); });
#endif
#ifdef MALLOC_TRACE
addGetStat("memory-alloc-flux", [] { return g_mtracer->getAllocFlux(string()); });
#endif
#ifdef MALLOC_TRACE
addGetStat("memory-allocated", [] { return g_mtracer->getTotAllocated(string()); });
#endif
addGetStat("dnssec-result-bogus-no-valid-dnskey", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::BogusNoValidDNSKEY); });
addGetStat("dnssec-result-bogus-invalid-denial", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::BogusInvalidDenial); });
addGetStat("dnssec-result-bogus-unable-to-get-dss", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::BogusUnableToGetDSs); });
addGetStat("dnssec-result-bogus-unable-to-get-dnskeys", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::BogusUnableToGetDNSKEYs); });
addGetStat("dnssec-result-bogus-self-signed-ds", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::BogusSelfSignedDS); });
addGetStat("dnssec-result-bogus-no-rrsig", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::BogusNoRRSIG); });
addGetStat("dnssec-result-bogus-no-valid-rrsig", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::BogusNoValidRRSIG); });
addGetStat("dnssec-result-bogus-missing-negative-indication", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::BogusMissingNegativeIndication); });
addGetStat("dnssec-result-bogus-signature-not-yet-valid", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::BogusSignatureNotYetValid); });
addGetStat("dnssec-result-bogus-signature-expired", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::BogusSignatureExpired); });
addGetStat("dnssec-result-bogus-unsupported-dnskey-algo", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::BogusUnsupportedDNSKEYAlgo); });
addGetStat("dnssec-result-bogus-unsupported-ds-digest-type", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::BogusUnsupportedDSDigestType); });
addGetStat("dnssec-result-bogus-no-zone-key-bit-set", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::BogusNoZoneKeyBitSet); });
addGetStat("dnssec-result-bogus-revoked-dnskey", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::BogusRevokedDNSKEY); });
addGetStat("dnssec-result-bogus-invalid-dnskey-protocol", [] { return g_Counters.sum(rec::DNSSECHistogram::dnssec).at(vState::BogusInvalidDNSKEYProtocol); });
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus", []() {
         std::set<vState> const bogusStates = {vState::BogusNoValidDNSKEY, vState::BogusInvalidDenial, vState::BogusUnableToGetDSs, vState::BogusUnableToGetDNSKEYs, vState::BogusSelfSignedDS, vState::BogusNoRRSIG, vState::BogusNoValidRRSIG, vState::BogusMissingNegativeIndication, vState::BogusSignatureNotYetValid, vState::BogusSignatureExpired, vState::BogusUnsupportedDNSKEYAlgo, vState::BogusUnsupportedDSDigestType, vState::BogusNoZoneKeyBitSet, vState::BogusRevokedDNSKEY, vState::BogusInvalidDNSKEYProtocol};
          auto counts = g_Counters.sum(rec::DNSSECHistogram::xdnssec);
          uint64_t total = 0;
          for (const auto& state : bogusStates) {
            total += counts.at(state);
          }
          return total;
         });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus-no-valid-dnskey", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::BogusNoValidDNSKEY); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus-invalid-denial", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::BogusInvalidDenial); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus-unable-to-get-dss", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::BogusUnableToGetDSs); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus-unable-to-get-dnskeys", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::BogusUnableToGetDNSKEYs); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus-self-signed-ds", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::BogusSelfSignedDS); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus-no-rrsig", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::BogusNoRRSIG); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus-no-valid-rrsig", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::BogusNoValidRRSIG); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus-missing-negative-indication", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::BogusMissingNegativeIndication); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus-signature-not-yet-valid", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::BogusSignatureNotYetValid); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus-signature-expired", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::BogusSignatureExpired); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus-unsupported-dnskey-algo", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::BogusUnsupportedDNSKEYAlgo); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus-unsupported-ds-digest-type", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::BogusUnsupportedDSDigestType); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus-no-zone-key-bit-set", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::BogusNoZoneKeyBitSet); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus-revoked-dnskey", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::BogusRevokedDNSKEY); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-bogus-invalid-dnskey-protocol", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::BogusInvalidDNSKEYProtocol); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-indeterminate", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::Indeterminate); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-nta", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::NTA); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-insecure", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::Insecure); });
}
if (::arg()["x-dnssec-names"].length() > 0) {
addGetStat("x-dnssec-result-secure", [] { return g_Counters.sum(rec::DNSSECHistogram::xdnssec).at(vState::Secure); });
}
addGetStat("idle-tcpout-connections", getCurrentIdleTCPConnections);
addGetStat("source-disallowed-notify", [] { return g_Counters.sum(rec::Counter::sourceDisallowedNotify); });
addGetStat("zone-disallowed-notify", [] { return g_Counters.sum(rec::Counter::zoneDisallowedNotify); });
addGetStat("non-resolving-nameserver-entries", SyncRes::getNonResolvingNSSize);
addGetStat("maintenance-usec", [] { return g_Counters.sum(rec::Counter::maintenanceUsec); });
addGetStat("maintenance-calls", [] { return g_Counters.sum(rec::Counter::maintenanceCalls); });
addGetStat("packetcache-contended", []() { return g_packetCache ? g_packetCache->stats().first : 0; });
addGetStat("packetcache-acquired", []() { return g_packetCache ? g_packetCache->stats().second : 0; });
addGetStat("nod-events", [] { return g_Counters.sum(rec::Counter::nodCount); });
addGetStat("udr-events", [] { return g_Counters.sum(rec::Counter::udrCount); });
addGetStat("max-chain-length", [] { return g_Counters.max(rec::Counter::maxChainLength); });
addGetStat("max-chain-weight", [] { return g_Counters.max(rec::Counter::maxChainWeight); });
addGetStat("chain-limits", [] { return g_Counters.sum(rec::Counter::chainLimits); });
addGetStat("tcp-overflow", [] { return g_Counters.sum(rec::Counter::tcpOverflow); });
addGetStat("remote-logger-count", []() {
    return toRemoteLoggerStatsMap("remote-logger-count");
        });
